
           
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                     Wilayah
                    </h1>
                           <ol class="breadcrumb">
                        <li><a href="<?=base_index();?>"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li><a href="<?=base_index();?>wilayah">Wilayah</a></li>
                        <li class="active">Tambah Wilayah</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
<div class="row">
    <div class="col-lg-12"> 
        <div class="box box-solid box-primary">
                                 <div class="box-header">
                                    <h3 class="box-title">Tambah Wilayah</h3>
                                    <div class="box-tools pull-right">
                                        <button class="btn btn-info btn-sm" data-widget="collapse"><i class="fa fa-minus"></i></button>
                                        <button class="btn btn-info btn-sm" data-widget="remove"><i class="fa fa-times"></i></button>
                                    </div>
                                </div>

                  <div class="box-body">
                     <form id="input" method="post" class="form-horizontal foto_banyak" action="<?=base_admin();?>modul/wilayah/wilayah_action.php?act=in">
                      <div class="form-group">
                        <label for="id_wil" class="control-label col-lg-2">id_wil</label>
                        <div class="col-lg-10">
                          <input type="text" data-rule-number="true" name="id_wil" placeholder="id_wil" class="form-control" required> 
                        </div>
                      </div><!-- /.form-group -->
<div class="form-group">
                        <label for="nm_wil" class="control-label col-lg-2">nm_wil</label>
                        <div class="col-lg-10">
                          <input type="text" name="nm_wil" placeholder="nm_wil" class="form-control" required> 
                        </div>
                      </div><!-- /.form-group -->
<div class="form-group">
                        <label for="id_induk_wilayah" class="control-label col-lg-2">id_induk_wilayah</label>
                        <div class="col-lg-10">
                          <select name="id_induk_wilayah" data-placeholder="Pilih id_induk_wilayah ..." class="form-control chzn-select" tabindex="2" required>
               <option value=""></option>
               <?php foreach ($db->fetch_all("data_wilayah") as $isi) {
                  echo "<option value='$isi->id'>$isi->nm_wil</option>";
               } ?>
              </select>
                        </div>
                      </div><!-- /.form-group -->
<div class="form-group">
                        <label for="id_level_wil" class="control-label col-lg-2">id_level_wil</label>
                        <div class="col-lg-10">
                          <input type="text" data-rule-number="true" name="id_level_wil" placeholder="id_level_wil" class="form-control" required> 
                        </div>
                      </div><!-- /.form-group -->

                      
                      <div class="form-group">
                        <label for="tags" class="control-label col-lg-2">&nbsp;</label>
                        <div class="col-lg-10">
                          <input type="submit" class="btn btn-primary btn-flat" value="submit">
                        </div>
                      </div><!-- /.form-group -->
                    </form>
 <a href="<?=base_index();?>wilayah" class="btn btn-success btn-flat"><i class="fa fa-step-backward"></i> Kembali</a>
          
                  </div>
                  </div>
              </div>
</div>
                  
                </section><!-- /.content -->
        
            