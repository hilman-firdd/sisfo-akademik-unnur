

                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                     Wilayah
                    </h1>
                   <ol class="breadcrumb">
                        <li><a href="<?=base_index();?>"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li><a href="<?=base_index();?>wilayah">Wilayah</a></li>
                        <li class="active">Detail Wilayah</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
<div class="row">
    <div class="col-lg-12">
        <div class="box box-solid box-primary">
                                   <div class="box-header">
                                    <h3 class="box-title">Detail Wilayah</h3>
                                    <div class="box-tools pull-right">
                                        <button class="btn btn-info btn-sm" data-widget="collapse"><i class="fa fa-minus"></i></button>
                                        <button class="btn btn-info btn-sm" data-widget="remove"><i class="fa fa-times"></i></button>
                                    </div>
                                </div>

                  <div class="box-body">
                   <form class="form-horizontal">
                      <div class="form-group">
                        <label for="id_wil" class="control-label col-lg-2">id_wil</label>
                        <div class="col-lg-10">
                          <input type="text" disabled="" value="<?=$data_edit->id_wil;?>" class="form-control">
                        </div>
                      </div><!-- /.form-group -->
<div class="form-group">
                        <label for="nm_wil" class="control-label col-lg-2">nm_wil</label>
                        <div class="col-lg-10">
                          <input type="text" disabled="" value="<?=$data_edit->nm_wil;?>" class="form-control">
                        </div>
                      </div><!-- /.form-group -->
<div class="form-group">
                        <label for="id_induk_wilayah" class="control-label col-lg-2">id_induk_wilayah</label>
                        <div class="col-lg-10">
                          <?php foreach ($db->fetch_all("data_wilayah") as $isi) {
                  if ($data_edit->id_induk_wilayah==$isi->id) {

                    echo "<input disabled class='form-control' type='text' value='$isi->nm_wil'>";
                  }
               } ?>
              
                        </div>
                      </div><!-- /.form-group -->
<div class="form-group">
                        <label for="id_level_wil" class="control-label col-lg-2">id_level_wil</label>
                        <div class="col-lg-10">
                          <input type="text" disabled="" value="<?=$data_edit->id_level_wil;?>" class="form-control">
                        </div>
                      </div><!-- /.form-group -->

                   
                    </form>
                    <a href="<?=base_index();?>wilayah" class="btn btn-success btn-flat"><i class="fa fa-step-backward"></i> Kembali</a>
          
                  </div>
                  </div>
              </div>
</div>
                  
                </section><!-- /.content -->
        
