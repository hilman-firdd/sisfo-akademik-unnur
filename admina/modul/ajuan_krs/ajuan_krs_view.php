
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        Manage Rencana Studi <?php echo $db->fetch_single_row('jurusan','kode_jurusan',$id_jur)->nama_jurusan;?>
                    </h1>
                        <ol class="breadcrumb">
                        <li><a href="<?=base_index();?>"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li><a href="<?=base_index();?>matakuliah-kurikulum">Matakuliah Kurikulum</a></li>
                        <li class="active">Matakuliah Kurikulum List</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="box">
                                <div class="box-header">
                                  <h3 class="box-title">List Rencana Studi</h3>
                                </div><!-- /.box-header -->

<form method="get" class="form-horizontal" action="">
                      
                      <div class="form-group">
                        <label for="Menu" class="control-label col-lg-2">Mahasiswa</label>
                        <div class="col-lg-4">
                        
                        
                          <link href="<?=base_admin();?>assets/plugins/chosen/chosen.min.css" rel="stylesheet" type="text/css" />
                          <script src="<?=base_admin();?>assets/plugins/chosen/chosen.jquery.min.js" type="text/javascript"></script>

                          <!-- Script -->
                          <script type="text/javascript">
                          $(document).ready(function(){
                           $('select').chosen({ height:'50' });
                          });
                          </script>

                        <select name="user" id="id_po_select" data-placeholder="Pilih User" class="form-control chzn-select" tabindex="2" >
                        <option value=""></option>
                          <?php 
                              $username=ucwords($db->fetch_single_row('sys_users','id',$_SESSION['id_user'])->username);
                              $name=ucwords($db->fetch_single_row('mhs','nipd',$username)->nm_pd);
                              if($username<>"Admin"){
                              foreach ($db->fetch_custom("select nipd, nm_pd from mhs                                                                                                                        
                                                          where mhs.nipd=? order by mhs.nm_pd asc", array('mhs.nipd'=>$username)) as $isi) {
                                       if ($username==$isi->nipd) {
                                                   echo "<option value='$isi->nipd' selected>$isi->nipd  -  $isi->nm_pd</option>";
                                       } else {
                                                   echo "<option value='$isi->nipd'>$isi->nipd  -  $isi->nm_pd</option>";
                                       }
                               }
                               }
                               else
                               {
                                foreach ($db->fetch_custom("select nipd, nm_pd from mhs") as $isi) {
                                  
                                       if ($_GET['user']==$isi->nipd) {
                                                   echo "<option value='$isi->nipd' selected>$isi->nipd  -  $isi->nm_pd</option>";
                                                   $username=$_GET['user'];
                                                   $name=ucwords($db->fetch_single_row('mhs','nipd',$username)->nm_pd);
                                       } else {
                                                   echo "<option value='$isi->nipd'>$isi->nipd  -  $isi->nm_pd</option>";
                                       }
                               }
                               } 
                          ?>

                  
                  </select>
                        </div>
                      </div><!-- /.form-group -->

 <label for="Menu" class="control-label col-lg-2">&nbsp;</label>
                        <div class="col-lg-10">
<button style="margin-top:10px;margin-bottom:10px" class="btn btn-primary">Show Rencana Studi</button>
</div>
</form>

                                <div class="box-body table-responsive">
          
<?php if (isset($_GET['user'])) {
  
?>       
<h3>Checklist Untuk memberikan pilihan mata kuliah sebagai rencana studi Anda pada semester ini</h3>
<table id="dtb" class="table table-bordered table-condensed table-hover table-striped">
                      <thead align="center">
                        <tr>
                          <th style="width:20px">Pilih Mata Kuliah</th>
                          <th style="width:10px ">No</th>
                          <th style="width:20px">Kode Mata Kuliah</th>
                          <th style="width:40px">Nama Mata Kuliah</th>
                          <th style="width:10px">Semester</th>
                          <th style="width:20px">Jenis Mata Kuliah</th>
                          <th style="width:20px">Total SKS</th>
                          <th style="width:20px">Keterangan</th>
                          <th style="width:20px">Jadwal Kuliah Pagi</th>
                          <th style="width:20px">Jadwal Kuliah Sore</th>
                         
                        </tr>
                      </thead>
                      <tbody>
                              <!--<div class="form-group"> <label>Semester : </label><input type="text" id="min" name="min"> </div> -->
                              <?php 
                                  $semester = $db->fetch_single_row('semester','status','Aktif')->semester;
                                  $dtb=$db->fetch_custom("SELECT * from view_0016_krs_mhs where   nim =? or nim is null", array('mhs.nipd'=>$_GET['user'])) ;
                                  //$dtb=$db->fetch_custom("SELECT distinct * from view_0016_krs_mhs where semester_mhs=5  or nim is null") ;
                                  
                                  //$dtb=$db->fetch_custom("SELECT distinct * from view_0011_jadwal_kuliah where semester = 20201") ;
                                  
                                  
                                  
                                  $i=1;
                                  foreach ($dtb as $isi) {
                                    ?>

                                    <tr id="line_<?=$isi->id;?>">
                                   
                                    <td align="center" >
                                      <div class="checkbox">
                                              <label>
                                                <!--<input class="uniform" name="cb_<?=$isi->id;?>" type="checkbox" value="option1" <?=(is_null($isi->id_krs_mhs))?'':'checked=""';?>  > -->

                                                <input class="uniform" name="option1" type="checkbox" value="<?=$isi->id;?>" onclick="read_act(<?=(is_null($isi->id_krs_mhs))?$isi->id:$isi->id_krs_mhs;?>,this)" <?=(is_null($isi->id_krs_mhs))?'':'checked=""';?>   >  
                                              </label>
                                       </div>
                                    </td>

                                    <td><?=$i.'.';?></td>
                                    <td align="center"><?=$isi->kode_mk;?></td>
                                    <td><?=$isi->nama_mk;?></td>
                                    <td align="center"><?=$isi->semester;?></td>
                                    <td align="center"><?=$isi->jns_matkul_deskripsi;?></td>
                                    <td align="center"><?=$isi->sks_tm;?></td>
                                    <td align="center" ><?php  if ($isi->status_krs == 1) {  echo "<b>[<font color='blue'>$isi->status_krs_nama</font>]</b>";} else if ($isi->status_krs == 2){echo  "<b>[<font color='red'>$isi->status_krs_nama</font>]</b>";} else {echo  "<b>[<font color='orange'>$isi->status_krs_nama</font>]</b>";} ?></td>
                                    <!--<td align=""><?=$isi->hari_nama.'-'.$isi->wkt_kul_deskripsi_pagi;?></td>
                                        <td align=""><?=$isi->hari_nama_sore.'-'.$isi->wkt_kul_deskripsi_sore;?></td>-->
                                    <td align=""><?=$isi->jadwal_pagi;?></td>
                                    <td align=""><?=$isi->jadwal_sore;?></td>
                                    
                                    <?php
                                    if($i)
                                    {
                                        ?>
                                        
                            <?php
                                    } else {

                                    

                                    ?>


                                    
                                                      <?php 
                                                      }
                                                      ?>
                                    </tr>
                                    <?php
                                    $i++;
                                  }
                                  ?>
                       </tbody>
</table>
<?php 

}  

?>


                                </div><!-- /.box-body -->
                            </div><!-- /.box -->
                        </div>
                    </div>
                </section><!-- /.content -->
  



<script type="text/javascript">



$(document).ready(function() {
      $('#dtb').DataTable( {


        //dom: 'Bfrtip',
        lengthMenu: [100, 200, 500],
        buttons: [
        'colvis'
        ],
        "oLanguage": {

           "sSearch": "Pencarian berdasarkan Semester atau Nama Mata Kuliah :  "

         },
    
         "columnDefs": [ {
      "targets": [0,1,2,5,6,7,8,9],
      "searchable": false
    } ],


    } );

   

  } );

 
  
function read_act(id,cb) {
  check_act = '';
  id_cb = id;
  if (cb.checked) {
     check_act = 'Y';

   setTimeout(function() {
   location.reload();
   }, 100);
  
  } else {
    check_act = 'N';

  setTimeout(function() {
   location.reload();
   }, 100);

  }
  
  
  
         

  $.ajax({

        type: "post",
        url: "<?=base_admin();?>modul/ajuan_krs/ajuan_krs_action.php?act=change_read",
        data: "nim="+'<?=$username;?>'+"&nama="+'<?=$name;?>'+"&id="+id_cb+"&cek="+check_act,
        
     //  enctype:  'multipart/form-data'
      success: function(data){
               
        console.log(data);
        //$("#dtb").dataTable().fnReloadAjax();

        //$('#dtb').DataTable().ajax.reload();

    }

  });
}

/*
function insert_act(id,cb) {
  check_act = '';
  if (cb.checked) {
     check_act = 'Y';
  } else {
    check_act = 'N';

  }
  $.ajax({

        type: "post",
        url: "<?=base_admin();?>modul/ajuan_krs/ajuan_krs_action.php?act=change_insert",
        data: "role_id="+id+"&data_act="+check_act,
     //  enctype:  'multipart/form-data'
      success: function(data){

        console.log(data);
    }

  });
}

function update_act(id,cb) {
  check_act = '';
  if (cb.checked) {
     check_act = 'Y';
  } else {
    check_act = 'N';

  }
  $.ajax({

        type: "post",
        url: "<?=base_admin();?>modul/ajuan_krs/ajuan_krs_action.php?act=change_update",
        data: "role_id="+id+"&data_act="+check_act,
     //  enctype:  'multipart/form-data'
      success: function(data){

        console.log(data);
    }

  });
}

function delete_act(id,cb) {
  check_act = '';
  if (cb.checked) {
     check_act = 'Y';
  } else {
    check_act = 'N';

  }
  $.ajax({

        type: "post",
        url: "<?=base_admin();?>modul/ajuan_krs/ajuan_krs_action.php?act=change_delete",
        data: "role_id="+id+"&data_act="+check_act,
     //  enctype:  'multipart/form-data'
      success: function(data){

        console.log(data);
    }

  });
}
*/

  </script>




