
           
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                     Ajuan KRS
                    </h1>
                           <ol class="breadcrumb">
                        <li><a href="<?=base_index();?>"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li><a href="<?=base_index();?>ajuan-krs">Ajuan KRS</a></li>
                        <li class="active">Tambah Ajuan KRS</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
<div class="row">
    <div class="col-lg-12"> 
        <div class="box box-solid box-primary">
                                 <div class="box-header">
                                    <h3 class="box-title">Tambah Ajuan KRS</h3>
                                    <div class="box-tools pull-right">
                                        <button class="btn btn-info btn-sm" data-widget="collapse"><i class="fa fa-minus"></i></button>
                                        <button class="btn btn-info btn-sm" data-widget="remove"><i class="fa fa-times"></i></button>
                                    </div>
                                </div>

                  <div class="box-body">
                     <form id="input" method="post" class="form-horizontal foto_banyak" action="<?=base_admin();?>modul/ajuan_krs/ajuan_krs_action.php?act=in">
                      <div class="form-group">
                        <label for="nim" class="control-label col-lg-2">nim</label>
                        <div class="col-lg-10">
                          <input type="text" name="nim" placeholder="nim" class="form-control" required> 
                        </div>
                      </div><!-- /.form-group -->
<div class="form-group">
                        <label for="nama" class="control-label col-lg-2">nama</label>
                        <div class="col-lg-10">
                          <input type="text" name="nama" placeholder="nama" class="form-control" required> 
                        </div>
                      </div><!-- /.form-group -->
<div class="form-group">
                        <label for="kode_mk" class="control-label col-lg-2">kode_mk</label>
                        <div class="col-lg-10">
                          <input type="text" name="kode_mk" placeholder="kode_mk" class="form-control" required> 
                        </div>
                      </div><!-- /.form-group -->
<div class="form-group">
                        <label for="nama_mk" class="control-label col-lg-2">nama_mk</label>
                        <div class="col-lg-10">
                          <input type="text" name="nama_mk" placeholder="nama_mk" class="form-control" required> 
                        </div>
                      </div><!-- /.form-group -->
<div class="form-group">
                        <label for="nama_kelas" class="control-label col-lg-2">nama_kelas</label>
                        <div class="col-lg-10">
                          <input type="text" name="nama_kelas" placeholder="nama_kelas" class="form-control" required> 
                        </div>
                      </div><!-- /.form-group -->
<div class="form-group">
                        <label for="semester" class="control-label col-lg-2">semester</label>
                        <div class="col-lg-10">
                          <input type="text" name="semester" placeholder="semester" class="form-control" required> 
                        </div>
                      </div><!-- /.form-group -->
<div class="form-group">
                        <label for="kode_jurusan" class="control-label col-lg-2">kode_jurusan</label>
                        <div class="col-lg-10">
                          <input type="text" name="kode_jurusan" placeholder="kode_jurusan" class="form-control" required> 
                        </div>
                      </div><!-- /.form-group -->
<div class="form-group">
                        <label for="status_error" class="control-label col-lg-2">status_error</label>
                        <div class="col-lg-10">
                          <input type="text" name="status_error" placeholder="status_error" class="form-control" > 
                        </div>
                      </div><!-- /.form-group -->
<div class="form-group">
                        <label for="keterangan" class="control-label col-lg-2">keterangan</label>
                        <div class="col-lg-10">
                          <input type="text" name="keterangan" placeholder="keterangan" class="form-control" > 
                        </div>
                      </div><!-- /.form-group -->
<div class="form-group">
                        <label for="status_krs" class="control-label col-lg-2">status_krs</label>
                        <div class="col-lg-10">
                          <input type="text" name="status_krs" placeholder="status_krs" class="form-control" required> 
                        </div>
                      </div><!-- /.form-group -->

                      
                      <div class="form-group">
                        <label for="tags" class="control-label col-lg-2">&nbsp;</label>
                        <div class="col-lg-10">
                          <input type="submit" class="btn btn-primary btn-flat" value="submit">
                        </div>
                      </div><!-- /.form-group -->
                    </form>
 <a href="<?=base_index();?>ajuan-krs" class="btn btn-success btn-flat"><i class="fa fa-step-backward"></i> Kembali</a>
          
                  </div>
                  </div>
              </div>
</div>
                  
                </section><!-- /.content -->
        
            