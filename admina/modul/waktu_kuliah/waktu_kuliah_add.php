
           
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                     Waktu Kuliah
                    </h1>
                           <ol class="breadcrumb">
                        <li><a href="<?=base_index();?>"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li><a href="<?=base_index();?>waktu-kuliah">Waktu Kuliah</a></li>
                        <li class="active">Tambah Waktu Kuliah</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
<div class="row">
    <div class="col-lg-12"> 
        <div class="box box-solid box-primary">
                                 <div class="box-header">
                                    <h3 class="box-title">Tambah Waktu Kuliah</h3>
                                    <div class="box-tools pull-right">
                                        <button class="btn btn-info btn-sm" data-widget="collapse"><i class="fa fa-minus"></i></button>
                                        <button class="btn btn-info btn-sm" data-widget="remove"><i class="fa fa-times"></i></button>
                                    </div>
                                </div>

                  <div class="box-body">
                     <form id="input" method="post" class="form-horizontal foto_banyak" action="<?=base_admin();?>modul/waktu_kuliah/waktu_kuliah_action.php?act=in">
                      <div class="form-group">
                        <label for="Waktu Kuliah" class="control-label col-lg-2">Waktu Kuliah</label>
                        <div class="col-lg-10">
                          <input type="text" name="wkt_kul_deskripsi" placeholder="Waktu Kuliah" class="form-control" required> 
                        </div>
                      </div><!-- /.form-group -->
<div class="form-group">
                        <label for="Sesi" class="control-label col-lg-2">Sesi</label>
                        <div class="col-lg-10">
                          <input type="text" data-rule-number="true" name="wkt_kul_sesi" placeholder="Sesi" class="form-control" > 
                        </div>
                      </div><!-- /.form-group -->
<div class="form-group">
                        <label for="pagi_sore" class="control-label col-lg-2">Kelas Pagi / Sore</label>
                        <div class="col-lg-10">
            <select name="pagi_sore" data-placeholder="Pilih Kelas Pagi/Sore? ..." class="form-control chzn-select" tabindex="2" >
               <option value="Pagi">Pagi</option>
               <option value="Sore">Sore</option>
              </select>
                        </div>
                      </div><!-- /.form-group -->
                      
                      <div class="form-group">
                        <label for="tags" class="control-label col-lg-2">&nbsp;</label>
                        <div class="col-lg-10">
                          <input type="submit" class="btn btn-primary btn-flat" value="submit">
                        </div>
                      </div><!-- /.form-group -->
                    </form>
 <a href="<?=base_index();?>waktu-kuliah" class="btn btn-success btn-flat"><i class="fa fa-step-backward"></i> Kembali</a>
          
                  </div>
                  </div>
              </div>
</div>
                  
                </section><!-- /.content -->
        
            