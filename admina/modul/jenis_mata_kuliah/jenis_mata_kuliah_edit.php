

                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                      Jenis Mata Kuliah
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="<?=base_index();?>"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li><a href="<?=base_index();?>jenis-mata-kuliah">Jenis Mata Kuliah</a></li>
                        <li class="active">Edit Jenis Mata Kuliah</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
<div class="row">
    <div class="col-lg-12">
        <div class="box box-solid box-primary">
                                   <div class="box-header">
                                    <h3 class="box-title">Edit Jenis Mata Kuliah</h3>
                                    <div class="box-tools pull-right">
                                        <button class="btn btn-info btn-sm" data-widget="collapse"><i class="fa fa-minus"></i></button>
                                        <button class="btn btn-info btn-sm" data-widget="remove"><i class="fa fa-times"></i></button>
                                    </div>
                                </div>

                  <div class="box-body">
                     <form id="update" method="post" class="form-horizontal" action="<?=base_admin();?>modul/jenis_mata_kuliah/jenis_mata_kuliah_action.php?act=up">
                      <div class="form-group">
                        <label for="Kode Jenis Mata Kuliah" class="control-label col-lg-2">Kode Jenis Mata Kuliah</label>
                        <div class="col-lg-10">
                          <input type="text" name="jns_matkul_kode" value="<?=$data_edit->jns_matkul_kode;?>" class="form-control" required> 
                        </div>
                      </div><!-- /.form-group -->
<div class="form-group">
                        <label for="Deskripsi Jenis Mata Kuliah" class="control-label col-lg-2">Deskripsi Jenis Mata Kuliah</label>
                        <div class="col-lg-10">
                          <input type="text" name="jns_matkul_deskripsi" value="<?=$data_edit->jns_matkul_deskripsi;?>" class="form-control" required> 
                        </div>
                      </div><!-- /.form-group -->

                      <input type="hidden" name="id" value="<?=$data_edit->jns_matkul_id;?>">
                      <div class="form-group">
                        <label for="tags" class="control-label col-lg-2">&nbsp;</label>
                        <div class="col-lg-10">
                          <input type="submit" class="btn btn-primary btn-flat" value="submit">
                        </div>
                      </div><!-- /.form-group -->
                    </form>
                    <a href="<?=base_index();?>jenis-mata-kuliah" class="btn btn-success btn-flat"><i class="fa fa-step-backward"></i> Kembali</a>
          
                  </div>
                  </div>
              </div>
</div>
                  
                </section><!-- /.content -->
        
 