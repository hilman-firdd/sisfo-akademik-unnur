

                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                      Log History Grade
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="<?=base_index();?>"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li><a href="<?=base_index();?>log-history-grade">Log History Grade</a></li>
                        <li class="active">Edit Log History Grade</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
<div class="row">
    <div class="col-lg-12">
        <div class="box box-solid box-primary">
                                   <div class="box-header">
                                    <h3 class="box-title">Edit Log History Grade</h3>
                                    <div class="box-tools pull-right">
                                        <button class="btn btn-info btn-sm" data-widget="collapse"><i class="fa fa-minus"></i></button>
                                        <button class="btn btn-info btn-sm" data-widget="remove"><i class="fa fa-times"></i></button>
                                    </div>
                                </div>

                  <div class="box-body">
                     <form id="update" method="post" class="form-horizontal" action="<?=base_admin();?>modul/log_history_grade/log_history_grade_action.php?act=up">
                      <div class="form-group">
                        <label for="log_user_id" class="control-label col-lg-2">log_user_id</label>
                        <div class="col-lg-10">
                          <input type="text" name="log_user_id" value="<?=$data_edit->log_user_id;?>" class="form-control" > 
                        </div>
                      </div><!-- /.form-group -->
<div class="form-group">
                        <label for="log_waktu" class="control-label col-lg-2">log_waktu</label>
                        <div class="col-lg-10">
                          <input type="text" name="log_waktu" value="<?=$data_edit->log_waktu;?>" class="form-control" > 
                        </div>
                      </div><!-- /.form-group -->
<div class="form-group">
                        <label for="log_aktifitas" class="control-label col-lg-2">log_aktifitas</label>
                        <div class="col-lg-10">
                          <input type="text" name="log_aktifitas" value="<?=$data_edit->log_aktifitas;?>" class="form-control" > 
                        </div>
                      </div><!-- /.form-group -->

                      <input type="hidden" name="id" value="<?=$data_edit->log_id;?>">
                      <div class="form-group">
                        <label for="tags" class="control-label col-lg-2">&nbsp;</label>
                        <div class="col-lg-10">
                          <input type="submit" class="btn btn-primary btn-flat" value="submit">
                        </div>
                      </div><!-- /.form-group -->
                    </form>
                    <a href="<?=base_index();?>log-history-grade" class="btn btn-success btn-flat"><i class="fa fa-step-backward"></i> Kembali</a>
          
                  </div>
                  </div>
              </div>
</div>
                  
                </section><!-- /.content -->
        
 