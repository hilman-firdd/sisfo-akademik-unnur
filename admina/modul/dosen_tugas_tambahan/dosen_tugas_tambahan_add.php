
           
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                     Dosen Tugas Tambahan
                    </h1>
                           <ol class="breadcrumb">
                        <li><a href="<?=base_index();?>"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li><a href="<?=base_index();?>dosen-tugas-tambahan">Dosen Tugas Tambahan</a></li>
                        <li class="active">Tambah Dosen Tugas Tambahan</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
<div class="row">
    <div class="col-lg-12"> 
        <div class="box box-solid box-primary">
                                 <div class="box-header">
                                    <h3 class="box-title">Tambah Dosen Tugas Tambahan</h3>
                                    <div class="box-tools pull-right">
                                        <button class="btn btn-info btn-sm" data-widget="collapse"><i class="fa fa-minus"></i></button>
                                        <button class="btn btn-info btn-sm" data-widget="remove"><i class="fa fa-times"></i></button>
                                    </div>
                                </div>

                  <div class="box-body">
                     <form id="input" method="post" class="form-horizontal foto_banyak" action="<?=base_admin();?>modul/dosen_tugas_tambahan/dosen_tugas_tambahan_action.php?act=in">
                      <div class="form-group">
                        <label for="Dosen" class="control-label col-lg-2">Dosen</label>
                        <div class="col-lg-10">
                          <select name="dos_jab_nidn" data-placeholder="Pilih Dosen ..." class="form-control chzn-select" tabindex="2" required>
               <option value=""></option>
               <?php foreach ($db->fetch_all("m_dosen") as $isi) {
                  echo "<option value='$isi->dosen_id'>$isi->dosen_nama</option>";
               } ?>
              </select>
                        </div>
                      </div><!-- /.form-group -->
<div class="form-group">
                        <label for="Tugas Tambahan" class="control-label col-lg-2">Tugas Tambahan</label>
                        <div class="col-lg-10">
                          <select name="dos_jabatan" data-placeholder="Pilih Tugas Tambahan ..." class="form-control chzn-select" tabindex="2" required>
               <option value=""></option>
               <?php foreach ($db->fetch_all("m_tgs_tambahan") as $isi) {
                  echo "<option value='$isi->tgs_tambahan_id'>$isi->tgs_tambahan_deskripsi</option>";
               } ?>
              </select>
                        </div>
                      </div><!-- /.form-group -->
<div class="form-group">
                        <label for="Jurusan" class="control-label col-lg-2">Jurusan</label>
                        <div class="col-lg-10">
                          <select name="dos_kode_jurusan" data-placeholder="Pilih Jurusan ..." class="form-control chzn-select" tabindex="2" required>
               <option value=""></option>
               <?php foreach ($db->fetch_all("jurusan") as $isi) {
                  echo "<option value='$isi->kode_jurusan'>$isi->nama_jurusan</option>";
               } ?>
              </select>
                        </div>
                      </div><!-- /.form-group -->
<div class="form-group">
                        <label for="Status Tugas Tambahan" class="control-label col-lg-2">Status Tugas Tambahan</label>
                        <div class="col-lg-10">
                          <input name="dos_status_jabatan" class="make-switch" type="checkbox" checked>
                        </div>
                      </div><!-- /.form-group -->

                      
                      <div class="form-group">
                        <label for="tags" class="control-label col-lg-2">&nbsp;</label>
                        <div class="col-lg-10">
                          <input type="submit" class="btn btn-primary btn-flat" value="submit">
                        </div>
                      </div><!-- /.form-group -->
                    </form>
 <a href="<?=base_index();?>dosen-tugas-tambahan" class="btn btn-success btn-flat"><i class="fa fa-step-backward"></i> Kembali</a>
          
                  </div>
                  </div>
              </div>
</div>
                  
                </section><!-- /.content -->
        
            