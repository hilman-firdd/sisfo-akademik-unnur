

                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                     Dosen Tugas Tambahan
                    </h1>
                   <ol class="breadcrumb">
                        <li><a href="<?=base_index();?>"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li><a href="<?=base_index();?>dosen-tugas-tambahan">Dosen Tugas Tambahan</a></li>
                        <li class="active">Detail Dosen Tugas Tambahan</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
<div class="row">
    <div class="col-lg-12">
        <div class="box box-solid box-primary">
                                   <div class="box-header">
                                    <h3 class="box-title">Detail Dosen Tugas Tambahan</h3>
                                    <div class="box-tools pull-right">
                                        <button class="btn btn-info btn-sm" data-widget="collapse"><i class="fa fa-minus"></i></button>
                                        <button class="btn btn-info btn-sm" data-widget="remove"><i class="fa fa-times"></i></button>
                                    </div>
                                </div>

                  <div class="box-body">
                   <form class="form-horizontal">
                      <div class="form-group">
                        <label for="Dosen" class="control-label col-lg-2">Dosen</label>
                        <div class="col-lg-10">
                          <?php foreach ($db->fetch_all("m_dosen") as $isi) {
                  if ($data_edit->dos_jab_nidn==$isi->dosen_id) {

                    echo "<input disabled class='form-control' type='text' value='$isi->dosen_nidn'>";
                  }
               } ?>
              
                        </div>
                      </div><!-- /.form-group -->
<div class="form-group">
                        <label for="Tugas Tambahan" class="control-label col-lg-2">Tugas Tambahan</label>
                        <div class="col-lg-10">
                          <?php foreach ($db->fetch_all("m_tgs_tambahan") as $isi) {
                  if ($data_edit->dos_jabatan==$isi->tgs_tambahan_id) {

                    echo "<input disabled class='form-control' type='text' value='$isi->tgs_tambahan_deskripsi'>";
                  }
               } ?>
              
                        </div>
                      </div><!-- /.form-group -->
<div class="form-group">
                        <label for="Jurusan" class="control-label col-lg-2">Jurusan</label>
                        <div class="col-lg-10">
                          <?php foreach ($db->fetch_all("jurusan") as $isi) {
                  if ($data_edit->dos_kode_jurusan==$isi->kode_jurusan) {

                    echo "<input disabled class='form-control' type='text' value='$isi->nama_jurusan'>";
                  }
               } ?>
              
                        </div>
                      </div><!-- /.form-group -->
<div class="form-group">
                        <label for="Status Tugas Tambahan" class="control-label col-lg-2">Status Tugas Tambahan</label>
                        <div class="col-lg-10">
                          <?php if ($data_edit->dos_status_jabatan=="Aktif") {
      ?>
      <input name="dos_status_jabatan" class="make-switch" disabled type="checkbox" checked>
      <?php
    } else {
      ?>
      <input name="dos_status_jabatan" class="make-switch" disabled type="checkbox">
      <?php
    }?>
                        </div>
                      </div><!-- /.form-group -->

                   
                    </form>
                    <a href="<?=base_index();?>dosen-tugas-tambahan" class="btn btn-success btn-flat"><i class="fa fa-step-backward"></i> Kembali</a>
          
                  </div>
                  </div>
              </div>
</div>
                  
                </section><!-- /.content -->
        
