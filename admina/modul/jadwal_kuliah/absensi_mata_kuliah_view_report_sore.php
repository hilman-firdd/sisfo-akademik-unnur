<?php 
include "../../inc/fpdf.php";
//include "fpdf.php";
$db = new PDO('mysql:host=localhost;dbname=ekomyanu_akademik','ekomyanu_root','ekomuh1801');

//ajar_dosen
//==========
$syarat =  $_GET['customvar']. "  ";
$syarat_id = $_GET['id']. "  ";
$q = $db->query("select * from view_0012_absensi_kuliah where view_0012_absensi_kuliah.kode_jurusan=". $syarat . " and view_0012_absensi_kuliah.id_record=" .$syarat_id .
 "and view_0012_absensi_kuliah.level LIKE '%Sore%' ");
$dt = $q->fetch(PDO::FETCH_OBJ);

if ($dt) {
if (substr($dt->semester, -1)==1) {$semester_var= " GANJIL";} else {$semester_var= " GENAP";}

$thn_akademik_var= substr($dt->semester, 0,4);
$thn_akademik_var= ($thn_akademik_var - 1) .'/'. $thn_akademik_var ;
$program_studi_var=  strtoupper($dt->nama_jurusan); 
$mulai_kul_var=date('l, d-m-Y');
$kode_mk = ($dt->kode_mk); 
$angka_semester = ($dt->angka_semester); 
$nama_mk = ($dt->nama_mk); 
$ruang = ($dt->nama_kelas); 
$kelas = "IF-".substr(($dt->nim_mhs),6,2);
$dosen = trim($dt->dosen_nama_sore); $dosen = ucwords($dosen);
$dosen_gelardepan = ($dt->dosen_gelardepan); $dosen_gelarblkg = ($dt->dosen_gelarblkg);

//krs
//===
//$q2 = $db->query('select * from view_0011_jadwal_kuliah where view_0011_jadwal_kuliah.kode_jurusan='. $syarat . ' and view_0011_jadwal_kuliah.id_record=' .$syarat_id);
//$dt2 = $q->fetch(PDO::FETCH_OBJ);

class myPDF extends FPDF{
    function header(){
        
        //$this->image('polban.png',10,6);
          //tipe font, format font bold/italic/underline, size font
        $this->Image('../../assets/logo_bw.png',10,6,-300);
        $this->SetFont('Times','B',12); $this->Cell(35,7,'' ,0,0,'L');  
        $this->SetFont('Times','B',12); $this->Cell(165,7,'KEGIATAN PROSES BELAJAR' ,0,0,'L');  
        $this->SetFont('Times','B',10); $this->Cell(25,7,'Mata Kuliah' ,0,0,'L'); 
        $this->SetFont('Times','',10); $this->Cell(5,7,' : ' ,0,0,'L'); 
        $this->SetFont('Times','',10); $this->Cell(100,7,$GLOBALS['kode_mk'].' -'. $GLOBALS['nama_mk'] ,0,1,'L'); 

        $this->SetFont('Times','B',12); $this->Cell(35,7,'' ,0,0,'L');  
        $this->SetFont('Times','B',12); $this->Cell(165,7,'DAFTAR HADIR T.A. '.$GLOBALS['thn_akademik_var'],0,0,'L');
        $this->SetFont('Times','B',10); $this->Cell(25,7,'Semester' ,0,0,'L'); 
        $this->SetFont('Times','',10); $this->Cell(5,7,' : ' ,0,0,'L'); 
        $this->SetFont('Times','',10); $this->Cell(100,7,$GLOBALS['angka_semester'] ,0,1,'L');

        $this->SetFont('Times','B',12); $this->Cell(35,7,'' ,0,0,'L');  
        $this->SetFont('Times','B',12); $this->Cell(165,7,'PROGRAM STUDI ' . $GLOBALS['program_studi_var'] ,0,0,'L');
        $this->SetFont('Times','B',10); $this->Cell(25,7,'Kelas/Ruang' ,0,0,'L'); 
        $this->SetFont('Times','',10); $this->Cell(5,7,' : ' ,0,0,'L'); 
        $this->SetFont('Times','',10); $this->Cell(100,7,$GLOBALS['kelas'].'/'.$GLOBALS['ruang'] ,0,1,'L');
        //$this->Cell(276,7,'PERKULIAHAN TGL '. $GLOBALS['mulai_kul_var'],0,0,'C');
        //$this->Ln();
        $this->SetFont('Times','B',12); $this->Cell(35,7,'' ,0,0,'L');  
        $this->SetFont('Times','B',12); $this->Cell(165,7,'(KELAS PAGI)' ,0,0,'L');
        $this->SetFont('Times','B',10); $this->Cell(25,7,'Dosen' ,0,0,'L'); 
        $this->SetFont('Times','',10); $this->Cell(5,7,' : ' ,0,0,'L'); 
        $this->SetFont('Times','',10); $this->Cell(100,7,$GLOBALS['dosen'] ,0,0,'L');
        
        $this->Ln(20);
        
        $this->SetFont('Times','B',9);
        
        $this->Cell(10,21,'No.',1,0,'C');  //kolom,baris, nama kolom tabel, , , center
        $this->Cell(100,21,'Nama Mahasiswa',1,0,'C');  //kolom,baris, nama kolom tabel, , , center
        $this->Cell(30,21,'NPM',1,0,'C');  //kolom,baris, nama kolom tabel, , , center
        
        
        $this->Cell(10,7,'1',1,0,'C');
        $this->Cell(10,7,'2',1,0,'C');
        $this->Cell(10,7,'3',1,0,'C');
        $this->Cell(10,7,'4',1,0,'C');
        $this->Cell(10,7,'5',1,0,'C');
        $this->Cell(10,7,'6',1,0,'C');
        $this->Cell(10,7,'7',1,0,'C');
        //$this->SetTextColor(128);  FONT BERWARNA GRAY
        $this->SetFillColor(230,230,230);
        $this->Cell(10,7,'UTS',1,0,'C');        
        $this->Cell(10,7,'9',1,0,'C');
        $this->Cell(10,7,'10',1,0,'C');
        $this->Cell(10,7,'11',1,0,'C');
        $this->Cell(10,7,'12',1,0,'C');
        $this->Cell(10,7,'13',1,0,'C');
        $this->Cell(10,7,'14',1,0,'C');
        $this->Cell(10,7,'15',1,0,'C');
        $this->Cell(10,7,'UAS',1,0,'C');
        $this->Cell(25,21,'Jml.Kehadiran',1,0,'C');  //kolom,baris, nama kolom tabel, , , center

        //dummy cell
        $this->Cell(10,7,'',0,1,'C');
        //dummy cell

        $this->Cell(140,7,'',0,0,'C');
        $this->Cell(160,7,'TANGGAL',1,0,'C');

        //dummy cell
        $this->Cell(10,7,'',0,1,'C');
        //dummy cell

        $this->Cell(140,7,'',0,'C');
        
        $this->Cell(10,7,'',1,0,'C');
        $this->Cell(10,7,'',1,0,'C');
        $this->Cell(10,7,'',1,0,'C');
        $this->Cell(10,7,'',1,0,'C');
        $this->Cell(10,7,'',1,0,'C');
        $this->Cell(10,7,'',1,0,'C');
        $this->Cell(10,7,'',1,0,'C');
        $this->Cell(10,7,'',1,0,'C');
        $this->Cell(10,7,'',1,0,'C');
        $this->Cell(10,7,'',1,0,'C');
        $this->Cell(10,7,'',1,0,'C');
        $this->Cell(10,7,'',1,0,'C');
        $this->Cell(10,7,'',1,0,'C');
        $this->Cell(10,7,'',1,0,'C');
        $this->Cell(10,7,'',1,0,'C');
        $this->Cell(10,7,'',1,1,'C'); 
         
        //$this->Ln();
        
    }
    function footer(){
        $this->SetY(-15);
        $this->SetFont('Arial','',8);
        $this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
    }
    function headerTable(){
        
        
    }
    function viewTable($db){
        
        $this->SetFont('Times','',9);
                       
        $stmt = $db->query('select * from view_0012_absensi_kuliah where kode_jurusan_krs ='. $GLOBALS['syarat'] . ' AND kode_mk_krs=' . $GLOBALS['kode_mk'] . " and level LIKE '%Sore%' ");
        
        //$stmt = $db->query('select * from view_0011_jadwal_kuliah where kode_jurusan ='. $GLOBALS['syarat']  AND );
        $i=1;
        while($data = $stmt->fetch(PDO::FETCH_OBJ)){
            $this->Cell(10,9,$i.'.',1,0,'C');
            $this->Cell(100,9,$data->nama_mhs,1,0,'L');
            $this->Cell(30,9,$data->nim_mhs,1,0,'C');
            
            //$this->SetFillColor(230,0,0);

            $this->Cell(10,9,'',1,0,'C');
            $this->Cell(10,9,'',1,0,'C');
            $this->Cell(10,9,'',1,0,'C');
            $this->Cell(10,9,'',1,0,'C');
            $this->Cell(10,9,'',1,0,'C');
            $this->Cell(10,9,'',1,0,'C');
            $this->Cell(10,9,'',1,0,'C');
            $this->Cell(10,9,'',1,0,'C');
            $this->Cell(10,9,'',1,0,'C');
            $this->Cell(10,9,'',1,0,'C');
            $this->Cell(10,9,'',1,0,'C');
            $this->Cell(10,9,'',1,0,'C');
            $this->Cell(10,9,'',1,0,'C');
            $this->Cell(10,9,'',1,0,'C');
            $this->Cell(10,9,'',1,0,'C');
            $this->Cell(10,9,'',1,0,'C'); 

            $this->Cell(25,9,'',1,1,'C'); 

            //$i++;    
            //$this->Ln();
            $i=$i+1;
        }
        $this->Ln(10);
        
        /*
        $this->Cell(25,5,'*Nilai Kepemimpinan --> untuk tenaga kependidikan PNS yang memiliki Jabatan Struktural atau Jabatan Fungsional Tertentu.',0,0,'L');   
        
        $this->Cell(170,5,' ',0,0,'L');   $this->Cell(25,5,'Bandung, Januari 2019',0,0,'L');   
        
        $this->Ln();
        $this->Cell(25,10,'* Penilaian perilaku adalah sebagai berikut: ',0,0,'L');  $this->Cell(170,10,' ',0,0,'L');$this->Cell(25,10,$jabatan_atasan.",",0,0,'L');
        
        $this->Ln();
        $this->Cell(10,10,'No.',1,0,'C');
        $this->Cell(35,10,'Nilai Rata-Rata.',1,0,'C');
        $this->Cell(35,10,'Nilai Capaian Perilaku (%)',1,0,'C');
        $this->Ln();
        $this->Cell(10,5,'1.',1,0,'C');
        $this->Cell(35,5,'85-91 ',1,0,'C');
        $this->Cell(35,5,'100',1,0,'C');
        $this->Ln();
        $this->Cell(10,5,'2.',1,0,'C');
        $this->Cell(35,5,'80-84,99  ',1,0,'C');
        $this->Cell(35,5,'90',1,0,'C');
        $this->Ln();
        
        $this->Cell(10,5,'3.',1,0,'C');               
        $this->Cell(35,5,'75-79,99  ',1,0,'C');                                        
        $this->Cell(35,5,'80',1,0,'C');                                        $this->Cell(115,5,' ',0,0,'L');   $this->Cell(25,5,$nama_atasan,0,0,'L');
        
        $this->Ln();
        $this->Cell(10,5,'4.',1,0,'C');
        $this->Cell(35,5,'70-74,99  ',1,0,'C');
        $this->Cell(35,5,'70',1,0,'C');                                        $this->Cell(115,5,' ',0,0,'L');   $this->Cell(25,5,"NIP ".$nip_atasan,0,0,'L'); 
        
        $this->Ln();
        $this->Cell(10,5,'5.',1,0,'C');
        $this->Cell(35,5,'65-69,99 ',1,0,'C');
        $this->Cell(35,5,'60',1,0,'C');
        $this->Ln();
        $this->Cell(10,5,'6.',1,0,'C');
        $this->Cell(35,5,'64,99 ke bawah',1,0,'C');
        $this->Cell(35,5,'50',1,0,'C');

        $this->TEXT(10,40,'Unit Kerja: '.$unitkerja); 

        */
        //Footer
        
        
    }
}

$pdf = new myPDF();
$pdf->AliasNbPages();
$pdf->AddPage('L','Legal',0); // Landscape, jenis kertas A4
$pdf->headerTable();
$pdf->viewTable($db);



$pdf->Output();
}
else
{
echo "
      <script type='text/javascript'>
      alert('Cetak Absensi tidak bisa dilakukan, belum ada Mahasiswa yang memilih mata kuliah ini...');
      
      </script>";
      
}
?>
