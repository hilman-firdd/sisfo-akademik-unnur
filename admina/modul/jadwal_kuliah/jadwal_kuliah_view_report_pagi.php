<?php 
include "../../inc/fpdf.php";
//include "fpdf.php";
$db = new PDO('mysql:host=localhost;dbname=ekomyanu_akademik','ekomyanu_root','ekomuh1801');

$syarat =  $_GET['customvar']. "  ";
$q = $db->query('select * from view_0011_jadwal_kuliah where view_0011_jadwal_kuliah.kode_jurusan='. $syarat. ' order by angka_semester asc, hari_nama desc');
$dt = $q->fetch(PDO::FETCH_OBJ);

if (substr($dt->semester, -1)==1) {$semester_var= " GANJIL";} else {$semester_var= " GENAP";}

$thn_akademik_var= substr($dt->semester, 0,4);
$thn_akademik_var= ($thn_akademik_var - 1) .'/'. $thn_akademik_var ;
$program_studi_var=  strtoupper($dt->nama_jurusan); 
$mulai_kul_var=date('l, d-m-Y');


class myPDF extends FPDF{
    function header(){
        
        //$this->image('polban.png',10,6);
        $this->SetFont('Times','B',12);  //tipe font, format font bold/italic/underline, size font
        $this->Cell(276,7,'JADWAL PERKULIAHAN SEMESTER'.$GLOBALS['semester_var']. ' TAHUN AKADEMIK '.$GLOBALS['thn_akademik_var'],0,0,'C');
        $this->Ln();
        
        $this->Cell(276,7,'PROGRAM STUDI ' . $GLOBALS['program_studi_var'] ,0,0,'C');
        $this->Ln();
        //$this->Cell(276,7,'PERKULIAHAN TGL '. $GLOBALS['mulai_kul_var'],0,0,'C');
        //$this->Ln();
        $this->Cell(276,7,'(KELAS PAGI)' ,0,0,'C');
        
        
        $this->Ln(20);
        
         $this->SetFont('Times','B',9);

        $this->Cell(30,10,'Hari',1,0,'C');  //kolom,baris, nama kolom tabel, , , center
        $this->Cell(15,10,'Semester',1,0,'C');  //kolom,baris, nama kolom tabel, , , center
        $this->Cell(35,10,'Waktu',1,0,'C');
        
        $this->Cell(10,10,'SKS',1,0,'C');
        $this->Cell(20,10,'Sandi MK',1,0,'C');
        $this->Cell(60,10,'Mata Kuliah',1,0,'C');
        $this->Cell(25,10,'Ruang',1,0,'C');
        $this->Cell(60,10,'Dosen',1,0,'C');
        $this->Cell(25,10,'Keterangan',1,0,'C');

        
        

        $this->Ln();
    }
    function footer(){
        $this->SetY(-15);
        $this->SetFont('Arial','',8);
        $this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
    }
    function headerTable(){
        
       
    }
    function viewTable($db){
        
        $this->SetFont('Times','',9);
                       
        $stmt = $db->query('select * from view_0011_jadwal_kuliah where kode_jurusan ='. $GLOBALS['syarat'] . ' order by angka_semester asc, hari_nama desc ');

        while($data = $stmt->fetch(PDO::FETCH_OBJ)){
            //$this->Cell(8,10,$i,1,0,'C');
            $this->Cell(30,10,$data->hari_nama,1,0,'C');
            $this->Cell(15,10,$data->angka_semester,1,0,'C');

            $this->Cell(35,10,$data->wkt_kul_deskripsi_pagi,1,0,'C');
            $this->Cell(10,10,$data->sks_tm,1,0,'C');
            $this->Cell(20,10,$data->kode_mk,1,0,'C');
            $this->Cell(60,10,$data->nama_mk,1,0,'L');
            $this->Cell(25,10,$data->nama_kelas,1,0,'C');
            $this->Cell(60,10,$data->dosen_nama_pagi,1,0,'L');
            $this->Cell(25,10,$data->keterangan,1,0,'L');
            
            //$i++;    
            $this->Ln();
        }
        $this->Ln(10);

        /*
        $this->Cell(25,5,'*Nilai Kepemimpinan --> untuk tenaga kependidikan PNS yang memiliki Jabatan Struktural atau Jabatan Fungsional Tertentu.',0,0,'L');   
        
        $this->Cell(170,5,' ',0,0,'L');   $this->Cell(25,5,'Bandung, Januari 2019',0,0,'L');   
        
        $this->Ln();
        $this->Cell(25,10,'* Penilaian perilaku adalah sebagai berikut: ',0,0,'L');  $this->Cell(170,10,' ',0,0,'L');$this->Cell(25,10,$jabatan_atasan.",",0,0,'L');
        
        $this->Ln();
        $this->Cell(10,10,'No.',1,0,'C');
        $this->Cell(35,10,'Nilai Rata-Rata.',1,0,'C');
        $this->Cell(35,10,'Nilai Capaian Perilaku (%)',1,0,'C');
        $this->Ln();
        $this->Cell(10,5,'1.',1,0,'C');
        $this->Cell(35,5,'85-91 ',1,0,'C');
        $this->Cell(35,5,'100',1,0,'C');
        $this->Ln();
        $this->Cell(10,5,'2.',1,0,'C');
        $this->Cell(35,5,'80-84,99  ',1,0,'C');
        $this->Cell(35,5,'90',1,0,'C');
        $this->Ln();
        
        $this->Cell(10,5,'3.',1,0,'C');               
        $this->Cell(35,5,'75-79,99  ',1,0,'C');                                        
        $this->Cell(35,5,'80',1,0,'C');                                        $this->Cell(115,5,' ',0,0,'L');   $this->Cell(25,5,$nama_atasan,0,0,'L');
        
        $this->Ln();
        $this->Cell(10,5,'4.',1,0,'C');
        $this->Cell(35,5,'70-74,99  ',1,0,'C');
        $this->Cell(35,5,'70',1,0,'C');                                        $this->Cell(115,5,' ',0,0,'L');   $this->Cell(25,5,"NIP ".$nip_atasan,0,0,'L'); 
        
        $this->Ln();
        $this->Cell(10,5,'5.',1,0,'C');
        $this->Cell(35,5,'65-69,99 ',1,0,'C');
        $this->Cell(35,5,'60',1,0,'C');
        $this->Ln();
        $this->Cell(10,5,'6.',1,0,'C');
        $this->Cell(35,5,'64,99 ke bawah',1,0,'C');
        $this->Cell(35,5,'50',1,0,'C');

        $this->TEXT(10,40,'Unit Kerja: '.$unitkerja); 

        */
        //Footer
        
        
    }
}

$pdf = new myPDF();
$pdf->AliasNbPages();
$pdf->AddPage('L','A4',0); // Landscape, jenis kertas A4
$pdf->headerTable();
$pdf->viewTable($db);



$pdf->Output();

?>
