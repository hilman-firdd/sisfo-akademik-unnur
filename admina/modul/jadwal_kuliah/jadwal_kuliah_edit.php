

                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                      Jadwal Kuliah
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="<?=base_index();?>"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li><a href="<?=base_index();?>jadwal-kuliah">Jadwal Kuliah</a></li>
                        <li class="active">Edit Jadwal Kuliah</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
<div class="row">
    <div class="col-lg-12">
        <div class="box box-solid box-primary">
                                   <div class="box-header">
                                    <h3 class="box-title">Edit Jadwal Kuliah</h3>
                                    <div class="box-tools pull-right">
                                        <button class="btn btn-info btn-sm" data-widget="collapse"><i class="fa fa-minus"></i></button>
                                        <button class="btn btn-info btn-sm" data-widget="remove"><i class="fa fa-times"></i></button>
                                    </div>
                                </div>

                  <div class="box-body">
                     <form id="update" method="post" class="form-horizontal" action="<?=base_admin();?>modul/jadwal_kuliah/jadwal_kuliah_action.php?act=up">
                      <div class="form-group">
                        <label for="Semester" class="control-label col-lg-2">Semester</label>
                        <div class="col-lg-10">
                          <input type="text" name="semester" value="<?=$data_edit->semester;?>" class="form-control" readonly > 
                        </div>
                      </div><!-- /.form-group -->
<div class="form-group">
                        <label for="NIDN Dosen Forlap Dikti" class="control-label col-lg-2">NIDN Dosen (Forlap Dikti)</label>
                        <div class="col-lg-10">
                          <input type="text" name="nidn" value="<?=$data_edit->nidn;?>" class="form-control" readonly> 
                        </div>
                      </div><!-- /.form-group -->
<div class="form-group">
                        <label for="Nama Dosen Forlap Dikti" class="control-label col-lg-2">Nama Dosen (Forlap Dikti)</label>
                        <div class="col-lg-10">
                          <input type="text" name="nama_dosen" value="<?=$data_edit->nama_dosen;?>" class="form-control" readonly> 
                        </div>
                      </div><!-- /.form-group -->
<div class="form-group">
                        <label for="Kode Mata Kuliah" class="control-label col-lg-2">Kode Mata Kuliah</label>
                        <div class="col-lg-10">
                          <input type="text" name="kode_mk" value="<?=$data_edit->kode_mk;?>" class="form-control" readonly> 
                        </div>
                      </div><!-- /.form-group -->
<div class="form-group">
                        <label for="Nama Mata Kuliah" class="control-label col-lg-2">Nama Mata Kuliah</label>
                        <div class="col-lg-10">
                          <input type="text" name="nama_mk" value="<?=$data_edit->nama_mk;?>" class="form-control" readonly> 
                        </div>
                      </div><!-- /.form-group -->
<div class="form-group">
                        <label for="Nama Kelas/Ruang" class="control-label col-lg-2">Nama Kelas/Ruang (Forlap Dikti)</label>
                        <div class="col-lg-10">
                          <input type="text" name="nama_kelas" value="<?=$data_edit->nama_kelas;?>" class="form-control" readonly> 
                        </div>
                      </div><!-- /.form-group -->
<div class="form-group">
                        <label for="Rencana Tatap Muka" class="control-label col-lg-2">Rencana Tatap Muka</label>
                        <div class="col-lg-10">
                          <input type="text" name="rencana_tatap_muka" value="<?=$data_edit->rencana_tatap_muka;?>" class="form-control" > 
                        </div>
                      </div><!-- /.form-group -->
<div class="form-group">
                        <label for="Tatap Muka Real" class="control-label col-lg-2">Tatap Muka Realisasi</label>
                        <div class="col-lg-10">
                          <input type="text" name="tatap_muka_real" value="<?=$data_edit->tatap_muka_real;?>" class="form-control" > 
                        </div>
                      </div><!-- /.form-group -->
<div class="form-group">
                        <label for="Jurusan" class="control-label col-lg-2">Jurusan</label>
                        <div class="col-lg-10">
                          <select name="kode_jurusan" data-placeholder="Pilih Jurusan..." class="form-control chzn-select" tabindex="2" >
               <option value=""></option>

               <?php foreach ($db->fetch_all("jurusan") as $isi) {

                  if ($data_edit->kode_jurusan==$isi->kode_jurusan) {
                    echo "<option value='$isi->kode_jurusan' selected>$isi->nama_jurusan</option>";
                  } else {
                  echo "<option value='$isi->kode_jurusan'>$isi->nama_jurusan</option>";
                    }
               } ?>
              </select>
                        </div>
                      </div><!-- /.form-group -->

<div class="form-group"> </div>
<div class="form-group"> </div>


<div class="form-group">
                        <label for="Hari" class="control-label col-lg-2">Hari (Jadwal Pagi)</label>
                        <div class="col-lg-10">
                          <select name="hari_id" data-placeholder="Pilih Hari Jadwal Pagi..." class="form-control chzn-select" tabindex="2" >
               <option value=""></option>
               <?php foreach ($db->fetch_all("m_nama_hari") as $isi) {

                  if ($data_edit->hari_id==$isi->hari_id) {
                    echo "<option value='$isi->hari_id' selected>$isi->hari_nama</option>";
                  } else {
                  echo "<option value='$isi->hari_id'>$isi->hari_nama</option>";
                    }
               } ?>
              </select>
                        </div>
                      </div><!-- /.form-group -->

<div class="form-group">
                        <label for="Ruang_Pagi" class="control-label col-lg-2">Ruang (Jadwal Pagi)</label>
                        <div class="col-lg-10">
                          <select name="ruang_pagi_id" data-placeholder="Pilih Ruang Jadwal Pagi..." class="form-control chzn-select" tabindex="2" >
               <option value=""></option>
               <?php foreach ($db->fetch_all("m_ruang") as $isi) {

                  if ($data_edit->ruang_pagi_id==$isi->ruang_id) {
                    echo "<option value='$isi->ruang_id' selected>$isi->ruang_deskripsi</option>";
                  } else {
                  echo "<option value='$isi->ruang_id'>$isi->ruang_deskripsi</option>";
                    }
               } ?>
              </select>
                        </div>
                      </div><!-- /.form-group -->                      

<div class="form-group">
                        <label for="Jadwal Pagi" class="control-label col-lg-2">Waktu (Jadwal Pagi)</label>
                        <div class="col-lg-10">
                          <select name="jadwal_pagi_id" data-placeholder="Pilih Jadwal Pagi..." class="form-control chzn-select" tabindex="2" >
               <option value=""></option>
               <?php foreach ($db->fetch_all("view_0007_waktu_kuliah_pagi") as $isi) {

                  if ($data_edit->jadwal_pagi_id==$isi->wkt_kul_id_pagi) {
                    echo "<option value='$isi->wkt_kul_id_pagi' selected>$isi->wkt_kul_deskripsi_pagi</option>";
                  } else {
                  echo "<option value='$isi->wkt_kul_id_pagi'>$isi->wkt_kul_deskripsi_pagi</option>";
                    }
               } ?>
              </select>
                        </div>
                      </div><!-- /.form-group -->

<div class="form-group">
                        <label for="Dosen Jadwal Pagi" class="control-label col-lg-2">Dosen (Jadwal Pagi)</label>
                        <div class="col-lg-10">
                          <select name="dosen_pagi_id" data-placeholder="Pilih Dosen Jadwal Pagi..." class="form-control chzn-select" tabindex="2" >
               <option value=""></option>
               <?php foreach ($db->fetch_all("m_dosen") as $isi) {

                  if ($data_edit->dosen_pagi_id==$isi->dosen_id) {
                    echo "<option value='$isi->dosen_id' selected>$isi->dosen_nama</option>";
                  } else {
                  echo "<option value='$isi->dosen_id'>$isi->dosen_nama</option>";
                    }
               } ?>
              </select>
                        </div>
                      </div><!-- /.form-group -->

<div class="form-group"> </div>
<div class="form-group"> </div>

<div class="form-group">
                        <label for="Hari_Sore" class="control-label col-lg-2">Hari (Jadwal Sore)</label>
                        <div class="col-lg-10">
                          <select name="hari_sore_id" data-placeholder="Pilih Hari Jadwal Sore..." class="form-control chzn-select" tabindex="2" >
               <option value=""></option>
               <?php foreach ($db->fetch_all("m_nama_hari") as $isi) {

                  if ($data_edit->hari_sore_id==$isi->hari_id) {
                    echo "<option value='$isi->hari_id' selected>$isi->hari_nama</option>";
                  } else {
                  echo "<option value='$isi->hari_id'>$isi->hari_nama</option>";
                    }
               } ?>
              </select>
                        </div>
                      </div><!-- /.form-group -->

<div class="form-group">
                        <label for="Ruang_Sore" class="control-label col-lg-2">Ruang (Jadwal Sore)</label>
                        <div class="col-lg-10">
                          <select name="ruang_sore_id" data-placeholder="Pilih Ruang Jadwal Sore..." class="form-control chzn-select" tabindex="2" >
               <option value=""></option>
               <?php foreach ($db->fetch_all("m_ruang") as $isi) {

                  if ($data_edit->ruang_sore_id==$isi->ruang_id) {
                    echo "<option value='$isi->ruang_id' selected>$isi->ruang_deskripsi</option>";
                  } else {
                  echo "<option value='$isi->ruang_id'>$isi->ruang_deskripsi</option>";
                    }
               } ?>
              </select>
                        </div>
                      </div><!-- /.form-group -->       

<div class="form-group">
                        <label for="Jadwal Sore" class="control-label col-lg-2">Waktu (Jadwal Sore)</label>
                        <div class="col-lg-10">
                          <select name="jadwal_sore_id" data-placeholder="Pilih Jadwal Sore..." class="form-control chzn-select" tabindex="2" >
               <option value=""></option>
               <?php foreach ($db->fetch_all("view_0008_waktu_kuliah_sore") as $isi) {

                  if ($data_edit->jadwal_sore_id==$isi->wkt_kul_id_sore) {
                    echo "<option value='$isi->wkt_kul_id_sore' selected>$isi->wkt_kul_deskripsi_sore</option>";
                  } else {
                  echo "<option value='$isi->wkt_kul_id_sore'>$isi->wkt_kul_deskripsi_sore</option>";
                    }
               } ?>
              </select>
                        </div>
                      </div><!-- /.form-group -->

<div class="form-group">
                        <label for="Dosen Jadwal Sore" class="control-label col-lg-2">Dosen (Jadwal Sore)</label>
                        <div class="col-lg-10">
                          <select name="dosen_sore_id" data-placeholder="Pilih Dosen Jadwal Sore..." class="form-control chzn-select" tabindex="2" >
               <option value=""></option>
               <?php foreach ($db->fetch_all("m_dosen") as $isi) {

                  if ($data_edit->dosen_sore_id==$isi->dosen_id) {
                    echo "<option value='$isi->dosen_id' selected>$isi->dosen_nama</option>";
                  } else {
                  echo "<option value='$isi->dosen_id'>$isi->dosen_nama</option>";
                    }
               } ?>
              </select>
                        </div>
                      </div><!-- /.form-group -->

<div class="form-group"> </div>
<div class="form-group"> </div>

<div class="form-group">
                        <label for="Keterangan" class="control-label col-lg-2">Keterangan</label>
                        <div class="col-lg-10">
                          <input type="text" name="keterangan" value="<?=$data_edit->keterangan;?>" class="form-control" > 
                        </div>
                      </div><!-- /.form-group -->

                      <input type="hidden" name="id" value="<?=$data_edit->id;?>">

<div class="alert alert-danger update" style="display:none">   
    <button type="button" class="close" data-dismiss="alert">&times;</button>
    <strong>Maaf, Dosen yang Anda input sudah dijadwalkan di hari dan waktu yang sama! 
    </strong> 
</div>

                      <div class="form-group">
                        <label for="tags" class="control-label col-lg-2">&nbsp;</label>
                        <div class="col-lg-10">
                          <input type="submit" class="btn btn-primary btn-flat" value="submit">
                        </div>
                      </div><!-- /.form-group -->
                    </form>
                    <a href="javascript:window.history.go(-1);" class="btn btn-success btn-flat"><i class="fa fa-step-backward"></i> Kembali</a>
          
                  </div>
                  </div>
              </div>
</div>
                  
                </section><!-- /.content -->
        
 