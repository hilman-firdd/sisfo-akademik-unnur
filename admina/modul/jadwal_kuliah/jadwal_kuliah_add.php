
           
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                     Jadwal Kuliah
                    </h1>
                           <ol class="breadcrumb">
                        <li><a href="<?=base_index();?>"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li><a href="<?=base_index();?>jadwal-kuliah">Jadwal Kuliah</a></li>
                        <li class="active">Tambah Jadwal Kuliah</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
<div class="row">
    <div class="col-lg-12"> 
        <div class="box box-solid box-primary">
                                 <div class="box-header">
                                    <h3 class="box-title">Tambah Jadwal Kuliah</h3>
                                    <div class="box-tools pull-right">
                                        <button class="btn btn-info btn-sm" data-widget="collapse"><i class="fa fa-minus"></i></button>
                                        <button class="btn btn-info btn-sm" data-widget="remove"><i class="fa fa-times"></i></button>
                                    </div>
                                </div>

                  <div class="box-body">
                     <form id="input" method="post" class="form-horizontal foto_banyak" action="<?=base_admin();?>modul/jadwal_kuliah/jadwal_kuliah_action.php?act=in">
                      <div class="form-group">
                        <label for="Semester" class="control-label col-lg-2">Semester</label>
                        <div class="col-lg-10">
                          <input type="text" name="semester" placeholder="Semester" class="form-control" > 
                        </div>
                      </div><!-- /.form-group -->
<div class="form-group">
                        <label for="NIDN Dosen Forlap Dikti" class="control-label col-lg-2">NIDN Dosen Forlap Dikti</label>
                        <div class="col-lg-10">
                          <input type="text" name="nidn" placeholder="NIDN Dosen Forlap Dikti" class="form-control" > 
                        </div>
                      </div><!-- /.form-group -->
<div class="form-group">
                        <label for="Nama Dosen Forlap Dikti" class="control-label col-lg-2">Nama Dosen Forlap Dikti</label>
                        <div class="col-lg-10">
                          <input type="text" name="nama_dosen" placeholder="Nama Dosen Forlap Dikti" class="form-control" > 
                        </div>
                      </div><!-- /.form-group -->
<div class="form-group">
                        <label for="Kode Mata Kuliah" class="control-label col-lg-2">Kode Mata Kuliah</label>
                        <div class="col-lg-10">
                          <input type="text" name="kode_mk" placeholder="Kode Mata Kuliah" class="form-control" > 
                        </div>
                      </div><!-- /.form-group -->
<div class="form-group">
                        <label for="Nama Mata Kuliah" class="control-label col-lg-2">Nama Mata Kuliah</label>
                        <div class="col-lg-10">
                          <input type="text" name="nama_mk" placeholder="Nama Mata Kuliah" class="form-control" > 
                        </div>
                      </div><!-- /.form-group -->
<div class="form-group">
                        <label for="Nama Kelas/Ruang" class="control-label col-lg-2">Nama Kelas/Ruang</label>
                        <div class="col-lg-10">
                          <input type="text" name="nama_kelas" placeholder="Nama Kelas/Ruang" class="form-control" > 
                        </div>
                      </div><!-- /.form-group -->
<div class="form-group">
                        <label for="Rencana Tatap Muka" class="control-label col-lg-2">Rencana Tatap Muka</label>
                        <div class="col-lg-10">
                          <input type="text" name="rencana_tatap_muka" placeholder="Rencana Tatap Muka" class="form-control" > 
                        </div>
                      </div><!-- /.form-group -->
<div class="form-group">
                        <label for="Tatap Muka Real" class="control-label col-lg-2">Tatap Muka Real</label>
                        <div class="col-lg-10">
                          <input type="text" name="tatap_muka_real" placeholder="Tatap Muka Real" class="form-control" > 
                        </div>
                      </div><!-- /.form-group -->
<div class="form-group">
                        <label for="Jurusan" class="control-label col-lg-2">Jurusan</label>
                        <div class="col-lg-10">
                          <select name="kode_jurusan" data-placeholder="Pilih Jurusan ..." class="form-control chzn-select" tabindex="2" >
               <option value=""></option>
               <?php foreach ($db->fetch_all("jurusan") as $isi) {
                  echo "<option value='$isi->id'>$isi->nama_jurusan</option>";
               } ?>
              </select>
                        </div>
                      </div><!-- /.form-group -->
<div class="form-group">
                        <label for="Keterangan" class="control-label col-lg-2">Keterangan</label>
                        <div class="col-lg-10">
                          <input type="text" name="keterangan" placeholder="Keterangan" class="form-control" > 
                        </div>
                      </div><!-- /.form-group -->
<div class="form-group">
                        <label for="Jadwal Pagi" class="control-label col-lg-2">Jadwal Pagi</label>
                        <div class="col-lg-10">
                          <select name="jadwal_pagi_id" data-placeholder="Pilih Jadwal Pagi ..." class="form-control chzn-select" tabindex="2" >
               <option value=""></option>
               <?php foreach ($db->fetch_all("m_waktu_kuliah") as $isi) {
                  echo "<option value='$isi->wkt_kul_id'>$isi->wkt_kul_deskripsi</option>";
               } ?>
              </select>
                        </div>
                      </div><!-- /.form-group -->


<div class="form-group">
                        <label for="Dosen Jadwal Pagi" class="control-label col-lg-2">Dosen Jadwal Pagi</label>
                        <div class="col-lg-10">
                          <select name="dosen_pagi_id" data-placeholder="Pilih Dosen Jadwal Pagi ..." class="form-control chzn-select" tabindex="2" >
               <option value=""></option>
               <?php foreach ($db->fetch_all("m_dosen") as $isi) {
                  echo "<option value='$isi->dosen_id'>$isi->dosen_nama</option>";
               } ?>
              </select>
                        </div>
                      </div><!-- /.form-group -->
                                            
<div class="form-group">
                        <label for="Jadwal Sore" class="control-label col-lg-2">Jadwal Sore</label>
                        <div class="col-lg-10">
                          <select name="jadwal_sore_id" data-placeholder="Pilih Jadwal Sore ..." class="form-control chzn-select" tabindex="2" >
               <option value=""></option>
               <?php foreach ($db->fetch_all("m_waktu_kuliah") as $isi) {
                  echo "<option value='$isi->wkt_kul_id'>$isi->wkt_kul_deskripsi</option>";
               } ?>
              </select>
                        </div>
                      </div><!-- /.form-group -->

<div class="form-group">
                        <label for="Dosen Jadwal Sore" class="control-label col-lg-2">Dosen Jadwal Sore</label>
                        <div class="col-lg-10">
                          <select name="dosen_sore_id" data-placeholder="Pilih Dosen Jadwal Sore ..." class="form-control chzn-select" tabindex="2" >
               <option value=""></option>
               <?php foreach ($db->fetch_all("m_dosen") as $isi) {
                  echo "<option value='$isi->dosen_id'>$isi->dosen_nama</option>";
               } ?>
              </select>
                        </div>
                      </div><!-- /.form-group -->
<div class="form-group">
                        <label for="Hari" class="control-label col-lg-2">Hari</label>
                        <div class="col-lg-10">
                          <select name="hari_id" data-placeholder="Pilih Hari ..." class="form-control chzn-select" tabindex="2" >
               <option value=""></option>
               <?php foreach ($db->fetch_all("m_nama_hari") as $isi) {
                  echo "<option value='$isi->hari_id'>$isi->hari_nama</option>";
               } ?>
              </select>
                        </div>
                      </div><!-- /.form-group -->

                      
                      <div class="form-group">
                        <label for="tags" class="control-label col-lg-2">&nbsp;</label>
                        <div class="col-lg-10">
                          <input type="submit" class="btn btn-primary btn-flat" value="submit">
                        </div>
                      </div><!-- /.form-group -->
                    </form>
 <a href="<?=base_index();?>jadwal-kuliah" class="btn btn-success btn-flat"><i class="fa fa-step-backward"></i> Kembali</a>
          
                  </div>
                  </div>
              </div>
</div>
                  
                </section><!-- /.content -->
        
            