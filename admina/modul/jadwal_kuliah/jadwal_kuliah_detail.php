

                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                     Jadwal Kuliah
                    </h1>
                   <ol class="breadcrumb">
                        <li><a href="<?=base_index();?>"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li><a href="<?=base_index();?>jadwal-kuliah">Jadwal Kuliah</a></li>
                        <li class="active">Detail Jadwal Kuliah</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
<div class="row">
    <div class="col-lg-12">
        <div class="box box-solid box-primary">
                                   <div class="box-header">
                                    <h3 class="box-title">Detail Jadwal Kuliah</h3>
                                    <div class="box-tools pull-right">
                                        <button class="btn btn-info btn-sm" data-widget="collapse"><i class="fa fa-minus"></i></button>
                                        <button class="btn btn-info btn-sm" data-widget="remove"><i class="fa fa-times"></i></button>
                                    </div>
                                </div>

                  <div class="box-body">
                   <form class="form-horizontal">
                      <div class="form-group">
                        <label for="Semester" class="control-label col-lg-2">Semester</label>
                        <div class="col-lg-10">
                          <input type="text" disabled="" value="<?=$data_edit->semester;?>" class="form-control">
                        </div>
                      </div><!-- /.form-group -->
<div class="form-group">
                        <label for="NIDN Dosen Forlap Dikti" class="control-label col-lg-2">NIDN Dosen Forlap Dikti</label>
                        <div class="col-lg-10">
                          <input type="text" disabled="" value="<?=$data_edit->nidn;?>" class="form-control">
                        </div>
                      </div><!-- /.form-group -->
<div class="form-group">
                        <label for="Nama Dosen Forlap Dikti" class="control-label col-lg-2">Nama Dosen Forlap Dikti</label>
                        <div class="col-lg-10">
                          <input type="text" disabled="" value="<?=$data_edit->nama_dosen;?>" class="form-control">
                        </div>
                      </div><!-- /.form-group -->
<div class="form-group">
                        <label for="Kode Mata Kuliah" class="control-label col-lg-2">Kode Mata Kuliah</label>
                        <div class="col-lg-10">
                          <input type="text" disabled="" value="<?=$data_edit->kode_mk;?>" class="form-control">
                        </div>
                      </div><!-- /.form-group -->
<div class="form-group">
                        <label for="Nama Mata Kuliah" class="control-label col-lg-2">Nama Mata Kuliah</label>
                        <div class="col-lg-10">
                          <input type="text" disabled="" value="<?=$data_edit->nama_mk;?>" class="form-control">
                        </div>
                      </div><!-- /.form-group -->
<div class="form-group">
                        <label for="Nama Kelas/Ruang" class="control-label col-lg-2">Nama Kelas/Ruang</label>
                        <div class="col-lg-10">
                          <input type="text" disabled="" value="<?=$data_edit->nama_kelas;?>" class="form-control">
                        </div>
                      </div><!-- /.form-group -->
<div class="form-group">
                        <label for="Rencana Tatap Muka" class="control-label col-lg-2">Rencana Tatap Muka</label>
                        <div class="col-lg-10">
                          <input type="text" disabled="" value="<?=$data_edit->rencana_tatap_muka;?>" class="form-control">
                        </div>
                      </div><!-- /.form-group -->
<div class="form-group">
                        <label for="Tatap Muka Real" class="control-label col-lg-2">Tatap Muka Real</label>
                        <div class="col-lg-10">
                          <input type="text" disabled="" value="<?=$data_edit->tatap_muka_real;?>" class="form-control">
                        </div>
                      </div><!-- /.form-group -->
<div class="form-group">
                        <label for="Jurusan" class="control-label col-lg-2">Jurusan</label>
                        <div class="col-lg-10">
                          <?php foreach ($db->fetch_all("jurusan") as $isi) {
                  if ($data_edit->kode_jurusan==$isi->kode_jurusan) {

                    echo "<input disabled class='form-control' type='text' value='$isi->nama_jurusan'>";
                  }
               } ?>
              
                        </div>
                      </div><!-- /.form-group -->
<div class="form-group">
                        <label for="Keterangan" class="control-label col-lg-2">Keterangan</label>
                        <div class="col-lg-10">
                          <input type="text" disabled="" value="<?=$data_edit->keterangan;?>" class="form-control">
                        </div>
                      </div><!-- /.form-group -->
<div class="form-group">
                        <label for="Jadwal Pagi" class="control-label col-lg-2">Jadwal Pagi</label>
                        <div class="col-lg-10">
                          <?php foreach ($db->fetch_all("m_waktu_kuliah") as $isi) {
                  if ($data_edit->jadwal_pagi_id==$isi->wkt_kul_id) {

                    echo "<input disabled class='form-control' type='text' value='$isi->wkt_kul_deskripsi'>";
                  }
               } ?>
              
                        </div>
                      </div><!-- /.form-group -->
<div class="form-group">
                        <label for="Jadwal Sore" class="control-label col-lg-2">Jadwal Sore</label>
                        <div class="col-lg-10">
                          <?php foreach ($db->fetch_all("m_waktu_kuliah") as $isi) {
                  if ($data_edit->jadwal_sore_id==$isi->wkt_kul_id) {

                    echo "<input disabled class='form-control' type='text' value='$isi->wkt_kul_deskripsi'>";
                  }
               } ?>
              
                        </div>
                      </div><!-- /.form-group -->
<div class="form-group">
                        <label for="Dosen Jadwal Pagi" class="control-label col-lg-2">Dosen Jadwal Pagi</label>
                        <div class="col-lg-10">
                          <?php foreach ($db->fetch_all("m_dosen") as $isi) {
                  if ($data_edit->dosen_pagi_id==$isi->dosen_id) {

                    echo "<input disabled class='form-control' type='text' value='$isi->dosen_nama'>";
                  }
               } ?>
              
                        </div>
                      </div><!-- /.form-group -->
<div class="form-group">
                        <label for="Dosen Jadwal Sore" class="control-label col-lg-2">Dosen Jadwal Sore</label>
                        <div class="col-lg-10">
                          <?php foreach ($db->fetch_all("m_dosen") as $isi) {
                  if ($data_edit->dosen_sore_id==$isi->dosen_id) {

                    echo "<input disabled class='form-control' type='text' value='$isi->dosen_nama'>";
                  }
               } ?>
              
                        </div>
                      </div><!-- /.form-group -->
<div class="form-group">
                        <label for="Hari" class="control-label col-lg-2">Hari</label>
                        <div class="col-lg-10">
                          <?php foreach ($db->fetch_all("m_nama_hari") as $isi) {
                  if ($data_edit->hari_id==$isi->hari_id) {

                    echo "<input disabled class='form-control' type='text' value='$isi->hari_nama'>";
                  }
               } ?>
              
                        </div>
                      </div><!-- /.form-group -->

                   
                    </form>
                    

                    <a href="javascript:window.history.go(-1);" class="btn btn-success btn-flat"><i class="fa fa-step-backward" ></i> Kembali</a>
                    

                  </div>
                  </div>
              </div>
</div>
                  
                </section><!-- /.content -->
        
