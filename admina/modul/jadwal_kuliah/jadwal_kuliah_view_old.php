
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        <?php
                          if ($path_id=="") {
                            include "jadwal_kuliah_view.php";
                          }
                          
                        ?>
                        Manage Jadwal Kuliah <b><?=$db->fetch_single_row('jurusan','kode_jurusan',$path_id)->nama_jurusan?></b>
                        
                    </h1>
                        <ol class="breadcrumb">
                        <li><a href="<?=base_index();?>"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li><a href="<?=base_index();?>jadwal-kuliah">Jadwal Kuliah</a></li>
                        <li class="active">Jadwal Kuliah List</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="box">
                                <div class="box-header">
                                <h3 class="box-title">List Jadwal Kuliah</h3>
                                </div><!-- /.box-header -->
                                <div class="box-body table-responsive">
                                    <table id="dtb_jadwal_kuliah" class="table table-bordered table-striped">

                             
                          <thead>
                          <tr>
                          <th>ID</th>
                          <th>Semester</th>
                          <th>Semester (1-8)</th>
                          <th>Kode Mata Kuliah</th>
                          <th>Nama Mata Kuliah</th>
                          <th>SKS</th>
                          
                          <th>Hari (Pagi)</th>
                          <th>Ruang (Pagi)</th>
                          <th>Waktu Kuliah (Pagi)</th>                          
                          <th>Nama Dosen Kuliah (Pagi)</th>                          
                          
                          <th>Hari (Sore)</th>
                          <th>Ruang (Sore)</th>
                          <th>Waktu Kuliah (Sore)</th>                          
                          <th>Nama Dosen Kuliah (Sore)</th>
                          
                          
                          
                          <th>Action</th>
                         
                        </tr>
                        </thead>


                                        <tbody>
                                        </tbody>
                                    </table>
                                </div><!-- /.box-body -->
                            </div><!-- /.box -->
                        </div>
                    </div>
        <?php
       
       $semester = ucwords($db->fetch_single_row('semester','status','Aktif')->semester);
       $username = ucwords($db->fetch_single_row('sys_users','id',$_SESSION['id_user'])->username);
       if ($username=="Admin") {
        $syarat = $path_id . " and view_0011_jadwal_kuliah.semester=". $semester;}
       else{
       $syarat =  $path_id . " and view_0011_jadwal_kuliah.semester=". $semester;
       
        //$syarat = " view_0011_jadwal_kuliah.kode_jurusan=".$path_id."  and semester=" .$semester;
        //$syarat2 = $username;
        //$syarat = "$syarat and view_0011_jadwal_kuliah.dosen_nidn_pagi=$syarat2 OR view_0011_jadwal_kuliah.dosen_nidn_sore=$syarat2";
       }

       foreach ($db->fetch_all("sys_menu") as $isi) {
                      if ($path_url==$isi->url) {
                          if ($role_act["insert_act"]=="Y") {
                    ?>
         <!--  <a href="<?=base_index();?>jadwal-kuliah/tambah" class="btn btn-primary btn-flat"><i class="fa fa-plus"></i> Tambah</a> -->
         
         <a href="<?=base_index();?>jadwal-kuliah
                     " target="" class="btn btn-primary btn-flat"><i class="fa fa-file"></i>Kembali</a>

         <a href="<?=base_admin();?>modul/jadwal_kuliah/jadwal_kuliah_view_report_pagi.php?customvar=<?php echo $syarat; ?>
                     " target="_blank" class="btn btn-primary btn-flat"><i class="fa fa-file"></i> Cetak Jadwal Kuliah Pagi</a>

         <a href="<?=base_admin();?>modul/jadwal_kuliah/jadwal_kuliah_view_report_sore.php?customvar=<?php echo $syarat; ?>
                     " target="_blank" class="btn btn-primary btn-flat"><i class="fa fa-file"></i> Cetak Jadwal Kuliah Sore</a>

         
                          
                          <?php


                          } 
                       } 
}
      
  foreach ($db->fetch_all("sys_menu") as $isi) {

  //jika url = url dari table menu
  if ($path_url==$isi->url) {
    //check edit permission
  if ($role_act["up_act"]=="Y") {
  $edit = '<a href="'.base_index()."jadwal-kuliah/edit/'+aData[indek]+' ". '" class="btn btn-primary btn-flat" title="Edit Jadwal"><i class="fa fa-pencil"></i></a>';
  } else {
    $edit ="";
  }
  
  if ($role_act['del_act']=='Y') {
   $del = "<span data-id='+aData[indek]+' data-uri=".base_admin()."modul/jadwal_kuliah/jadwal_kuliah_action.php".' class="btn btn-danger hapus btn-flat"><i class="fa fa-trash"></i></span>';
  } else {
    $del="";
  }
                   }

  }

  $user_id = ucwords($db->fetch_single_row('sys_users','id',$_SESSION['id_user'])->id);
  if ($user_id==1){
    //Pagi
    $cetak_absensi = '<a href="'.base_admin()."modul/jadwal_kuliah/absensi_mata_kuliah_view_report_pagi.php?customvar=$syarat&id='+aData[indek]+' ". '" title="Cetak Daftar Hadir (Pagi)" target="_blank" class="btn btn-primary btn-warning"><i class="fa fa-book"></i> </a>';
    $cetak_ba      = '<a href="'.base_admin()."modul/jadwal_kuliah/berita_acara_mata_kuliah_view_report_pagi.php?customvar=$syarat&id='+aData[indek]+' ". '" title="Cetak Berita Acara (Pagi)" target="_blank" class="btn btn-primary btn-warning"><i class="fa fa-book"></i> </a>';

    //Sore
    $cetak_absensi_sore = '<a href="'.base_admin()."modul/jadwal_kuliah/absensi_mata_kuliah_view_report_sore.php?customvar=$syarat&id='+aData[indek]+' ". '" title="Cetak Daftar Hadir (Sore)" target="_blank" class="btn btn-primary btn-danger"><i class="fa fa-book"></i> </a>';
    $cetak_ba_sore      = '<a href="'.base_admin()."modul/jadwal_kuliah/berita_acara_mata_kuliah_view_report_sore.php?customvar=$syarat&id='+aData[indek]+' ". '" title="Cetak Berita Acara (Sore)" target="_blank" class="btn btn-primary btn-danger"><i class="fa fa-book"></i> </a>';
  }
  else
  {
    $cetak_absensi = '';
    $cetak_ba      = '';

    //Sore
    $cetak_absensi_sore ='';
    $cetak_ba_sore      =''; 
  } 
?>  
                </section><!-- /.content -->


<script type="text/javascript">

var dataTable = $("#dtb_jadwal_kuliah").dataTable({
           "fnCreatedRow": function( nRow, aData, iDataIndex ) {
            var indek = aData.length-1;           
     $('td:eq('+indek+')', nRow).html(' <a href="<?=base_index();?>jadwal-kuliah/detail/'+aData[indek]+'" title="Detil Jadwal" class="btn btn-success btn-flat"><i class="fa fa-eye"></i></a> <?=$edit;?> <?=$cetak_absensi;?> <?=$cetak_ba;?> <?=$cetak_absensi_sore;?> <?=$cetak_ba_sore;?>');
     $(nRow).attr('id', 'line_'+aData[indek]);
   },
           'bProcessing': true,
           'bServerSide': true,
           'sAjaxSource': '<?=base_admin();?>modul/jadwal_kuliah/jadwal_kuliah_data.php?customvar=<?php echo $syarat;?> ',
          
           'aoColumnDefs': [ {className: 'text-center', 
           'aTargets': [0,1,2,5,6,7,8,10,11,12,14],
           
    } ],
    
    lengthMenu: [50, 100, 200, 500],
    buttons: [
        'colvis'
        ],
        "oLanguage": {

           "sSearch": "Pencarian :  "

         },
    
         "columnDefs": [ {
         "targets": [0,1,3,5,6,7,8,10,11,12,14],
         "searchable": true
          } ],
    
        });

</script>  
            