
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        Manage Tes Jadwal Kuliah
                    </h1>
                        <ol class="breadcrumb">
                        <li><a href="<?=base_index();?>"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li><a href="<?=base_index();?>tes-jadwal-kuliah">Tes Jadwal Kuliah</a></li>
                        <li class="active">Tes Jadwal Kuliah List</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="box">
                                <div class="box-header">
                                <h3 class="box-title">List Tes Jadwal Kuliah</h3>
                                </div><!-- /.box-header -->
                                <div class="box-body table-responsive">
                                    <table id="dtb_tes_jadwal_kuliah" class="table table-bordered table-striped">
                                   <thead>
                                     <tr>

                          <th>hari_nama</th>
													<th>semester</th>
													<th>angka_semester</th>
													<th>kode_mk</th>
													<th>nama_mk</th>
													<th>sks_tm</th>
													<th>wkt_kul_sesi_pagi</th>
													<th>wkt_kul_deskripsi_pagi</th>
													<th>dosen_nidn_pagi</th>
													<th>dosen_nama_pagi</th>
													<th>wkt_kul_sesi_sore</th>
													<th>wkt_kul_deskripsi_sore</th>
													<th>hari_nama_sore</th>
													<th>nama_kelas_sore</th>
													<th>nama_kelas_pagi</th>
													
                          <th>Action</th>
                         
                        </tr>
                                      </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div><!-- /.box-body -->
                            </div><!-- /.box -->
                        </div>
                    </div>
        <?php
       foreach ($db->fetch_all("sys_menu") as $isi) {
                      if ($path_url==$isi->url) {
                          if ($role_act["insert_act"]=="Y") {
                    ?>
          <a href="<?=base_index();?>tes-jadwal-kuliah/tambah" class="btn btn-primary btn-flat"><i class="fa fa-plus"></i> Tambah</a>
                          <?php
                          } 
                       } 
}
      
  foreach ($db->fetch_all("sys_menu") as $isi) {

  //jika url = url dari table menu
  if ($path_url==$isi->url) {
    //check edit permission
  if ($role_act["up_act"]=="Y") {
  $edit = '<a href="'.base_index()."tes-jadwal-kuliah/edit/'+aData[indek]+'".'" class="btn btn-primary btn-flat"><i class="fa fa-pencil"></i></a>';
  } else {
    $edit ="";
  }
  if ($role_act['del_act']=='Y') {
   $del = "<span data-id='+aData[indek]+' data-uri=".base_admin()."modul/tes_jadwal_kuliah/tes_jadwal_kuliah_action.php".' class="btn btn-danger hapus btn-flat"><i class="fa fa-trash"></i></span>';
  } else {
    $del="";
  }
                   } 
  }
  
?>  
                </section><!-- /.content -->

<script type="text/javascript">

var dataTable = $("#dtb_tes_jadwal_kuliah").dataTable({
           "fnCreatedRow": function( nRow, aData, iDataIndex ) {
            var indek = aData.length-1;           
     $('td:eq('+indek+')', nRow).html(' <a href="<?=base_index();?>tes-jadwal-kuliah/detail/'+aData[indek]+'" class="btn btn-success btn-flat"><i class="fa fa-eye"></i></a> <?=$edit;?> <?=$del;?>');
     $(nRow).attr('id', 'line_'+aData[indek]);
   },
           'bProcessing': true,
           'bServerSide': true,
           'sAjaxSource': '<?=base_admin();?>modul/tes_jadwal_kuliah/tes_jadwal_kuliah_data.php',
         /*     'aoColumnDefs': [{
                'bSortable': false,
                'aTargets': [0]
            }]*/
        });</script>  
            