
           
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                     Ketentuan Nilai Akhir
                    </h1>
                           <ol class="breadcrumb">
                        <li><a href="<?=base_index();?>"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li><a href="<?=base_index();?>ketentuan-nilai-akhir">Ketentuan Nilai Akhir</a></li>
                        <li class="active">Tambah Ketentuan Nilai Akhir</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
<div class="row">
    <div class="col-lg-12"> 
        <div class="box box-solid box-primary">
                                 <div class="box-header">
                                    <h3 class="box-title">Tambah Ketentuan Nilai Akhir</h3>
                                    <div class="box-tools pull-right">
                                        <button class="btn btn-info btn-sm" data-widget="collapse"><i class="fa fa-minus"></i></button>
                                        <button class="btn btn-info btn-sm" data-widget="remove"><i class="fa fa-times"></i></button>
                                    </div>
                                </div>

                  <div class="box-body">
                     <form id="input" method="post" class="form-horizontal foto_banyak" action="<?=base_admin();?>modul/ketentuan_nilai_akhir/ketentuan_nilai_akhir_action.php?act=in">
                      <div class="form-group">
                        <label for="Batas Bawah Nilai" class="control-label col-lg-2">Batas Bawah Nilai</label>
                        <div class="col-lg-10">
                          <input type="text" data-rule-number="true" name="int_grade_batas_bawah" placeholder="Batas Bawah Nilai" class="form-control" required> 
                        </div>
                      </div><!-- /.form-group -->
<div class="form-group">
                        <label for="Batas Atas Nilai" class="control-label col-lg-2">Batas Atas Nilai</label>
                        <div class="col-lg-10">
                          <input type="text" data-rule-number="true" name="int_grade_batas_atas" placeholder="Batas Atas Nilai" class="form-control" required> 
                        </div>
                      </div><!-- /.form-group -->
<div class="form-group">
                        <label for="Grade" class="control-label col-lg-2">Grade</label>
                        <div class="col-lg-10">
                          <select name="int_grade_deskripsi" data-placeholder="Pilih Grade ..." class="form-control chzn-select" tabindex="2" required>
               <option value=""></option>
               <?php foreach ($db->fetch_all("m_grade") as $isi) {
                  echo "<option value='$isi->grade_deskripsi'>$isi->grade_deskripsi</option>";
               } ?>
              </select>
                        </div>
                      </div><!-- /.form-group -->

                      
                      <div class="form-group">
                        <label for="tags" class="control-label col-lg-2">&nbsp;</label>
                        <div class="col-lg-10">
                          <input type="submit" class="btn btn-primary btn-flat" value="submit">
                        </div>
                      </div><!-- /.form-group -->
                    </form>
 <a href="<?=base_index();?>ketentuan-nilai-akhir" class="btn btn-success btn-flat"><i class="fa fa-step-backward"></i> Kembali</a>
          
                  </div>
                  </div>
              </div>
</div>
                  
                </section><!-- /.content -->
        
            