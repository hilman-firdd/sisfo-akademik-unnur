

                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                     Ketentuan Nilai Akhir
                    </h1>
                   <ol class="breadcrumb">
                        <li><a href="<?=base_index();?>"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li><a href="<?=base_index();?>ketentuan-nilai-akhir">Ketentuan Nilai Akhir</a></li>
                        <li class="active">Detail Ketentuan Nilai Akhir</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
<div class="row">
    <div class="col-lg-12">
        <div class="box box-solid box-primary">
                                   <div class="box-header">
                                    <h3 class="box-title">Detail Ketentuan Nilai Akhir</h3>
                                    <div class="box-tools pull-right">
                                        <button class="btn btn-info btn-sm" data-widget="collapse"><i class="fa fa-minus"></i></button>
                                        <button class="btn btn-info btn-sm" data-widget="remove"><i class="fa fa-times"></i></button>
                                    </div>
                                </div>

                  <div class="box-body">
                   <form class="form-horizontal">
                      <div class="form-group">
                        <label for="Batas Bawah Nilai" class="control-label col-lg-2">Batas Bawah Nilai</label>
                        <div class="col-lg-10">
                          <input type="text" disabled="" value="<?=$data_edit->int_grade_batas_bawah;?>" class="form-control">
                        </div>
                      </div><!-- /.form-group -->
<div class="form-group">
                        <label for="Batas Atas Nilai" class="control-label col-lg-2">Batas Atas Nilai</label>
                        <div class="col-lg-10">
                          <input type="text" disabled="" value="<?=$data_edit->int_grade_batas_atas;?>" class="form-control">
                        </div>
                      </div><!-- /.form-group -->
<div class="form-group">
                        <label for="Grade" class="control-label col-lg-2">Grade</label>
                        <div class="col-lg-10">
                          <?php foreach ($db->fetch_all("m_grade") as $isi) {
                  if ($data_edit->int_grade_deskripsi==$isi->grade_deskripsi) {

                    echo "<input disabled class='form-control' type='text' value='$isi->grade_deskripsi'>";
                  }
               } ?>
              
                        </div>
                      </div><!-- /.form-group -->

                   
                    </form>
                    <a href="<?=base_index();?>ketentuan-nilai-akhir" class="btn btn-success btn-flat"><i class="fa fa-step-backward"></i> Kembali</a>
          
                  </div>
                  </div>
              </div>
</div>
                  
                </section><!-- /.content -->
        
