<?php
include "../../inc/fpdf.php";

$db = new PDO('mysql:host=localhost;dbname=ekomyanu_akademik','ekomyanu_root','ekomuh1801');

$syarat =  $_GET['customvar'];
$array_data = array();
$array_data = explode("  -  ",$syarat);

$semester = $array_data[0];
   $kelas = $array_data[1];
$username = $array_data[2];   
    $name = $array_data[3];
 $kode_mk = $array_data[4];
 $nama_mk = $array_data[5];

$array_jadwal = array();
if ($kelas=="Pagi"){    
 $query=$db->query("select * from view_0019_list_nilai_matkul where kode_mk_mhs=". $kode_mk . " and dosen_nidn_pagi=" .$username. " and level LIKE '%Pagi%' " );
 $data = $query->fetch(PDO::FETCH_OBJ);
 $array_jadwal = explode(",",$data->jadwal_pagi);
 $hari = $array_jadwal[0];
 $waktu = $array_jadwal[1];

 $username = $data->dosen_nidn_pagi;   
     $name = $data->dosen_nama_pagi;
}
else
{
  $query=$db->query("select * from view_0019_list_nilai_matkul where kode_mk_mhs=". $kode_mk . " and dosen_nidn_sore=" .$username. " and level LIKE '%Sore%' " );
  $data = $query->fetch(PDO::FETCH_OBJ);
  $array_jadwal = explode(",",$data->jadwal_sore);
  $hari = $array_jadwal[0];
  $waktu = $array_jadwal[1];

  $username = $data->dosen_nidn_sore;   
      $name = $data->dosen_nama_sore;
}

$tahun_akademik= $data->smt_name;
$program_studi = $data->nama_jurusan;
$nama_mk       = $data->nama_mk_mhs;
$nama_kelas    = $data->nama_kelas;
$kode_jurusan  = $data->kode_jurusan;
$kaprodi_nidn  = $data->kaprodi_nidn;
$kaprodi_nama  = $data->kaprodi_nama;

 

class myPDF extends FPDF{
    function header(){
        $this->Image('../../assets/logo_bw.png',10,6,-400);
        $this->ln(2);
        $this->SetFont('Arial','B',14);
        $this->cell(197,5,'UNIVERSITAS NURTANIO BANDUNG',0,1,'C');
        $this->SetFont('Arial','',14);
        $this->cell(197,5,'FAKULTAS ILMU KOMPUTER DAN INFORMATIKA',0,1,'C');
        $this->SetFont('Arial','',10);
        $this->cell(197,5,'Jl.Pajajaran No.219 Lanud Husein S.Bandung 40174 Telp.022 86061700/Fax 86061701',0,1,'C');
        $this->Line(10, 32, 210-10, 32);
        $this->ln(10);

        $this->SetFont('Arial','B',12);
        $this->cell(190,7,'DAFTAR HADIR UJIAN AKHIR SEMESTER',0,1,'C');
        $this->cell(190,7,'TAHUN AKADEMIK '. $GLOBALS['tahun_akademik'],0,1,'C');
        
        $this->SetFont('Times','',10);
        $this->cell(25,7,'Program Studi',0,0,'L');
        $this->Cell(5,7,' : ' ,0,0,'L'); 
        $this->Cell(80,7,$GLOBALS['program_studi'] ,0,0,'L');

        $this->cell(20,7,'Dosen',0,0,'L');
        $this->Cell(5,7,' : ' ,0,0,'L'); 
        $this->Cell(100,7,$GLOBALS['name'] ,0,1,'L');

        $this->cell(25,7,'Konsentrasi',0,0,'L');
        $this->Cell(5,7,' : ' ,0,0,'L'); 
        $this->Cell(80,7,'-' ,0,0,'L');

        $this->cell(20,7,'Hari/Tanggal',0,0,'L');
        $this->Cell(5,7,' : ' ,0,0,'L'); 
        $this->Cell(100,7,$GLOBALS['hari'] .', Tentatif' ,0,1,'L');

        $this->cell(25,7,'Nama Matakuliah',0,0,'L');
        $this->Cell(5,7,' : ' ,0,0,'L'); 
        $this->Cell(80,7,$GLOBALS['nama_mk'] ,0,0,'L');

        $this->cell(20,7,'Waktu',0,0,'L');
        $this->Cell(5,7,' : ' ,0,0,'L'); 
        $this->Cell(100,7,$GLOBALS['waktu'] ,0,1,'L');

        $this->cell(25,7,'Kode Matakuliah',0,0,'L');
        $this->Cell(5,7,' : ' ,0,0,'L'); 
        $this->Cell(80,7,$GLOBALS['kode_mk'],0,0,'L');

        $this->cell(20,7,'Kelas',0,0,'L');
        $this->Cell(5,7,' : ' ,0,0,'L'); 
        $this->Cell(100,7,$GLOBALS['kelas'] .'-'. $GLOBALS['nama_kelas'] ,0,1,'L');

        $this->Ln(5);
    }
    function footer(){
        $this->SetY(-15);
        $this->SetFont('Arial','',8);
        $this->Cell(0,5,'Hal. '.$this->PageNo().' dari {nb}',0,1,'C');
        $this->Cell(0,5,'Tanggal Cetak : [' . date("d/m/yy h:m:s").']',0,0,'C');
    }
    function headerTable(){
        $this->SetFont('Times','',9);

        $this->cell(10,10,'No',1,0,'C');
        $this->cell(47,10,'Nama Mahasiswa',1,0,'C');
        $this->cell(23,10,'NPM',1,0,'C');
        $this->cell(30,10,'Tanda Tangan',1,0,'C');
        //$this->cell(10,10,'HDRII',1,0,'C');
        $this->cell(60,5,'Nilai (dengan angka)',1,0,'C');
        $this->cell(20,5,'NAA','T',0,'C');
        $this->cell(0,5,'','R',1); //dummy

        $this->cell(110,5,'',0,0);
        $this->cell(10,5,'HDR',1,0,'C');
        $this->cell(12.5,5,'TGS',1,0,'C');
        $this->cell(12.5,5,'UTS',1,0,'C');
        $this->cell(12.5,5,'UAS',1,0,'C');
        $this->cell(12.5,5,'NAA',1,0,'C');
        $this->cell(20,5,'Huruf Mutu','B',0,'C');
        $this->cell(0,5,'','R',1); //dummy
    }
    function viewTable($db){
         $this->SetFont('Times','',7);
        if ($GLOBALS['kelas']=="Pagi"){    
         $query=$db->query('select DISTINCT * from view_0019_list_nilai_matkul where status_krs=1 and semester='. $GLOBALS['semester'] . ' and kode_mk_mhs='. $GLOBALS['kode_mk'] . ' and dosen_nidn_pagi=' .$GLOBALS['username'] .  " and level LIKE '%Pagi%' " );
         
        }
        else
        {
          $query=$db->query('select DISTINCT * from view_0019_list_nilai_matkul where status_krs=1 and semester='. $GLOBALS['semester'] . ' and  kode_mk_mhs='. $GLOBALS['kode_mk'] . ' and dosen_nidn_sore=' .$GLOBALS['username'] .  " and level LIKE '%Sore%' " );
        }
        $i=1;
        while($data = $query->fetch(PDO::FETCH_OBJ)){

        $this->cell(10,7,$i.'.',1,0,'C');
        $this->cell(47,7,$data->nama,1,0,'L');
        $this->cell(23,7,$data->nim,1,0,'C');
        $this->cell(15,7,'',1,0,'C');
        $this->cell(15,7,'',1,0,'C');
        $this->cell(10,7,$data->nilai_kehadiran,1,0,'C');
        $this->cell(12.5,7,$data->nilai_tugas,1,0,'C');
        $this->cell(12.5,7,$data->nilai_uts,1,0,'C');
        $this->cell(12.5,7,$data->nilai_uas,1,0,'C');
        $this->cell(12.5,7,$data->nilai_angka,1,0,'C');
        $this->cell(20,7,$data->nilai_huruf,1,1,'C');

        $i=$i+1;
        }

    }
    function footerTable($db){
        $this->SetFont('Times','B',9);
        $this->cell(80,5,'Keterangan :',1,0,'L');
        $this->SetFont('Times','',9);
        $this->cell(15,5,'',1,0,'C');
        $this->cell(15,5,'',1,0,'C');
        $this->cell(10,5,'',1,0,'C');
        $this->cell(12.5,5,'',1,0,'C');
        $this->cell(12.5,5,'',1,0,'C');
        $this->cell(12.5,5,'',1,0,'C');
        $this->cell(12.5,5,'',1,0,'C');
        $this->cell(20,5,'',1,1,'C');

        $this->cell(0.1,5,'','L',0); // dummy
        $this->cell(56,5,'Nilai Absolut Akhir','B',0,'C');
        $this->cell(23.9,5,'Huruf Mutu(HM)','B',0,'C');
        $this->cell(15,5,'',1,0,'C');
        $this->cell(15,5,'',1,0,'C');
        $this->cell(10,5,'',1,'R','C');
        $this->cell(12.5,5,'',1,0,'C');
        $this->cell(12.5,5,'HDR',0,0,'C');
        $this->cell(1,5,': ',0,0,'C');
        $this->cell(44,5,'Kehadiran','R',1,'L');

        $this->cell(57,5,'80 - 100','L',0,'C');
        $this->cell(23,5,'A','R',0,'C');
        $this->cell(15,5,'','R',0,'C');
        $this->cell(15,5,'','R',0,'C');
        $this->cell(10,5,'','R',0);
        $this->cell(12.5,5,'','R',0,'C');
        $this->cell(12.5,5,'TGS',0,0,'C');
        $this->cell(1,5,': ',0,0,'C');
        $this->cell(44,5,'Rata-Rata Tugas','R',1,'L');

        $this->cell(57,5,'70 - 79','L',0,'C');
        $this->cell(23,5,'B','R',0,'C');
        $this->cell(15,5,'','R',0,'C');
        $this->cell(15,5,'','R',0,'C');
        $this->cell(10,5,'','R',0);
        $this->cell(12.5,5,'','R',0,'C');
        $this->cell(12.5,5,'UTS',0,0,'C');
        $this->cell(1,5,': ',0,0,'C');
        $this->cell(44,5,'Ujian Tengah Semester','R',1,'L');

        $this->cell(57,5,'60 - 69','L',0,'C');
        $this->cell(23,5,'C','R',0,'C');
        $this->cell(15,5,'','R',0,'C');
        $this->cell(15,5,'','R',0,'C');
        $this->cell(10,5,'','R',0);
        $this->cell(12.5,5,'','R',0,'C');
        $this->cell(12.5,5,'NAA',0,0,'C');
        $this->cell(1,5,': ',0,0,'C');
        //$this->SetFont('Times','U',9);
        $this->cell(44,5,'Ujian Akhir Semester','R',1,'L');

        $this->SetFont('Times','',9);
        $this->cell(57,5,'55 - 59','L',0,'C');
        $this->cell(23,5,'D','R',0,'C');
        $this->cell(15,5,'','R',0,'C');
        $this->cell(15,5,'','R',0,'C');
        $this->cell(10,5,'','R',0);
        $this->cell(12.5,5,'','R',0,'C');
        $this->cell(12.5,5,'NAA',0,0,'C');
        $this->cell(1,5,':',0,0,'C');
        $this->SetFont('Times','U',9);
        $this->cell(44,5,'1HDR + 2TGS + 3UTS + 4UAS','R',1,'L');
         $this->SetFont('Times','',9);

        $this->cell(57,5,'00 - 54','L',0,'C');
        $this->cell(23,5,'E','R',0,'C');
        $this->cell(15,5,'','R',0,'C');
        $this->cell(15,5,'','R',0,'C');
        $this->cell(10,5,'','R',0);
        $this->cell(12.5,5,'','R',0,'C');
        $this->cell(12.5,5,'',0,0,'C');
        $this->cell(1,5,'',0,0,'C');
        $this->cell(44,5,'                        10','R',1,'L');

        $this->cell(0.1,5,'','L',0); // dummy
        $this->cell(80,5,'Kaprodi '. $GLOBALS['program_studi'],'T',0,'C');
        $this->cell(52.5,5,'Pengawas','T',0,'C');
        $this->cell(57.4,5,'Bandung, '.date("d F yy"),'T',0,'C');
        $this->cell(0,5,'','L',1); // dummy

        $this->cell(80,15,'','L',0,'C');
        $this->cell(52.5,15,'',0,0,'C');
        $this->cell(57.5,15,'','R',1,'C');
        
        $this->cell(0.1,5,'','L',0); // dummy
        $this->cell(80,5,$GLOBALS['kaprodi_nama'],0,0,'C');
        $this->cell(52.5,5,'...................',0,0,'C');
        $this->cell(57.4,5,$GLOBALS['name'],0,0,'C');
        $this->cell(0,5,'','L',1); // dummy

        $this->cell(0.1,5,'','L',0); // dummy
        $this->cell(80,5,$GLOBALS['kaprodi_nidn'],'B',0,'C');
        $this->cell(52.5,5,'NIP.             ','B',0,'C');
        $this->cell(57.4,5,'NIP.'.$GLOBALS['username'],'B',0,'C');
        $this->cell(0,5,'','L',1); // dummy


    }
}

$pdf = new myPDF('P','mm','A4');
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->headerTable();
$pdf->viewTable($db);
$pdf->footerTable($db);
$pdf->Output();

?>