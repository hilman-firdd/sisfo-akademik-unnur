
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        Manage Isi Daftar Nilai <?php echo $db->fetch_single_row('jurusan','kode_jurusan',$id_jur)->nama_jurusan;?>
                    </h1>
                        <ol class="breadcrumb">
                        <li><a href="<?=base_index();?>"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li><a href="<?=base_index();?>setujui-krs">Isi Daftar Nilai</a></li>
                        <li class="active">Isi Daftar Nilai</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="box">
                                <div class="box-header">
                                  <h3 class="box-title">List Isi Daftar Nilai</h3>                                  
                                </div><!-- /.box-header -->

<form method="get" class="form-horizontal foto_banyak" action="">

                      <div class="form-group">
                        <label for="Menu" class="control-label col-lg-2">Pencarian</label>
                        <div class="col-lg-8" >

                          <link href="<?=base_admin();?>assets/plugins/chosen/chosen.min.css" rel="stylesheet" type="text/css" />
                          <script src="<?=base_admin();?>assets/plugins/chosen/chosen.jquery.min.js" type="text/javascript"></script>

                          <!-- Script -->
                          
                          <script type="text/javascript">
                          $(document).ready(function(){
                           $('select').chosen({ height:'50' });
                          });
                          </script>
                          

                            <select id="mhs" name="user" id="id_po_select" data-placeholder="Pilih Semester-Matakuliah...." class="form-control chzn-select" tabindex="2" >
                        <option value=""></option>
                          <?php 
                              $username=ucwords($db->fetch_single_row('sys_users','id',$_SESSION['id_user'])->username);
                              $name=ucwords($db->fetch_single_row('m_dosen','dosen_nidn',$username)->dosen_nama);
                              if($username<>"Admin"){
                              foreach ($db->fetch_custom("select * from view_0017_list_matkul_yg_diajar_dosen                                                                                                                        
                                                          where view_0017_list_matkul_yg_diajar_dosen.NIDN=? order by view_0017_list_matkul_yg_diajar_dosen.Kelas asc", array('view_0017_list_matkul_yg_diajar_dosen.NIDN'=>$username)) as $isi) {

                                       if ($_GET['user']==$isi->cari) {
                                                   echo "<option value='$isi->cari' selected > $isi->cari</option>";
                                       } else {
                                                   echo "<option value='$isi->cari'  > $isi->cari</option>";

                                       }
                               }
                               }
                               else
                               {
                                foreach ($db->fetch_custom("select * from view_0017_list_matkul_yg_diajar_dosen                                                                                                                        
                                                           order by view_0017_list_matkul_yg_diajar_dosen.Kelas asc", array('view_0017_list_matkul_yg_diajar_dosen.NIDN'=>$username)) as $isi) {

                                       if ($_GET['user']==$isi->cari) {
                                                   echo "<option value='$isi->cari' selected > $isi->cari</option>";
                                       } else {
                                                   echo "<option value='$isi->cari'  > $isi->cari</option>";

                                       }
                               }
                               } 
                          ?>
                          <option value=""> </option>
                  
                  </select>
                        </div>
                      </div><!-- /.form-group -->

 <label for="Menu" class="control-label col-lg-2">&nbsp;</label>
                        <div class="col-lg-10">
<button style="margin-top:10px;margin-bottom:10px" class="btn btn-primary">Show Isi Daftar Nilai</button>
</div>
</form>

                                <div class="box-body table-responsive">
          
<?php 

    if (isset($_GET['user'])) {
    $syarat = $_GET['user'];
    $array_data = array();
    $array_data = explode("  -  ",$_GET['user']);
    $semester = $array_data[0];
    $username = $array_data[2];
    $name = $array_data[3];
    $kode_mk= $array_data[4];
    $kelas_tipe= $array_data[1] ; 
    if ($array_data[1]=="Pagi"){ 
        $ruang_kls=$db->fetch_custom_single("select distinct nama_kelas_pagi from view_0011_jadwal_kuliah where dosen_nidn_pagi=? and semester=? and kode_mk=?",array("nidn"=>$username,"semester" =>$semester,"kode_mk" =>$kode_mk ));
        $kelas= $ruang_kls->nama_kelas_pagi. ' - ('.$array_data[1].')' ; 
    }
    else{
        $ruang_kls=$db->fetch_custom_single("select distinct nama_kelas_sore from view_0011_jadwal_kuliah where dosen_nidn_sore=? and semester=? and kode_mk=?",array("nidn"=>$username,"semester" =>$semester,"kode_mk" =>$kode_mk ));
        $kelas= $ruang_kls->nama_kelas_sore. ' - ('.$array_data[1].')' ; 
    }
   
   
    
    //Cari Bobot Penilaian Mata Kuliah
    $data_bobot = $db->fetch_custom_single("select * from m_bobot_matkul where bobot_matkul=?",array("cari" =>$_GET['user']));

    $hdr=(isset($data_bobot->bobot_kehadiran))? $data_bobot->bobot_kehadiran: 0;
    $tgs=(isset($data_bobot->bobot_tugas))? $data_bobot->bobot_tugas: 0;
    $uts=(isset($data_bobot->bobot_uts))? $data_bobot->bobot_uts: 0;
    $uas=(isset($data_bobot->bobot_uas))? $data_bobot->bobot_uas: 0;

    //Cari Ketentuan Penilaian Grade Mata Kuliah
    $data_interval_grade_A = $db->fetch_custom_single("select * from m_interval_nilai_grade where int_grade_deskripsi=?",array("cari" =>'A')); 
    $A1=$data_interval_grade_A->int_grade_batas_bawah;
    $A2=$data_interval_grade_A->int_grade_batas_atas;

    $data_interval_grade_B = $db->fetch_custom_single("select * from m_interval_nilai_grade where int_grade_deskripsi=?",array("cari" =>'B')); 
    $B1=$data_interval_grade_B->int_grade_batas_bawah;
    $B2=$data_interval_grade_B->int_grade_batas_atas;

    $data_interval_grade_C = $db->fetch_custom_single("select * from m_interval_nilai_grade where int_grade_deskripsi=?",array("cari" =>'C')); 
    $C1=$data_interval_grade_C->int_grade_batas_bawah;
    $C2=$data_interval_grade_C->int_grade_batas_atas;

    $data_interval_grade_D = $db->fetch_custom_single("select * from m_interval_nilai_grade where int_grade_deskripsi=?",array("cari" =>'D')); 
    $D1=$data_interval_grade_D->int_grade_batas_bawah;
    $D2=$data_interval_grade_D->int_grade_batas_atas;

    $data_interval_grade_E = $db->fetch_custom_single("select * from m_interval_nilai_grade where int_grade_deskripsi=?",array("cari" =>'E')); 
    $E1=$data_interval_grade_E->int_grade_batas_bawah;
    $E2=$data_interval_grade_E->int_grade_batas_atas;




?>       

    <table>
        <tr><td>Nama Dosen</td>       <td>&ensp;:&ensp;</td> <td><?=$array_data[2] . " - " .$name?></td></tr>
        <tr><td>Semester</td>         <td>&ensp;:&ensp;</td> <td><?=$array_data[0]?></td></tr>
        <tr><td>Kelas</td>            <td>&ensp;:&ensp;</td> <td><?=$kelas?></td></tr>
        <tr><td>Kode Mata Kuliah</td> <td>&ensp;:&ensp;</td> <td><?=$array_data[4]?></td></tr>
        <tr><td>Nama Mata Kuliah</td> <td>&ensp;:&ensp;</td> <td><?=$array_data[5]?></td></tr>
    </table>

    <h5><b> Isikan Nilai mahasiswa : 

    </br></br></b></h5>
    <div><marquee scrollamount="5" behavior="scroll" direction="left" id="mymarquee">Informasi: Untuk pengisian angka berkoma, dientrikan dengan format titik, contoh : 75,50 <b>menjadi</b> 75.50</marquee></div>
    <input type="button" value="Stop Pesan Berjalan" onClick="document.getElementById('mymarquee').stop();">
    <input type="button" value="Start Pesan Berjalan" onClick="document.getElementById('mymarquee').start();">

<table id="dtb" class="table table-bordered table-condensed table-hover table-striped ">
                      <thead style="text-align:center">
                        <tr>
                          <th style="width:10px" class="text-center">No.</th>
                          <th style="width:10px">NIM</th>
                          <th style="width:20px">Nama</th>     
                          <th style="width:20px">Nilai Kehadiran <?=(isset($data_bobot->bobot_kehadiran))? ' ('.$data_bobot->bobot_kehadiran .'%)' : '(0%)';?></th>
                          <th style="width:40px">Nilai Tugas <?=(isset($data_bobot->bobot_tugas))? ' ('.$data_bobot->bobot_tugas .'%)' : '(0%)';?> </th>                          
                          <th style="width:40px">Nilai UTS <?=(isset($data_bobot->bobot_uts))? ' ('.$data_bobot->bobot_uts .'%)' : '(0%)';?> </th>  
                          <th style="width:40px">Nilai UAS <?=(isset($data_bobot->bobot_uas))? ' ('.$data_bobot->bobot_uas .'%)' : '(0%)';?> </th>  
                          <th style="width:40px">Nilai Akhir</th>  
                          <th style="width:40px">Grade</th>  
                                              
                        </tr>
                      </thead>
                      <tbody>
                              <!--<div class="form-group"> <label>Semester : </label><input type="text" id="min" name="min"> </div> -->
                              <?php 

                                  
                              

                                  if ($kelas_tipe=="Pagi"){    
                                      $dtb=$db->fetch_custom("SELECT distinct * from view_0019_list_nilai_matkul where kode_mk_mhs=? and dosen_nidn_pagi=? and status_krs=1 and level LIKE '%Pagi%' and semester_mhs=?", array('$kode_mk'=>$kode_mk,'dosen_nidn_pagi'=>$username,'semester'=>$semester) ) ;
                                  }
                                  else
                                  {
                                      $dtb=$db->fetch_custom("SELECT distinct * from view_0019_list_nilai_matkul where kode_mk_mhs=? and dosen_nidn_sore=? and status_krs=1 and level LIKE '%Sore%' and semester_mhs=?", array('$kode_mk'=>$kode_mk,'dosen_nidn_sore'=>$username,'semester'=>$semester) ) ;
                                  }
                                  $i=1;
                                  foreach ($dtb as $isi) {
                                    ?>

                                    <tr id="line_<?=$isi->id_krs_mhs;?>" >

                                    
                                    <td><?=$i.'.';?></td>
                                    <td align="center"><?=$isi->nim;?></td>
                                    <td align=""><?=$isi->nama;?></td>

                                    <!--<td align="center">Nilai Absensi</td>-->
                                    <td align="center" >
                                      <div class="textbox">
                                              <label>
                                                <input size="4" data-rule-number="true"  class="uniform" id="texthadir_<?=$isi->id_krs_mhs;?>" name="texthadir" type="text" value="<?=(isset($isi->nilai_kehadiran))? $isi->nilai_kehadiran : '0';?>" onKeyUp="numericFilter(this);" onchange="nilai_kehadiran_act(<?=$isi->id_krs_mhs;?>,this);sum(<?=$isi->id_krs_mhs;?>, <?=$hdr?>, <?=$tgs?>, <?=$uts?>, <?=$uas?>); "   >  
                                              </label>
                                       </div>
                                    </td>

                                    <!--<td align="center">Nilai Tugas</td>-->      
                                    <td align="center" >
                                      <div class="textbox">
                                              <label>
                                                <input size="4" data-rule-number="true" class="uniform" id="texttugas_<?=$isi->id_krs_mhs;?>" name="texttugas" type="text" value="<?=(isset($isi->nilai_tugas))? $isi->nilai_tugas : '0';?>" onKeyUp="numericFilter(this);" onchange="nilai_tugas_act(<?=$isi->id_krs_mhs;?>,this);sum(<?=$isi->id_krs_mhs;?>, <?=$hdr?>, <?=$tgs?>, <?=$uts?>, <?=$uas?>);"   >  
                                              </label>
                                       </div>
                                    </td>                            
                                    <!--<td align="center">Nilai UTS</td>-->
                                    <td align="center" >
                                      <div class="textbox">
                                              <label>
                                                <input size="4" data-rule-number="true" class="uniform" id="textuts_<?=$isi->id_krs_mhs;?>" name="textuts" type="text" value="<?=(isset($isi->nilai_uts))? $isi->nilai_uts : '0';?>" onKeyUp="numericFilter(this);" onchange="nilai_uts_act(<?=$isi->id_krs_mhs;?>,this);sum(<?=$isi->id_krs_mhs;?>, <?=$hdr?>, <?=$tgs?>, <?=$uts?>, <?=$uas?>);"   >   
                                              </label>
                                       </div>
                                    </td>   
                                    <!--<td align="center">Nilai UAS</td>-->
                                    <td align="center" >
                                      <div class="textbox">
                                              <label>
                                                <input size="4" data-rule-number="true" class="uniform" id="textuas_<?=$isi->id_krs_mhs;?>" name="textuas" type="text" value="<?=(isset($isi->nilai_uas))? $isi->nilai_uas : '0';?>" onKeyUp="numericFilter(this);" onchange="nilai_uas_act(<?=$isi->id_krs_mhs;?>,this);sum(<?=$isi->id_krs_mhs;?>, <?=$hdr?>, <?=$tgs?>, <?=$uts?>, <?=$uas?>);"   >   
                                              </label>
                                       </div>
                                    </td>   
                                    <!--NILAI AKHIR-->
                                    <td align="center" >
                                      <div class="textbox">
                                              <label>
                                                <input size="4" class="uniform" id="textnilaiakhir_<?=$isi->id_krs_mhs;?>" name="textnilaiakhir" type="text" value="<?=(isset($isi->nilai_angka))? $isi->nilai_angka : '';?>"    disabled>  
                                              </label>
                                       </div>
                                    </td>   
                                    <td align="center" >
                                      <div>
                                              <label>
                                                <input size="4" style="text-align:center;" id="textgrade_<?=$isi->id_krs_mhs;?>" name="textgrade" type="text" value="<?=(isset($isi->nilai_huruf))? $isi->nilai_huruf : '';?>"    disabled>  
                                              </label>
                                       </div>
                                    </td>   
                                    
                                    <?php
                                    if($i)
                                    {
                                        ?>
                                        
                            <?php
                                    } else {

                                    

                                    ?>


                                    
                                                      <?php 
                                                      }
                                                      ?>
                                    </tr>
                                    <?php
                                    $i++;
                                  }

                                  ?>
                           <div align="right">
                              <a href="<?=base_index();?>isi-daftar-nilai/import?customvar=<?php echo $syarat; ?>" class="btn btn-primary btn-flat"><i class="fa fa-cloud-upload"></i> Upload Data Excel</a>
                              <a href="<?=base_admin();?>modul/isi_daftar_nilai/daftar_nilai_report.php?customvar=<?php echo $syarat; ?>" target="_blank" class="btn btn-primary btn-flat" align="right"  <?=($i>1)? '':'disabled';  ?> ><i class="fa fa-file"></i>  Cetak Daftar Nilai</a>       
                           </div>
                           <br></br>
                       </tbody>
</table>
<?php 

}  

?>


                                </div><!-- /.box-body -->
                            </div><!-- /.box -->
                        </div>
                    </div>
                </section><!-- /.content -->
  



<script type="text/javascript">



$(document).ready(function() {


      $('#dtb').DataTable( {

        lengthMenu: [50, 100, 200, 500],
        //dom: 'Bfrtip',
        buttons: [
        'colvis'
        ],
        "oLanguage": {

           "sSearch": "Pencarian berdasarkan Nama Mahasiswa :  "

         },
    
         "columnDefs": [ {
      "targets": [0,1,3,4,5,6,7],
      "searchable": false
    } ],


    } );

   
  } );
  
function roundToTwo(num) {    
    return +(Math.round(num + "e+2")  + "e-2");
}

function ketentuan_indek (grade){
        var grade;

        if (grade =='A')
                 { indek=4; }
        else if (grade =='B' ) 
                { indek=3;  }
              else if (grade =='C' ) 
                { indek='2';  }
                    else if (grade ==D ) 
                      { indek='1';  }
                         else if (grade ==E) 
                             { indek='0';  }
        return indek;

}

function ketentuan_grade (nilai_akhir, A1,A2,B1,B2,C1,C2,D1,D2,E1,E2){
        var grade;

        if (roundToTwo(nilai_akhir) >=roundToTwo(A1) )
                 { grade='A'; }
        else if (roundToTwo(nilai_akhir) >=roundToTwo(B1) ) 
                { grade='B';  }
              else if (roundToTwo(nilai_akhir) >=roundToTwo(C1) ) 
                { grade='C';  }
                    else if (roundToTwo(nilai_akhir) >=roundToTwo(D1) ) 
                      { grade='D';  }
                         else if (roundToTwo(nilai_akhir) >=roundToTwo(E1) ) 
                             { grade='E';  }
        return grade;

}

function grade(id_krs, bobot_hadir, bobot_tugas, bobot_uts, bobot_uas) {
        var txt_bobot_kehadiran = document.getElementById("texthadir_"+id_krs).value;
        var txt_bobot_tugas = document.getElementById("texttugas_"+id_krs).value;
        var txt_bobot_uts = document.getElementById("textuts_"+id_krs).value;
        var txt_bobot_uas = document.getElementById("textuas_"+id_krs).value;
        var result = roundToTwo(parseFloat(txt_bobot_kehadiran)*parseFloat(bobot_hadir/100))+
                     roundToTwo(parseFloat(txt_bobot_tugas)*parseFloat(bobot_tugas/100))+
                     roundToTwo(parseFloat(txt_bobot_uts)*parseFloat(bobot_uts/100))+
                     roundToTwo(parseFloat(txt_bobot_uas)*parseFloat(bobot_uas/100));
        if (!isNaN (result)){
            document.getElementById("textnilaiakhir_"+id_krs).value = result;
            var grade_final=ketentuan_grade(result, '<?=$A1;?>','<?=$A2;?>','<?=$B1;?>','<?=$B2;?>','<?=$C1;?>','<?=$C2;?>','<?=$D1;?>','<?=$D2;?>','<?=$E1;?>','<?=$E2;?>');
            document.getElementById("textgrade_"+id_krs).value = grade_final; 
            
        }
        
       
        return grade_final;
    } 

function sum(id_krs, bobot_hadir, bobot_tugas, bobot_uts, bobot_uas) {
        var txt_bobot_kehadiran = document.getElementById("texthadir_"+id_krs).value;
        var txt_bobot_tugas = document.getElementById("texttugas_"+id_krs).value;
        var txt_bobot_uts = document.getElementById("textuts_"+id_krs).value;
        var txt_bobot_uas = document.getElementById("textuas_"+id_krs).value;
        var result = roundToTwo(parseFloat(txt_bobot_kehadiran)*parseFloat(bobot_hadir/100))+
                     roundToTwo(parseFloat(txt_bobot_tugas)*parseFloat(bobot_tugas/100))+
                     roundToTwo(parseFloat(txt_bobot_uts)*parseFloat(bobot_uts/100))+
                     roundToTwo(parseFloat(txt_bobot_uas)*parseFloat(bobot_uas/100));
        if (!isNaN (result)){
            document.getElementById("textnilaiakhir_"+id_krs).value = result;
            var grade_final=ketentuan_grade(roundToTwo(result), '<?=$A1;?>','<?=$A2;?>','<?=$B1;?>','<?=$B2;?>','<?=$C1;?>','<?=$C2;?>','<?=$D1;?>','<?=$D2;?>','<?=$E1;?>','<?=$E2;?>');
            document.getElementById("textgrade_"+id_krs).value = grade_final; 

        }
        
       
        return result;
    } 
  
function nilai_kehadiran_act(id,cb) {
  
  var NA = sum(id, '<?=$hdr;?>', '<?=$tgs;?>', '<?=$uts;?>','<?=$uas;?>' );
  var GA = ketentuan_grade(NA, '<?=$A1;?>','<?=$A2;?>','<?=$B1;?>','<?=$B2;?>','<?=$C1;?>','<?=$C2;?>','<?=$D1;?>','<?=$D2;?>','<?=$E1;?>','<?=$E2;?>');
  
 /*
  setTimeout(function() {
   location.reload();
   }, 100);
  */

  //debugger;
  
  if (cb.value < 0 || cb.value > 100) {alert ('Angka yang dientri harus 0 s.d 100! '); cb.value=0; cb.focus();}
  
  $.ajax({

        type: "post",
        url: "<?=base_admin();?>modul/isi_daftar_nilai/isi_daftar_nilai_action.php?act=change_nilai_kehadiran",
        data: "nim="+'<?=$username;?>'+"&nama="+'<?=$name;?>'+"&id="+id+"&nilai="+cb.value+"&NA="+NA+"&GA="+GA,
        
     //  enctype:  'multipart/form-data'
      success: function(data){
               
        console.log(data);
        //$("#dtb").dataTable().fnReloadAjax();

        //$('#dtb').DataTable().ajax.reload();

    }

  });
}

function nilai_tugas_act(id,cb) {
  
  var NA = sum(id, '<?=$hdr;?>', '<?=$tgs;?>', '<?=$uts;?>','<?=$uas;?>' );
  var GA = ketentuan_grade(NA, '<?=$A1;?>','<?=$A2;?>','<?=$B1;?>','<?=$B2;?>','<?=$C1;?>','<?=$C2;?>','<?=$D1;?>','<?=$D2;?>','<?=$E1;?>','<?=$E2;?>');
  
  /*
  setTimeout(function() {
   location.reload();
   }, 100);
  */

  //debugger;
  if (cb.value < 0 || cb.value > 100) {alert ('Angka yang dientri harus 0 s.d 100! '); cb.value=0; cb.focus();}
  
  $.ajax({

        type: "post",
        url: "<?=base_admin();?>modul/isi_daftar_nilai/isi_daftar_nilai_action.php?act=change_nilai_tugas",
        data: "nim="+'<?=$username;?>'+"&nama="+'<?=$name;?>'+"&id="+id+"&nilai="+cb.value+"&NA="+NA+"&GA="+GA,
        
     //  enctype:  'multipart/form-data'
      success: function(data){
               
        console.log(data);
        //$("#dtb").dataTable().fnReloadAjax();

        //$('#dtb').DataTable().ajax.reload();

    }

  });
}


function nilai_uts_act(id,cb) {
  
  var NA = sum(id, '<?=$hdr;?>', '<?=$tgs;?>', '<?=$uts;?>','<?=$uas;?>' );
  var GA = ketentuan_grade(NA, '<?=$A1;?>','<?=$A2;?>','<?=$B1;?>','<?=$B2;?>','<?=$C1;?>','<?=$C2;?>','<?=$D1;?>','<?=$D2;?>','<?=$E1;?>','<?=$E2;?>');

  /*
  setTimeout(function() {
   location.reload();
   }, 100);
  */

  if (cb.value < 0 || cb.value > 100) {alert ('Angka yang dientri harus 0 s.d 100! '); cb.value=0; cb.focus();}
  
  //debugger;
  $.ajax({

        type: "post",
        url: "<?=base_admin();?>modul/isi_daftar_nilai/isi_daftar_nilai_action.php?act=change_nilai_uts",
        data: "nim="+'<?=$username;?>'+"&nama="+'<?=$name;?>'+"&id="+id+"&nilai="+cb.value+"&NA="+NA+"&GA="+GA,
        
     //  enctype:  'multipart/form-data'
      success: function(data){
               
        console.log(data);
        //$("#dtb").dataTable().fnReloadAjax();

        //$('#dtb').DataTable().ajax.reload();

    }

  });
}


function nilai_uas_act(id,cb) {
  
  var NA = sum(id, '<?=$hdr;?>', '<?=$tgs;?>', '<?=$uts;?>','<?=$uas;?>' );
  var GA = ketentuan_grade(NA, '<?=$A1;?>','<?=$A2;?>','<?=$B1;?>','<?=$B2;?>','<?=$C1;?>','<?=$C2;?>','<?=$D1;?>','<?=$D2;?>','<?=$E1;?>','<?=$E2;?>');
  
  /*
  setTimeout(function() {
   location.reload();
   }, 100);
  */

  if (cb.value < 0 || cb.value > 100) {alert ('Angka yang dientri harus 0 s.d 100! '); cb.value=0; cb.focus();}
 
  
  //debugger;
  $.ajax({

        type: "post",
        url: "<?=base_admin();?>modul/isi_daftar_nilai/isi_daftar_nilai_action.php?act=change_nilai_uas",
        data: "nim="+'<?=$username;?>'+"&nama="+'<?=$name;?>'+"&id="+id+"&nilai="+cb.value+"&NA="+NA+"&GA="+GA,
        
     //  enctype:  'multipart/form-data'
      success: function(data){
               
        console.log(data);
        //$("#dtb").dataTable().fnReloadAjax();

        //$('#dtb').DataTable().ajax.reload();

    }

  });
}

function numericFilter(txb) {
   txb.value = txb.value.replace(/[^\0-9]/ig, "");
}



  </script>




