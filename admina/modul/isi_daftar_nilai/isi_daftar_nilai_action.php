<?php
session_start();
include "../../inc/config.php";
session_check();

/** PHPExcel_IOFactory */
require_once '../../lib/PHPExcel/IOFactory.php';

switch ($_GET["act"]) {
   
    
   
    
  
  case 'change_nilai_kehadiran':

          if ($_POST['GA']== "A") {$index=4;}
     else if ($_POST['GA']== "B") {$index=3;}
     else if ($_POST['GA']== "C") {$index=2;}
     else if ($_POST['GA']== "D") {$index=1;}
     else if ($_POST['GA']== "E") {$index=0;}
    
    //Cek, Mata Kuliah dari Mhs, di Nilai
        
    $data_nilai = $db->fetch_custom_single("select * from view_0019_list_nilai_matkul where id_krs_mhs=?",array("cari" =>$_POST['id'] ));

    $data_cek = array("id_krs_mhs"=>$_POST["id"]                         );
    $check = $db->check_exist('nilai',$data_cek);
    
    

    if ($check==false) {         
          
          $data = array(
          "nim"=>$data_nilai->nim,
          "nama"=>$data_nilai->nama,
          "kode_mk"=>$data_nilai->kode_mk_mhs,
          "nama_mk"=>$data_nilai->nama_mk_mhs,
          "nama_kelas" => $data_nilai->nama_kelas,
          "semester" => $data_nilai->semester_mhs,
          "kode_jurusan" =>$data_nilai->kode_jurusan,
          "nilai_kehadiran" =>$_POST['nilai'],
          "id_krs_mhs"  =>$_POST['id'],
          "nilai_angka" =>$_POST['NA'],
          "nilai_huruf" =>$_POST['GA'],    
          "nilai_indek" =>$index,    
          );
          
          $in = $db->insert("nilai",$data);
          
          //lOG INSERT
          $data_log= array("log_user_id"=>$_SESSION['id_user'],
                           "log_waktu"=>date("Y-m-d H:m:s"),                           
                           "log_aktifitas"=>'INSERT GRADE  ' .$_POST['GA'].'  UNTUK MAHASISWA '. $data_nilai->nim . '-'.$data_nilai->nama ,); 
          $in_log = $db->insert("t_log_nilai",$data_log);      

          
    }
    else {
          
           $data = array(
          "nilai_kehadiran" =>$_POST['nilai'], 
          "nilai_angka" =>$_POST['NA'],  
          "nilai_huruf" =>$_POST['GA'],    
          "nilai_indek" =>$index,       
          
          );
          
          $in = $db->update("nilai",$data, 'id_krs_mhs', $_POST['id']); 
          
          //lOG UPDATE
          $data_log= array("log_user_id"=>$_SESSION['id_user'],
                           "log_waktu"=>date("Y-m-d H:m:s"),                           
                           "log_aktifitas"=>'UPDATE GRADE ' .$_POST['GA'].'  UNTUK MAHASISWA '. $data_nilai->nim . '-'.$data_nilai->nama ,);   
          $in_log = $db->insert("t_log_nilai",$data_log);   

          
    }
    
    break;
    

case 'change_nilai_tugas':

     if ($_POST['GA']== "A") {$index=4;}
     else if ($_POST['GA']== "B") {$index=3;}
     else if ($_POST['GA']== "C") {$index=2;}
     else if ($_POST['GA']== "D") {$index=1;}
     else if ($_POST['GA']== "E") {$index=0;}
    
    //Cek, Mata Kuliah dari Mhs, di Nilai
        
    $data_nilai = $db->fetch_custom_single("select * from view_0019_list_nilai_matkul where id_krs_mhs=?",array("cari" =>$_POST['id'] ));

    $data_cek = array("id_krs_mhs"=>$_POST["id"]                         );
    $check = $db->check_exist('nilai',$data_cek);

    if ($check==false) {         
          
          $data = array(
          "nim"=>$data_nilai->nim,
          "nama"=>$data_nilai->nama,
          "kode_mk"=>$data_nilai->kode_mk_mhs,
          "nama_mk"=>$data_nilai->nama_mk_mhs,
          "nama_kelas" => $data_nilai->nama_kelas,
          "semester" => $data_nilai->semester_mhs,
          "kode_jurusan" =>$data_nilai->kode_jurusan,
          "nilai_tugas" =>$_POST['nilai'],
          "id_krs_mhs" =>$_POST['id'],
          "nilai_angka" =>$_POST['NA'],
          "nilai_huruf" =>$_POST['GA'],    
          "nilai_indek" =>$index,    
          );
          
          $in = $db->insert("nilai",$data); 
          
          //lOG INSERT
          $data_log= array("log_user_id"=>$_SESSION['id_user'],
                           "log_waktu"=>date("Y-m-d H:m:s"),                           
                           "log_aktifitas"=>'INSERT GRADE  ' .$_POST['GA'].'  UNTUK MAHASISWA '. $data_nilai->nim . '-'.$data_nilai->nama ,); 
          $in_log = $db->insert("t_log_nilai",$data_log);      

          
    }
    else {
          
           $data = array(
          "nilai_tugas" =>$_POST['nilai'],   
          "nilai_angka" =>$_POST['NA'],
          "nilai_huruf" =>$_POST['GA'],    
          "nilai_indek" =>$index,    
          );
          
          $in = $db->update("nilai",$data, 'id_krs_mhs', $_POST['id']);
          
          //lOG UPDATE
          $data_log= array("log_user_id"=>$_SESSION['id_user'],
                           "log_waktu"=>date("Y-m-d H:m:s"),                           
                           "log_aktifitas"=>'UPDATE GRADE ' .$_POST['GA'].'  UNTUK MAHASISWA '. $data_nilai->nim . '-'.$data_nilai->nama ,);   
          $in_log = $db->insert("t_log_nilai",$data_log);   

          
    }
    
    break;


    case 'change_nilai_uts':

     if ($_POST['GA']== "A") {$index=4;}
     else if ($_POST['GA']== "B") {$index=3;}
     else if ($_POST['GA']== "C") {$index=2;}
     else if ($_POST['GA']== "D") {$index=1;}
     else if ($_POST['GA']== "E") {$index=0;}
    
    //Cek, Mata Kuliah dari Mhs, di Nilai
        
    $data_nilai = $db->fetch_custom_single("select * from view_0019_list_nilai_matkul where id_krs_mhs=?",array("cari" =>$_POST['id'] ));

    $data_cek = array("id_krs_mhs"=>$_POST["id"]                         );
    $check = $db->check_exist('nilai',$data_cek);

    if ($check==false) {         
          
          $data = array(
          "nim"=>$data_nilai->nim,
          "nama"=>$data_nilai->nama,
          "kode_mk"=>$data_nilai->kode_mk_mhs,
          "nama_mk"=>$data_nilai->nama_mk_mhs,
          "nama_kelas" => $data_nilai->nama_kelas,
          "semester" => $data_nilai->semester_mhs,
          "kode_jurusan" =>$data_nilai->kode_jurusan,
          "nilai_uts" =>$_POST['nilai'],
          "id_krs_mhs" =>$_POST['id'],
          "nilai_angka" =>$_POST['NA'],
          "nilai_huruf" =>$_POST['GA'],    
          "nilai_indek" =>$index,    
          );
          
          $in = $db->insert("nilai",$data);
          
          //lOG INSERT
          $data_log= array("log_user_id"=>$_SESSION['id_user'],
                           "log_waktu"=>date("Y-m-d H:m:s"),                           
                           "log_aktifitas"=>'INSERT GRADE  ' .$_POST['GA'].'  UNTUK MAHASISWA '. $data_nilai->nim . '-'.$data_nilai->nama ,); 
          $in_log = $db->insert("t_log_nilai",$data_log);      

          
    }
    else {
          
           $data = array(
          "nilai_uts" =>$_POST['nilai'],   
          "nilai_angka" =>$_POST['NA'],
          "nilai_huruf" =>$_POST['GA'],    
          "nilai_indek" =>$index,     
          );
          
          $in = $db->update("nilai",$data, 'id_krs_mhs', $_POST['id']); 
          
          //lOG UPDATE
          $data_log= array("log_user_id"=>$_SESSION['id_user'],
                           "log_waktu"=>date("Y-m-d H:m:s"),                           
                           "log_aktifitas"=>'UPDATE GRADE ' .$_POST['GA'].'  UNTUK MAHASISWA '. $data_nilai->nim . '-'.$data_nilai->nama ,);   
          $in_log = $db->insert("t_log_nilai",$data_log);   

          
    }
    
    break;

    case 'change_nilai_uas':

     if ($_POST['GA']== "A") {$index=4;}
     else if ($_POST['GA']== "B") {$index=3;}
     else if ($_POST['GA']== "C") {$index=2;}
     else if ($_POST['GA']== "D") {$index=1;}
     else if ($_POST['GA']== "E") {$index=0;}
    
    //Cek, Mata Kuliah dari Mhs, di Nilai
        
    $data_nilai = $db->fetch_custom_single("select * from view_0019_list_nilai_matkul where id_krs_mhs=?",array("cari" =>$_POST['id'] ));

    $data_cek = array("id_krs_mhs"=>$_POST["id"]                         );
    $check = $db->check_exist('nilai',$data_cek);

    if ($check==false) {         
          
          $data = array(
          "nim"=>$data_nilai->nim,
          "nama"=>$data_nilai->nama,
          "kode_mk"=>$data_nilai->kode_mk_mhs,
          "nama_mk"=>$data_nilai->nama_mk_mhs,
          "nama_kelas" => $data_nilai->nama_kelas,
          "semester" => $data_nilai->semester_mhs,
          "kode_jurusan" =>$data_nilai->kode_jurusan,
          "nilai_uas" =>$_POST['nilai'],
          "id_krs_mhs" =>$_POST['id'],
          "nilai_angka" =>$_POST['NA'],
          "nilai_huruf" =>$_POST['GA'],    
          "nilai_indek" =>$index,    
          );
          
          $in = $db->insert("nilai",$data); 
          
          //lOG INSERT
          $data_log= array("log_user_id"=>$_SESSION['id_user'],
                           "log_waktu"=>date("Y-m-d H:m:s"),                           
                           "log_aktifitas"=>'INSERT GRADE  ' .$_POST['GA'].'  UNTUK MAHASISWA '. $data_nilai->nim . '-'.$data_nilai->nama ,); 
          $in_log = $db->insert("t_log_nilai",$data_log);      

          
    }
    else {
          
           $data = array(
          "nilai_uas" =>$_POST['nilai'],   
          "nilai_angka" =>$_POST['NA'],
          "nilai_huruf" =>$_POST['GA'],    
          "nilai_indek" =>$index,    
          );
          
          $in = $db->update("nilai",$data, 'id_krs_mhs', $_POST['id']);   
          
          //lOG UPDATE
          $data_log= array("log_user_id"=>$_SESSION['id_user'],
                           "log_waktu"=>date("Y-m-d H:m:s"),                           
                           "log_aktifitas"=>'UPDATE GRADE ' .$_POST['GA'].'  UNTUK MAHASISWA '. $data_nilai->nim . '-'.$data_nilai->nama ,);   
          $in_log = $db->insert("t_log_nilai",$data_log);   

          
    }
    
    break;

case 'import':
  if (!is_dir("../../../upload/nilai")) {
              mkdir("../../../upload/nilai");
            }


   if (!preg_match("/.(xls|xlsx)$/i", $_FILES["semester"]["name"]) ) {

              echo "pastikan file yang anda pilih xls|xlsx";
              exit();

            } else {
              move_uploaded_file($_FILES["semester"]["tmp_name"], "../../../upload/nilai/".$_FILES['semester']['name']);
              $semester = array("semester"=>$_FILES["semester"]["name"]);

            }


    $objPHPExcel = PHPExcel_IOFactory::load("../../../upload/nilai/".$_FILES['semester']['name']);

    $data = $objPHPExcel->getActiveSheet()->toArray();

    $error_count = 0;
    $error = array();
    $sukses = 0;

    foreach ($data as $key => $val) {

        if ($key>0) {

    if ($val[1]!='') {
      
          if ($val[6]=='') {
          $nama_kelas = "01";
        } else {
          $nama_kelas =filter_var($val[6], FILTER_UNSAFE_RAW, FILTER_FLAG_STRIP_LOW|FILTER_FLAG_STRIP_HIGH);
        }

        if ($val[7]!='') {
            $check = $db->check_exist('nilai',array('nim'=>filter_var($val[1], FILTER_UNSAFE_RAW, FILTER_FLAG_STRIP_LOW|FILTER_FLAG_STRIP_HIGH),'kode_mk' => filter_var($val[3], FILTER_UNSAFE_RAW, FILTER_FLAG_STRIP_LOW|FILTER_FLAG_STRIP_HIGH),'semester'=>filter_var($val[6], FILTER_UNSAFE_RAW, FILTER_FLAG_STRIP_LOW|FILTER_FLAG_STRIP_HIGH),'nama_kelas'=>$nama_kelas));
        if ($check==true) {
          $error_count++;
          $error[] = $val[1]." ".$val[3]." Sudah Ada";
        } else {
          $sukses++;
          $kode_mk = filter_var($val[3], FILTER_UNSAFE_RAW, FILTER_FLAG_STRIP_LOW|FILTER_FLAG_STRIP_HIGH);
          $nim_mhs = trim(filter_var($val[1], FILTER_UNSAFE_RAW, FILTER_FLAG_STRIP_LOW|FILTER_FLAG_STRIP_HIGH));
          $id_krs_mhs = $db->fetch_custom_single("select id_krs_mhs from view_0016_krs_mhs where kode_mk=? and nim=?",array("cari1" =>$kode_mk, "cari2" =>$nim_mhs));

        $data = array(
          'semester' => filter_var($val[5], FILTER_UNSAFE_RAW, FILTER_FLAG_STRIP_LOW|FILTER_FLAG_STRIP_HIGH),
          'nim' => trim(filter_var($val[1], FILTER_UNSAFE_RAW, FILTER_FLAG_STRIP_LOW|FILTER_FLAG_STRIP_HIGH)),
          'nama' => filter_var($val[2], FILTER_UNSAFE_RAW, FILTER_FLAG_STRIP_LOW|FILTER_FLAG_STRIP_HIGH),
          'kode_mk' =>  filter_var($val[3], FILTER_UNSAFE_RAW, FILTER_FLAG_STRIP_LOW|FILTER_FLAG_STRIP_HIGH),
          'nama_mk' => filter_var($val[4], FILTER_UNSAFE_RAW, FILTER_FLAG_STRIP_LOW|FILTER_FLAG_STRIP_HIGH),
          'nama_kelas' => trim($nama_kelas),
          'nilai_huruf' =>filter_var($val[7], FILTER_UNSAFE_RAW, FILTER_FLAG_STRIP_LOW|FILTER_FLAG_STRIP_HIGH),
          'nilai_indek' => filter_var($val[8], FILTER_UNSAFE_RAW, FILTER_FLAG_STRIP_LOW|FILTER_FLAG_STRIP_HIGH),
          'nilai_angka' => filter_var($val[9], FILTER_UNSAFE_RAW, FILTER_FLAG_STRIP_LOW|FILTER_FLAG_STRIP_HIGH),
          'kode_jurusan' => $_POST['jurusan'],

          'nilai_kehadiran' => filter_var($val[10], FILTER_UNSAFE_RAW, FILTER_FLAG_STRIP_LOW|FILTER_FLAG_STRIP_HIGH),
          'nilai_tugas' => filter_var($val[11], FILTER_UNSAFE_RAW, FILTER_FLAG_STRIP_LOW|FILTER_FLAG_STRIP_HIGH),
          'nilai_uts' => filter_var($val[12], FILTER_UNSAFE_RAW, FILTER_FLAG_STRIP_LOW|FILTER_FLAG_STRIP_HIGH),
          'nilai_uas' => filter_var($val[13], FILTER_UNSAFE_RAW, FILTER_FLAG_STRIP_LOW|FILTER_FLAG_STRIP_HIGH),
          'id_krs_mhs' => $id_krs_mhs->id_krs_mhs

          );
          $in = $db->insert('nilai',$data);
            }
        }

    }
          
        }
       
    }


        unlink("../../../upload/nilai/".$_FILES['semester']['name']);
        $msg = '';
        if (($sukses>0) || ($error_count>0)) {
          $msg =  "<div class=\"alert alert-warning alert-dismissible\" role=\"alert\" >
          <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">×</button>
              <font color=\"#3c763d\">".$sukses." Data Nilai  berhasil di import</font><br />
              <font color=\"#ce4844\" >".$error_count." data tidak bisa ditambahkan </font>";
              if (!$error_count==0) {
                $msg .= "<a data-toggle=\"collapse\" href=\"#collapseExample\" aria-expanded=\"false\" aria-controls=\"collapseExample\">Detail error</a>";
              }
              //echo "<br />Total: ".$i." baris data";
              $msg .= "<div class=\"collapse\" id=\"collapseExample\">";
                  $i=1;
                  foreach ($error as $pesan) {
                      $msg .= "<div class=\"bs-callout bs-callout-danger\">".$i.". ".$pesan."</div><br />";
                    $i++;
                    }
              $msg .= "</div>
            </div>";
        }
        echo $msg;
        break;

/*
  case 'change_insert':
    $db->update('sys_menu_role',array('insert_act'=>$_POST['data_act']),'id',$_POST['role_id']);
    break;

  case 'change_update':
    $db->update('sys_menu_role',array('update_act'=>$_POST['data_act']),'id',$_POST['role_id']);
    break;
  case 'change_push':
    $db->update('sys_menu_role',array('push_act'=>$_POST['data_act']),'id',$_POST['role_id']);
    break;
  case 'change_delete':
    $db->update('sys_menu_role',array('delete_act'=>$_POST['data_act']),'id',$_POST['role_id']);
    break;

  case "delete":
    $db->delete("sys_menu_role","id",$_GET["id"]);
    break;

*/    
  default:
    # code...
    break;
}



?>