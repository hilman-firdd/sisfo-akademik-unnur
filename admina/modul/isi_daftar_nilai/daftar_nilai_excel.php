<?php  
//export.php  
$connect = mysqli_connect("localhost", "root", "", "ekomyanu_akademik");
//$connect = mysqli_connect("localhost", "root", "", "pengawas");
$output = '';

date_default_timezone_set('Asia/Jakarta');
$tgl=date('l, d-m-Y, H:i:s');
$tahun=date('Y');

$syarat =  $_GET['customvar'];

$array_data = array();
$array_data = explode("  -  ",$syarat);

$semester = $array_data[0];
   $kelas = $array_data[1];
$username = $array_data[2];   
    $name = $array_data[3];
 $kode_mk = $array_data[4];
 $nama_mk = $array_data[5];

$array_jadwal = array();
if ($kelas=="Pagi"){    
 //$query="select DISTINCT * from view_0019_list_nilai_matkul where status_krs=1 and semester=" . $semester . " and kode_mk_mhs=". $kode_mk . " and dosen_nidn_pagi=" .$username .  " and level LIKE '%Pagi%' " ;
 $query="SELECT distinct * from view_0019_list_nilai_matkul where kode_mk_mhs=". $kode_mk . " and dosen_nidn_pagi=".$username . " and status_krs=1 and level LIKE '%Pagi%' and semester_mhs=" . $semester ;

}
else
{
 $query="SELECT distinct * from view_0019_list_nilai_matkul where kode_mk_mhs=". $kode_mk . " and dosen_nidn_sore=".$username . " and status_krs=1 and level LIKE '%Sore%' and semester_mhs=" . $semester ;
  
}

 
 
 
 $result = mysqli_query($connect, $query);
 
 

//HEADER TABLE
 $output .= '
             

             <table border="1" >  
                              <tr style="background-color: gray;" height="50">  
                                   <th>No.</th>                           
                                   <th>NIM</th>  
                                   <th>Nama Mahasiswa</th>
                                   <th>Kode MK</th>  
                                   <th>Mata Kuliah</th>
                                   <th>Semester</th>
                                   <th>Kelas</th>
                                   <th>Nilai Huruf</th>
                                   <th>Nilai Indek</th>
                                   <th>Nilai Angka</th>
                                   <th>Nilai Kehadiran</th>
                                   <th>Nilai Tugas</th>
                                   <th>Nilai UTS</th>
                                   <th>Nilai UAS</th>
                              </tr>
            ';

 //LOOPING
 $i=0;
 while($row = mysqli_fetch_array($result)){
            
           
            $i=$i+1;
              
             //CONTENT TABLE
             $output .= '
              <tr  height="35">  
                                   <td  align="center" valign="top" >'.$i.'</td>   
                                   
                                   <td valign="top">'.$row["nim"].'</td>  
                                   <td valign="top">'.$row["nama"].'</td>                                                               
                                   <td align="center" valign="top">'.$row["kode_mk_mhs"].'</td>  
                                   <td align="center" valign="top">'.$row["nama_mk_mhs"].'</td>  
                                   <td align="center" valign="top">'.$row["semester_mhs"].'</td> 
                                   <td align="center" valign="top">'.$row["nama_kelas"].'</td> 
                                   <td valign="top" align="center">'.$row["nilai_huruf"].'</td>
                                   <td valign="top" align="center">'.$row["nilai_indek"].'</td>
                                   <td valign="top" align="center">'.$row["nilai_angka"].'</td>
                                   <td valign="top" align="center">'.$row["nilai_kehadiran"].'</td>
                                   <td valign="top" align="center">'.$row["nilai_tugas"].'</td>
                                   <td valign="top" align="center">'.$row["nilai_uts"].'</td>
                                   <td valign="top" align="center">'.$row["nilai_uas"].'</td>
             </tr>
             ';
            

  }
  
  $output .= '</table>';

  header('Content-Type: application/xls');
  header('Content-Disposition: attachment; filename=download_daftar_nilai.xls');
  echo $output;
 
//}
?>
