
           
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                     Bobot Nilai
                    </h1>
                           <ol class="breadcrumb">
                        <li><a href="<?=base_index();?>"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li><a href="<?=base_index();?>bobot-nilai">Bobot Nilai</a></li>
                        <li class="active">Tambah Bobot Nilai</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
<div class="row">
    <div class="col-lg-12"> 
        <div class="box box-solid box-primary">
                                 <div class="box-header">
                                    <h3 class="box-title">Tambah Bobot Nilai</h3>
                                    <div class="box-tools pull-right">
                                        <button class="btn btn-info btn-sm" data-widget="collapse"><i class="fa fa-minus"></i></button>
                                        <button class="btn btn-info btn-sm" data-widget="remove"><i class="fa fa-times"></i></button>
                                    </div>
                                </div>

                  <div class="box-body">
                     <form id="update" method="post" class="form-horizontal foto_banyak" action="<?=base_admin();?>modul/bobot_nilai/bobot_nilai_action.php?act=in">
                      <div class="form-group">
                        <label for="bobot_matkul" class="control-label col-lg-2">Mata Kuliah</label>
                        <div class="col-lg-10">
                          <select id="bobot_matkul" name="bobot_matkul" data-placeholder="Pilih Mata Kuliah ..." class="form-control chzn-select" tabindex="2" required>
               
               <?php 
                $username=ucwords($db->fetch_single_row('sys_users','id',$_SESSION['id_user'])->username);
                if($username=="Admin"){
                   foreach ($db->fetch_all("view_0017_list_matkul_yg_diajar_dosen") as $isi) {
                      echo "<option value='$isi->cari'>$isi->cari</option>";
                   }
                 }
                 else{
                  foreach ($db->fetch_custom("select * from view_0017_list_matkul_yg_diajar_dosen where NIDN=$username ") as $isi) {
                      echo "<option value='$isi->cari'>$isi->cari</option>";
                   }
                 }  
               
                    ?>
              </select>
                        </div>
                      </div><!-- /.form-group -->
<div class="form-group">
                        <label for="Bobot Nilai Kehadiran" class="control-label col-lg-2">Bobot Nilai Kehadiran (%)</label>
                        <div class="col-lg-1">
                          <input type="text" data-rule-number="true" id="bobot_kehadiran" value="10" onkeyup="sum();" name="bobot_kehadiran" placeholder="Bobot Nilai Kehadiran" class="form-control" required> 
                        </div>
                      </div><!-- /.form-group -->
<div class="form-group">
                        <label for="Bobot Nilai Tugas" class="control-label col-lg-2">Bobot Nilai Tugas (%)</label>
                        <div class="col-lg-1">
                          <input type="text" data-rule-number="true" id="bobot_tugas" value="20" onkeyup="sum();" name="bobot_tugas" placeholder="Bobot Nilai Tugas" class="form-control" required> 
                        </div>
                      </div><!-- /.form-group -->
<div class="form-group">
                        <label for="Bobot Nilai UTS" class="control-label col-lg-2">Bobot Nilai UTS (%)</label>
                        <div class="col-lg-1">
                          <input type="text" data-rule-number="true" id="bobot_uts"  value="30"onkeyup="sum();" name="bobot_uts" placeholder="Bobot Nilai UTS" class="form-control" required> 
                        </div>
                      </div><!-- /.form-group -->
<div class="form-group">
                        <label for="Bobot Nilai UAS" class="control-label col-lg-2">Bobot Nilai UAS (%)</label>
                        <div class="col-lg-1">
                          <input type="text" data-rule-number="true" id="bobot_uas" value="40" onkeyup="sum();" name="bobot_uas" placeholder="Bobot Nilai UAS" class="form-control" required> 
                        </div>
                      </div><!-- /.form-group -->

<div class="form-group">
                        <label for="Total" class="control-label col-lg-2">Total (%)</label>
                        <div class="col-lg-1">
                          <input type="text" data-rule-number="true" id="total" value="100" name="total" placeholder="Total Bobot" class="form-control" > 
                        </div>
                      </div><!-- /.form-group -->


<div class="alert alert-danger update" style="display:none">   
    <button type="button" class="close" data-dismiss="alert">&times;</button>
    <strong>Maaf, tambah data bobot mata kuliah tersebut sudah Anda input, lakukan edit untuk melakukan perubahan data...    
    </strong> 
</div>    

                      <div class="form-group">
                        <label for="tags" class="control-label col-lg-2">&nbsp;</label>
                        <div class="col-lg-10">
                          <input type="submit" class="btn btn-primary btn-flat" value="submit">
                        </div>
                      </div><!-- /.form-group -->
                    </form>
 <a href="<?=base_index();?>bobot-nilai" class="btn btn-success btn-flat"><i class="fa fa-step-backward"></i> Kembali</a>
          
                  </div>
                  </div>
              </div>
</div>
                  
                </section><!-- /.content -->

<script>          
    function sum() {
        var txt_bobot_kehadiran = document.getElementById("bobot_kehadiran").value;
        var txt_bobot_tugas = document.getElementById("bobot_tugas").value;
        var txt_bobot_uts = document.getElementById("bobot_uts").value;
        var txt_bobot_uas = document.getElementById("bobot_uas").value;
        var result = parseFloat(txt_bobot_kehadiran)+
                     parseFloat(txt_bobot_tugas)+
                     parseFloat(txt_bobot_uts)+
                     parseFloat(txt_bobot_uas);
        if (!isNaN (result)){
            document.getElementById("total").value = result;
        }
        
        /*
        if (document.getElementById("total").value !== 100 )
          {alert("Hello! I am an alert box!!");}
        */  

    }
</script>