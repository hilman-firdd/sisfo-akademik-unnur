

                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                      Bobot Nilai
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="<?=base_index();?>"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li><a href="<?=base_index();?>bobot-nilai">Bobot Nilai</a></li>
                        <li class="active">Edit Bobot Nilai</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
<div class="row">
    <div class="col-lg-12">
        <div class="box box-solid box-primary">
                                   <div class="box-header">
                                    <h3 class="box-title">Edit Bobot Nilai</h3>
                                    <div class="box-tools pull-right">
                                        <button class="btn btn-info btn-sm" data-widget="collapse"><i class="fa fa-minus"></i></button>
                                        <button class="btn btn-info btn-sm" data-widget="remove"><i class="fa fa-times"></i></button>
                                    </div>
                                </div>

                  <div class="box-body">
                     <form id="update"  method="post" class="form-horizontal" action="<?=base_admin();?>modul/bobot_nilai/bobot_nilai_action.php?act=up">
                      <body onload="sum()">
                      <div class="form-group">
                        <label for="Mata Kuliah" class="control-label col-lg-2">Mata Kuliah</label>
                        <div class="col-lg-10">
                          <select name="bobot_matkul" data-placeholder="Pilih Mata Kuliah..." class="form-control chzn-select" tabindex="2" disabled>
               <option value=""></option>
               <?php foreach ($db->fetch_all("view_0017_list_matkul_yg_diajar_dosen") as $isi) {

                  if ($data_edit->bobot_matkul==$isi->cari) {
                    echo "<option value='$isi->cari' selected>$isi->cari</option>";
                  } else {
                  echo "<option value='$isi->cari'>$isi->cari</option>";
                    }
               } ?>
              </select>
                        </div>
                      </div><!-- /.form-group -->
<div class="form-group">
                        <label for="Bobot Nilai Kehadiran" class="control-label col-lg-2">Bobot Nilai Kehadiran</label>
                        <div class="col-lg-1">
                          <input type="text" data-rule-number="true" id="bobot_kehadiran" name="bobot_kehadiran" onkeyup="sum();" value="<?=$data_edit->bobot_kehadiran;?>" class="form-control" required> 
                        </div>
                      </div><!-- /.form-group -->
<div class="form-group">
                        <label for="Bobot Nilai Tugas" class="control-label col-lg-2">Bobot Nilai Tugas</label>
                        <div class="col-lg-1">
                          <input type="text" data-rule-number="true" id="bobot_tugas" name="bobot_tugas" onkeyup="sum();" value="<?=$data_edit->bobot_tugas;?>" class="form-control" required> 
                        </div>
                      </div><!-- /.form-group -->
<div class="form-group">
                        <label for="Bobot Nilai UTS" class="control-label col-lg-2">Bobot Nilai UTS</label>
                        <div class="col-lg-1">
                          <input type="text" data-rule-number="true" id="bobot_uts" name="bobot_uts" onkeyup="sum();"  value="<?=$data_edit->bobot_uts;?>" class="form-control" required> 
                        </div>
                      </div><!-- /.form-group -->
<div class="form-group">
                        <label for="Bobot Nilai UAS" class="control-label col-lg-2">Bobot Nilai UAS</label>
                        <div class="col-lg-1">
                          <input type="text" data-rule-number="true" id="bobot_uas" name="bobot_uas" onkeyup="sum();" value="<?=$data_edit->bobot_uas;?>" class="form-control" required> 
                        </div>
                      </div><!-- /.form-group -->
<div class="form-group">
                        <label for="Total" class="control-label col-lg-2">Total (%)</label>
                        <div class="col-lg-1">
                          <input type="text" data-rule-number="true" id="total" value="0" name="total" placeholder="Total Bobot" class="form-control" > 
                        </div>
                      </div><!-- /.form-group -->        

 

                      <input type="hidden" name="id" value="<?=$data_edit->bobot_id;?>">
                      <div class="form-group">
                        <label for="tags" class="control-label col-lg-2">&nbsp;</label>
                        <div class="col-lg-10">
                          <input type="submit" class="btn btn-primary btn-flat" value="submit">
                        </div>
                      </div><!-- /.form-group -->
                    </body>  
                    </form>
                    <a href="<?=base_index();?>bobot-nilai" class="btn btn-success btn-flat"><i class="fa fa-step-backward"></i> Kembali</a>
          
                  </div>
                  </div>
              </div>
</div>
                  
                </section><!-- /.content -->
        
<script>          
    function sum() {
        var txt_bobot_kehadiran = document.getElementById("bobot_kehadiran").value;
        var txt_bobot_tugas = document.getElementById("bobot_tugas").value;
        var txt_bobot_uts = document.getElementById("bobot_uts").value;
        var txt_bobot_uas = document.getElementById("bobot_uas").value;
        var result = parseFloat(txt_bobot_kehadiran)+
                     parseFloat(txt_bobot_tugas)+
                     parseFloat(txt_bobot_uts)+
                     parseFloat(txt_bobot_uas);
        if (!isNaN (result)){
            document.getElementById("total").value = result;
        }
        
        /*
        if (document.getElementById("total").value !== 100 )
          {alert("Hello! I am an alert box!!");}
        */  

    }
</script>