

                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                     Bobot Nilai
                    </h1>
                   <ol class="breadcrumb">
                        <li><a href="<?=base_index();?>"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li><a href="<?=base_index();?>bobot-nilai">Bobot Nilai</a></li>
                        <li class="active">Detail Bobot Nilai</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
<div class="row">
    <div class="col-lg-12">
        <div class="box box-solid box-primary">
                                   <div class="box-header">
                                    <h3 class="box-title">Detail Bobot Nilai</h3>
                                    <div class="box-tools pull-right">
                                        <button class="btn btn-info btn-sm" data-widget="collapse"><i class="fa fa-minus"></i></button>
                                        <button class="btn btn-info btn-sm" data-widget="remove"><i class="fa fa-times"></i></button>
                                    </div>
                                </div>

                  <div class="box-body">
                   <form class="form-horizontal">
                      <div class="form-group">
                        <label for="Mata Kuliah" class="control-label col-lg-2">Mata Kuliah</label>
                        <div class="col-lg-10">
                          <?php foreach ($db->fetch_all("view_0017_list_matkul_yg_diajar_dosen") as $isi) {
                  if ($data_edit->bobot_matkul==$isi->cari) {

                    echo "<input disabled class='form-control' type='text' value='$isi->cari'>";
                  }
               } ?>
              
                        </div>
                      </div><!-- /.form-group -->
<div class="form-group">
                        <label for="Bobot Nilai Kehadiran" class="control-label col-lg-2">Bobot Nilai Kehadiran</label>
                        <div class="col-lg-1">
                          <input type="text" disabled="" value="<?=$data_edit->bobot_kehadiran;?>" class="form-control">
                        </div>
                      </div><!-- /.form-group -->
<div class="form-group">
                        <label for="Bobot Nilai Tugas" class="control-label col-lg-2">Bobot Nilai Tugas</label>
                        <div class="col-lg-1">
                          <input type="text" disabled="" value="<?=$data_edit->bobot_tugas;?>" class="form-control">
                        </div>
                      </div><!-- /.form-group -->
<div class="form-group">
                        <label for="Bobot Nilai UTS" class="control-label col-lg-2">Bobot Nilai UTS</label>
                        <div class="col-lg-1">
                          <input type="text" disabled="" value="<?=$data_edit->bobot_uts;?>" class="form-control">
                        </div>
                      </div><!-- /.form-group -->
<div class="form-group">
                        <label for="Bobot Nilai UAS" class="control-label col-lg-2">Bobot Nilai UAS</label>
                        <div class="col-lg-1">
                          <input type="text" disabled="" value="<?=$data_edit->bobot_uas;?>" class="form-control">
                        </div>
                      </div><!-- /.form-group -->

                   
                    </form>
                    <a href="<?=base_index();?>bobot-nilai" class="btn btn-success btn-flat"><i class="fa fa-step-backward"></i> Kembali</a>
          
                  </div>
                  </div>
              </div>
</div>
                  
                </section><!-- /.content -->
        
