
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        Manage Bobot Nilai
                    </h1>
                        <ol class="breadcrumb">
                        <li><a href="<?=base_index();?>"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li><a href="<?=base_index();?>bobot-nilai">Bobot Nilai</a></li>
                        <li class="active">Bobot Nilai List</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="box">
                                <div class="box-header">
                                <h3 class="box-title">List Bobot Nilai</h3>
                                </div><!-- /.box-header -->
                                <div class="box-body table-responsive">
                                    <table id="dtb_bobot_nilai"  class="table table-bordered table-striped">
                                   <thead>
                                     <tr>

                          <th>[Semester]-[Kelas]-[ID Dosen]-[Nama Dosen]-[Kode MK]-[Nama MK]</th>
													<th>Bobot Nilai Kehadiran (%)</th>
													<th>Bobot Nilai Tugas (%)</th>
													<th>Bobot Nilai UTS (%)</th>
													<th>Bobot Nilai UAS (%)</th>
													
                          <th>Action</th>
                         
                        </tr>
                                      </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div><!-- /.box-body -->
                            </div><!-- /.box -->
                        </div>
                    </div>

       <?php

       $username=$db->fetch_single_row('sys_users','id',$_SESSION['id_user'])->username;

       if ($username <> "Admin") { $syarat = $username;}
       else {$syarat = "";}


      

       foreach ($db->fetch_all("sys_menu") as $isi) {
                      if ($path_url==$isi->url) {
                          if ($role_act["insert_act"]=="Y") {
                    ?>
       <a href="<?=base_index();?>bobot-nilai/tambah" class="btn btn-primary btn-flat"><i class="fa fa-plus"></i> Tambah</a>
                          <?php
                          } 
                       } 
}
      
  foreach ($db->fetch_all("sys_menu") as $isi) {

  //jika url = url dari table menu
  if ($path_url==$isi->url) {
    //check edit permission
  if ($role_act["up_act"]=="Y") {
  $edit = '<a href="'.base_index()."bobot-nilai/edit/'+aData[indek]+'".'" class="btn btn-primary btn-flat"><i class="fa fa-pencil"></i></a>';
  } else {
    $edit ="";
  }
  if ($role_act['del_act']=='Y') {
   $del = "<span data-id='+aData[indek]+' data-uri=".base_admin()."modul/bobot_nilai/bobot_nilai_action.php".' class="btn btn-danger hapus btn-flat"><i class="fa fa-trash"></i></span>';
  } else {
    $del="";
  }
                   } 
  }
  
?>  
                </section><!-- /.content -->
        <script type="text/javascript">
var dataTable = $("#dtb_bobot_nilai").dataTable({
           "fnCreatedRow": function( nRow, aData, iDataIndex ) {
            var indek = aData.length-1;           
     $('td:eq('+indek+')', nRow).html(' <a href="<?=base_index();?>bobot-nilai/detail/'+aData[indek]+'" class="btn btn-success btn-flat"><i class="fa fa-eye"></i></a> <?=$edit;?> <?=$del;?>');
       $(nRow).attr('id', 'line_'+aData[indek]);
   },
           'bProcessing': true,
           'bServerSide': true,

           'sAjaxSource': '<?=base_admin();?>modul/bobot_nilai/bobot_nilai_data.php?customvar= <?php echo $syarat; ?>',

              'aoColumnDefs': [{ className: 'text-center', targets: [1,2,3,4],
                'bSortable': false
                
            }],
        });</script>  

