
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        Manage Absensi Kuliah
                    </h1>
                        <ol class="breadcrumb">
                        <li><a href="<?=base_index();?>"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li><a href="<?=base_index();?>absensi-kuliah">Absensi Kuliah</a></li>
                        <li class="active">Absensi Kuliah List</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="box">
                                <div class="box-header">
                                <h3 class="box-title">List Absensi Kuliah</h3>
                                </div><!-- /.box-header -->
                                <div class="box-body table-responsive">
                                    <table id="dtb_absensi_kuliah" class="table table-bordered table-striped">
                                   <thead>
                                     <tr>

                          <th>absen_id</th>
													<th>kode_mk</th>
													<th>nama_mk</th>
													<th>nidn</th>
													<th>nama_dosen</th>
													<th>absen_tgl</th>
													<th>nama_yang_absensi</th>
													
                          <th>Action</th>
                         
                        </tr>
                                      </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div><!-- /.box-body -->
                            </div><!-- /.box -->
                        </div>
                    </div>
        <?php
       foreach ($db->fetch_all("sys_menu") as $isi) {
                      if ($path_url==$isi->url) {
                          if ($role_act["insert_act"]=="Y") {
                    ?>
          <a href="<?=base_index();?>absensi-kuliah/tambah" class="btn btn-primary btn-flat"><i class="fa fa-plus"></i> Tambah</a>
                          <?php
                          } 
                       } 
}
  

  //here
  $nama_dpn_user=ucwords($db->fetch_single_row('sys_users','id',$_SESSION['id_user'])->username);

  $id_group=ucwords($db->fetch_single_row('sys_users','id',$_SESSION['id_user'])->id_group);

  $syarat = "";                
  if($id_group==3) {$syarat = " WHERE nim =".$nama_dpn_user;}
  if($id_group==4) {$syarat = " WHERE nim =".$nama_dpn_user;}
  if($id_group==5) {$syarat = " WHERE view_0001_list_absensi_mahasiswa.nidn=".$nama_dpn_user;}
  if($id_group==6) {$syarat = " WHERE view_0001_list_absensi_mahasiswa.nidn=".$nama_dpn_user;}
  

  foreach ($db->fetch_all("sys_menu") as $isi) {

  //jika url = url dari table menu
  if ($path_url==$isi->url) {
    //check edit permission
  if ($role_act["up_act"]=="Y") {
  $edit = '<a href="'.base_index()."absensi-kuliah/edit/'+aData[indek]+'".'" class="btn btn-primary btn-flat"><i class="fa fa-pencil"></i></a>';
  } else {
    $edit ="";
  }
  if ($role_act['del_act']=='Y') {
   $del = "<span data-id='+aData[indek]+' data-uri=".base_admin()."modul/absensi_kuliah/absensi_kuliah_action.php".' class="btn btn-danger hapus btn-flat"><i class="fa fa-trash"></i></span>';
  } else {
    $del="";
  }
                   } 
  }
  
?>  
                </section><!-- /.content -->
        <script type="text/javascript">
var dataTable = $("#dtb_absensi_kuliah").dataTable({
           "fnCreatedRow": function( nRow, aData, iDataIndex ) {
            var indek = aData.length-1;           
     $('td:eq('+indek+')', nRow).html(' <a href="<?=base_index();?>absensi-kuliah/detail/'+aData[indek]+'" class="btn btn-success btn-flat"><i class="fa fa-eye"></i></a> <?=$edit;?> <?=$del;?>');
       $(nRow).attr('id', 'line_'+aData[indek]);
   },
           'bProcessing': true,
            'bServerSide': true,
        'sAjaxSource': '<?=base_admin();?>modul/absensi_kuliah/absensi_kuliah_data.php?customvar=<?php echo $syarat;?>',
         /*     'aoColumnDefs': [{
                'bSortable': false,
                'aTargets': [0]
            }]*/
        });</script>  
            