

                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                      Absensi Kuliah
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="<?=base_index();?>"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li><a href="<?=base_index();?>absensi-kuliah">Absensi Kuliah</a></li>
                        <li class="active">Edit Absensi Kuliah</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
<div class="row">
    <div class="col-lg-12">
        <div class="box box-solid box-primary">
                                   <div class="box-header">
                                    <h3 class="box-title">Edit Absensi Kuliah</h3>
                                    <div class="box-tools pull-right">
                                        <button class="btn btn-info btn-sm" data-widget="collapse"><i class="fa fa-minus"></i></button>
                                        <button class="btn btn-info btn-sm" data-widget="remove"><i class="fa fa-times"></i></button>
                                    </div>
                                </div>

                  <div class="box-body">
                     <form id="update" method="post" class="form-horizontal" action="<?=base_admin();?>modul/absensi_kuliah/absensi_kuliah_action.php?act=up">
                      <div class="form-group">
                        <label for="kode_mk" class="control-label col-lg-2">kode_mk</label>
                        <div class="col-lg-10">
                          <select name="kode_mk" data-placeholder="Pilih kode_mk..." class="form-control chzn-select" tabindex="2" >
               <option value=""></option>
               <?php foreach ($db->fetch_all("mat_kurikulum") as $isi) {

                  if ($data_edit->kode_mk==$isi->kode_mk) {
                    echo "<option value='$isi->kode_mk' selected>$isi->nama_mk</option>";
                  } else {
                  echo "<option value='$isi->kode_mk'>$isi->nama_mk</option>";
                    }
               } ?>
              </select>
                        </div>
                      </div><!-- /.form-group -->
<div class="form-group">
                        <label for="nidn" class="control-label col-lg-2">nidn</label>
                        <div class="col-lg-10">
                          <select name="nidn" data-placeholder="Pilih nidn..." class="form-control chzn-select" tabindex="2" >
               <option value=""></option>
               <?php foreach ($db->fetch_all("ajar_dosen") as $isi) {

                  if ($data_edit->nidn==$isi->nidn) {
                    echo "<option value='$isi->nidn' selected>$isi->nama_dosen</option>";
                  } else {
                  echo "<option value='$isi->nidn'>$isi->nama_dosen</option>";
                    }
               } ?>
              </select>
                        </div>
                      </div><!-- /.form-group -->
<div class="form-group">
                        <label for="absen_tgl" class="control-label col-lg-2">absen_tgl</label>
                        <div class="col-lg-10">
                          <input type="text" id="tgl1" data-rule-date="true" name="absen_tgl" value="<?=$data_edit->absen_tgl;?>" class="form-control" > 
                        </div>
                      </div><!-- /.form-group -->
<div class="form-group">
                        <label for="nim" class="control-label col-lg-2">nim</label>
                        <div class="col-lg-10">
                          <select name="nim" data-placeholder="Pilih nim..." class="form-control chzn-select" tabindex="2" >
               <option value=""></option>
               <?php foreach ($db->fetch_all("mhs") as $isi) {

                  if ($data_edit->nim==$isi->nipd) {
                    echo "<option value='$isi->nipd' selected>$isi->nm_pd</option>";
                  } else {
                  echo "<option value='$isi->nipd'>$isi->nm_pd</option>";
                    }
               } ?>
              </select>
                        </div>
                      </div><!-- /.form-group -->

                      <input type="hidden" name="id" value="<?=$data_edit->absen_id;?>">
                      <div class="form-group">
                        <label for="tags" class="control-label col-lg-2">&nbsp;</label>
                        <div class="col-lg-10">
                          <input type="submit" class="btn btn-primary btn-flat" value="submit">
                        </div>
                      </div><!-- /.form-group -->
                    </form>
                    <a href="<?=base_index();?>absensi-kuliah" class="btn btn-success btn-flat"><i class="fa fa-step-backward"></i> Kembali</a>
          
                  </div>
                  </div>
              </div>
</div>
                  
                </section><!-- /.content -->
        
 