

                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                     Absensi Kuliah
                    </h1>
                   <ol class="breadcrumb">
                        <li><a href="<?=base_index();?>"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li><a href="<?=base_index();?>absensi-kuliah">Absensi Kuliah</a></li>
                        <li class="active">Detail Absensi Kuliah</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
<div class="row">
    <div class="col-lg-12">
        <div class="box box-solid box-primary">
                                   <div class="box-header">
                                    <h3 class="box-title">Detail Absensi Kuliah</h3>
                                    <div class="box-tools pull-right">
                                        <button class="btn btn-info btn-sm" data-widget="collapse"><i class="fa fa-minus"></i></button>
                                        <button class="btn btn-info btn-sm" data-widget="remove"><i class="fa fa-times"></i></button>
                                    </div>
                                </div>

                  <div class="box-body">
                   <form class="form-horizontal">
                      <div class="form-group">
                        <label for="kode_mk" class="control-label col-lg-2">kode_mk</label>
                        <div class="col-lg-10">
                          <?php foreach ($db->fetch_all("mat_kurikulum") as $isi) {
                  if ($data_edit->kode_mk==$isi->kode_mk) {

                    echo "<input disabled class='form-control' type='text' value='$isi->nama_mk'>";
                  }
               } ?>
              
                        </div>
                      </div><!-- /.form-group -->
<div class="form-group">
                        <label for="nidn" class="control-label col-lg-2">nidn</label>
                        <div class="col-lg-10">
                          <?php foreach ($db->fetch_all("ajar_dosen") as $isi) {
                  if ($data_edit->nidn==$isi->nidn) {

                    echo "<input disabled class='form-control' type='text' value='$isi->nama_dosen'>";
                  }
               } ?>
              
                        </div>
                      </div><!-- /.form-group -->
<div class="form-group">
                        <label for="absen_tgl" class="control-label col-lg-2">absen_tgl</label>
                        <div class="col-lg-10">
                          <input type="text" disabled="" value="<?=tgl_indo($data_edit->absen_tgl);?>" class="form-control">
                        </div>
                      </div><!-- /.form-group -->
<div class="form-group">
                        <label for="nim" class="control-label col-lg-2">nim</label>
                        <div class="col-lg-10">
                          <?php foreach ($db->fetch_all("mhs") as $isi) {
                  if ($data_edit->nim==$isi->nipd) {

                    echo "<input disabled class='form-control' type='text' value='$isi->nm_pd'>";
                  }
               } ?>
              
                        </div>
                      </div><!-- /.form-group -->

                   
                    </form>
                    <a href="<?=base_index();?>absensi-kuliah" class="btn btn-success btn-flat"><i class="fa fa-step-backward"></i> Kembali</a>
          
                  </div>
                  </div>
              </div>
</div>
                  
                </section><!-- /.content -->
        
