
           
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                     Dosen Ajar
                    </h1>
                           <ol class="breadcrumb">
                        <li><a href="<?=base_index();?>"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li><a href="<?=base_index();?>dosen-ajar">Dosen Ajar</a></li>
                        <li class="active">Tambah Dosen Ajar</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
<div class="row">
    <div class="col-lg-12"> 
        <div class="box box-solid box-primary">
                                 <div class="box-header">
                                    <h3 class="box-title">Tambah Dosen Ajar</h3>
                                    <div class="box-tools pull-right">
                                        <button class="btn btn-info btn-sm" data-widget="collapse"><i class="fa fa-minus"></i></button>
                                        <button class="btn btn-info btn-sm" data-widget="remove"><i class="fa fa-times"></i></button>
                                    </div>
                                </div>

                  <div class="box-body">
                     <form id="input_user" method="post" class="form-horizontal foto_banyak" action="<?=base_admin();?>modul/dosen_ajar/dosen_ajar_action.php?act=in">
 
<div class="form-group">
                        <label for="semester" class="control-label col-lg-2">Semester</label>
                        <div class="col-lg-10">

                          <select id="chosen_semester"  name="semesteran" data-placeholder="Pilih Semester..." class="form-control chzn-select" tabindex="2" >
               <option value=""></option>
               <?php foreach ($db->fetch_all("semester") as $isi) {
                    echo "<option value='$isi->semester'>$isi->semester  $isi->status</option>";
               } ?>
              </select>
                        </div>
                      </div><!-- /.form-group -->
<div class="form-group">
<label for="NIDN" class="control-label col-lg-2">NIDN</label>
                        <div class="col-lg-10">
                                <select name="nidn" id="nidn" onchange="myFunction()" data-placeholder="Pilih Dosen..." class="form-control chzn-select" tabindex="2" >
                                       <option value=""></option>
                                       <?php foreach ($db->fetch_all("m_dosen") as $isi) {
                                               echo "<option value='$isi->dosen_nidn.$isi->dosen_nama'  > $isi->dosen_nidn.$isi->dosen_nama</option>";                                            
                                       } ?>
                                 </select>
                        </div>
                      </div><!-- /.form-group -->

<div class="form-group">
                        <label for="Nama Dosen" class="control-label col-lg-2">Nama Dosen</label>
                        <div class="col-lg-10">
                          <input id="nama_dosen" type="text" name="nama_dosen" placeholder="Nama Dosen" class="form-control" required readonly> 
                        </div>
                      </div><!-- /.form-group -->


<div class="form-group">
                        <label for="Kode Matakuliah" class="control-label col-lg-2">Kode Matakuliah</label>
                        <div class="col-lg-10">
                          <select id="kodemk" onchange="myFunction()" name="kode_mk" data-placeholder="Pilih Mata Kuliah..." class="form-control chzn-select" tabindex="2" >
               <option value=""></option>
               
               <?php 
                    $query="select * from kelas_kuliah where kode_jurusan=$id_jur";
                    foreach ($db->fetch_custom($query) as $isi) {
                    echo "<option 
                                 kode_mk='$isi->kode_mk' 
                                 nama_kelas= '$isi->nama_kelas' 
                                 nama_mk='$isi->nama_mk' 
                                 value= '$isi->kode_mk.$isi->nama_mk'              > $isi->kode_mk.$isi->nama_mk</option>";
               } ?>
              </select>
                        </div>
                      </div><!-- /.form-group -->
<div class="form-group">
                        <label for="Nama Mata Kuliah" class="control-label col-lg-2">Nama Mata Kuliah</label>
                        <div class="col-lg-10">
                          <input id="namamk" type="text" name="nama_mk" placeholder="" class="form-control" required readonly> 
                        </div>
                      </div><!-- /.form-group -->
<div class="form-group">
                        <label for="Nama Kelas" class="control-label col-lg-2">Nama Kelas</label>
                        <div class="col-lg-10">
                          <input id="nama_kelas" type="text" name="nama_kelas" placeholder="contoh : 01" class="form-control" required readonly> 
                        </div>
                      </div><!-- /.form-group -->
<div class="form-group">
                        <label for="tatap_muka" class="control-label col-lg-2">Jumlah Tatap Muka</label>
                        <div class="col-lg-10">
                          <input type="text" data-rule-number="true" name="tatap_muka" placeholder="Jumlah Tatap Muka" class="form-control" required >  
                        </div>
                      </div><!-- /.form-group -->
<div class="form-group">
                        <label for="tatap_muka_real" class="control-label col-lg-2">Jumlah Realisasi Tatap Muka</label>
                        <div class="col-lg-10">
                          <input type="text" data-rule-number="true" name="tatap_muka_real" placeholder="Jumlah Realisasi Tatap Muka" class="form-control" required > 
                        </div>
                      </div><!-- /.form-group -->
                     <input type="hidden" name="jurusan" value="<?=$id_jur;?>"> 


<div class="alert alert-danger user_exist" style="display:none">   
    <button type="button" class="close" data-dismiss="alert">&times;</button>
    <strong>Maaf, entri data diatas sudah tersimpan dalam sistem, terjadi duplikasi data...!
    </strong> 
</div>

<!--
<div class="alert alert-danger user_exist" style="display:none">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    <strong>Danger!</strong> This alert box could indicate a dangerous or potentially negative action.
  </div>
-->
<div class="form-group">
                        <label for="tags" class="control-label col-lg-2">&nbsp;</label>
                        <div class="col-lg-10">
                          <input type="submit" class="btn btn-primary btn-flat" value="submit">
                        </div>
                      </div><!-- /.form-group -->



                    </form>
 <a onclick="window.history.back(-1)" class="btn btn-success btn-flat"><i class="fa fa-step-backward"></i> Kembali</a>
          
                  </div>
                  </div>
              </div>
</div>
                  
                </section><!-- /.content -->
        
<script type="text/javascript">
    $(document).ready(function(){
      $("#nidn").change(function(){        
        $("#nama_dosen").val($("#nidn").val());
      });
    });

  

    $(document).ready(function(){
      $("#kodemk").change(function(){  
          

        var a=$("#kodemk option:selected").attr("nama_mk");
        var b=$("#kodemk option:selected").attr("nama_kelas");
        var c=$("#kodemk option:selected").attr("semester");

        $("#namamk").val(a);
        $("#nama_kelas").val(b);
        $("#semester").val(c);
      });
    });

</script>  

