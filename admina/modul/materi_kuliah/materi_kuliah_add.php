
           
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                     Materi Kuliah
                    </h1>
                           <ol class="breadcrumb">
                        <li><a href="<?=base_index();?>"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li><a href="<?=base_index();?>materi-kuliah">Materi Kuliah</a></li>
                        <li class="active">Tambah Materi Kuliah</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
<div class="row">
    <div class="col-lg-12"> 
        <div class="box box-solid box-primary">
                                 <div class="box-header">
                                    <h3 class="box-title">Tambah Materi Kuliah</h3>
                                    <div class="box-tools pull-right">
                                        <button class="btn btn-info btn-sm" data-widget="collapse"><i class="fa fa-minus"></i></button>
                                        <button class="btn btn-info btn-sm" data-widget="remove"><i class="fa fa-times"></i></button>
                                    </div>
                                </div>

                  <div class="box-body">
                     <form id="input" method="post" class="form-horizontal foto_banyak" action="<?=base_admin();?>modul/materi_kuliah/materi_kuliah_action.php?act=in">
                      <div class="form-group">
                        <label for="kode_mk" class="control-label col-lg-2">kode_mk</label>
                        <div class="col-lg-10">
                          <select name="kode_mk" data-placeholder="Pilih kode_mk ..." class="form-control chzn-select" tabindex="2" required>
               <option value=""></option>
               <?php foreach ($db->fetch_all("mat_kurikulum") as $isi) {
                  echo "<option value='$isi->kode_mk'>$isi->nama_mk</option>";
               } ?>
              </select>
                        </div>
                      </div><!-- /.form-group -->
<div class="form-group">
                        <label for="nidn" class="control-label col-lg-2">nidn</label>
                        <div class="col-lg-10">
                          <select name="nidn" data-placeholder="Pilih nidn ..." class="form-control chzn-select" tabindex="2" required>
               <option value=""></option>
               <?php 
               
               $username=ucwords($db->fetch_single_row('sys_users','id',$_SESSION['id_user'])->username);
               $first_name=ucwords($db->fetch_single_row('sys_users','id',$_SESSION['id_user'])->first_name);
               $id_group=ucwords($db->fetch_single_row('sys_users','id',$_SESSION['id_user'])->id_group);
               if ($id_group == 1) {
                   foreach ($db->fetch_all("ajar_dosen") as $isi) {
                      echo "<option value='$isi->nidn'>$isi->nama_dosen</option>";
                   }
               }
               else
               {
                   echo "<option value='$username'>$first_name</option>";
               }
               ?>
              </select>
                        </div>
                      </div><!-- /.form-group -->
<div class="form-group">
                        <label for="materi_deksripsi" class="control-label col-lg-2">materi_deksripsi</label>
                        <div class="col-lg-10">
                          <textarea id="editbox" name="materi_deksripsi" class="editbox"></textarea>
                        </div>
                      </div><!-- /.form-group -->
<div class="form-group">
                        <label for="materi_file" class="control-label col-lg-2">materi_file</label>
                        <div class="col-lg-10">
                          <div class="fileinput fileinput-new" data-provides="fileinput">
                            <div class="input-group">
                              <div class="form-control uneditable-input span3" data-trigger="fileinput">
                                <i class="glyphicon glyphicon-file fileinput-exists"></i> <span class="fileinput-filename"></span> 
                              </div>
                              <span class="input-group-addon btn btn-default btn-file"><span class="fileinput-new">Select file</span> <span class="fileinput-exists">Change</span> 
                                <input type="file" name="materi_file" required>
                              </span> 
                              <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a> 
                            </div>
                          </div>
                        </div>
                      </div><!-- /.form-group -->
<div class="form-group">
                        <label for="materi_status" class="control-label col-lg-2">materi_status</label>
                        <div class="col-lg-10">
                          <input name="materi_status" class="make-switch" type="checkbox" checked>
                        </div>
                      </div><!-- /.form-group -->

                      
                      <div class="form-group">
                        <label for="tags" class="control-label col-lg-2">&nbsp;</label>
                        <div class="col-lg-10">
                          <input type="submit" class="btn btn-primary btn-flat" value="submit">
                        </div>
                      </div><!-- /.form-group -->
                    </form>
 <a href="<?=base_index();?>materi-kuliah" class="btn btn-success btn-flat"><i class="fa fa-step-backward"></i> Kembali</a>
          
                  </div>
                  </div>
              </div>
</div>
                  
                </section><!-- /.content -->
        
            