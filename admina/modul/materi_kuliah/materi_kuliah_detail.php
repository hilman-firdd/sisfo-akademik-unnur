

                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                     Materi Kuliah
                    </h1>
                   <ol class="breadcrumb">
                        <li><a href="<?=base_index();?>"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li><a href="<?=base_index();?>materi-kuliah">Materi Kuliah</a></li>
                        <li class="active">Detail Materi Kuliah</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
<div class="row">
    <div class="col-lg-12">
        <div class="box box-solid box-primary">
                                   <div class="box-header">
                                    <h3 class="box-title">Detail Materi Kuliah</h3>
                                    <div class="box-tools pull-right">
                                        <button class="btn btn-info btn-sm" data-widget="collapse"><i class="fa fa-minus"></i></button>
                                        <button class="btn btn-info btn-sm" data-widget="remove"><i class="fa fa-times"></i></button>
                                    </div>
                                </div>

                  <div class="box-body">
                   <form class="form-horizontal">
                      <div class="form-group">
                        <label for="kode_mk" class="control-label col-lg-2">kode_mk</label>
                        <div class="col-lg-10">
                          <?php foreach ($db->fetch_all("mat_kurikulum") as $isi) {
                  if ($data_edit->kode_mk==$isi->kode_mk) {

                    echo "<input disabled class='form-control' type='text' value='$isi->nama_mk'>";
                  }
               } ?>
              
                        </div>
                      </div><!-- /.form-group -->
<div class="form-group">
                        <label for="nidn" class="control-label col-lg-2">nidn</label>
                        <div class="col-lg-10">
                          <?php foreach ($db->fetch_all("ajar_dosen") as $isi) {
                  if ($data_edit->nidn==$isi->nidn) {

                    echo "<input disabled class='form-control' type='text' value='$isi->nama_dosen'>";
                  }
               } ?>
              
                        </div>
                      </div><!-- /.form-group -->
<div class="form-group">
                        <label for="materi_deksripsi" class="control-label col-lg-2">materi_deksripsi</label>
                        <div class="col-lg-10">
                          <textarea id="editbox" name="materi_deksripsi" disabled="" class="editbox"required><?=$data_edit->materi_deksripsi;?> </textarea>
                        </div>
                      </div><!-- /.form-group -->
<div class="form-group">
                        <label for="materi_file" class="control-label col-lg-2">materi_file</label>
                        <div class="col-lg-10">
                          <input type="text" disabled="" value="<?=$data_edit->materi_file;?>" class="form-control">
                        </div>
                      </div><!-- /.form-group -->
<div class="form-group">
                        <label for="materi_status" class="control-label col-lg-2">materi_status</label>
                        <div class="col-lg-10">
                          <?php if ($data_edit->materi_status=="Aktif") {
      ?>
      <input name="materi_status" class="make-switch" disabled type="checkbox" checked>
      <?php
    } else {
      ?>
      <input name="materi_status" class="make-switch" disabled type="checkbox">
      <?php
    }?>
                        </div>
                      </div><!-- /.form-group -->

                   
                    </form>
                    <a href="<?=base_index();?>materi-kuliah" class="btn btn-success btn-flat"><i class="fa fa-step-backward"></i> Kembali</a>
          
                  </div>
                  </div>
              </div>
</div>
                  
                </section><!-- /.content -->
        
