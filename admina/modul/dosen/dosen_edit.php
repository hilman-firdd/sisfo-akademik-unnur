

                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                      Dosen
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="<?=base_index();?>"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li><a href="<?=base_index();?>dosen">Dosen</a></li>
                        <li class="active">Edit Dosen</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
<div class="row">
    <div class="col-lg-12">
        <div class="box box-solid box-primary">
                                   <div class="box-header">
                                    <h3 class="box-title">Edit Dosen</h3>
                                    <div class="box-tools pull-right">
                                        <button class="btn btn-info btn-sm" data-widget="collapse"><i class="fa fa-minus"></i></button>
                                        <button class="btn btn-info btn-sm" data-widget="remove"><i class="fa fa-times"></i></button>
                                    </div>
                                </div>

                  <div class="box-body">
                     <form id="update" method="post" class="form-horizontal" action="<?=base_admin();?>modul/dosen/dosen_action.php?act=up">
                      <div class="form-group">
                        <label for="NIDN/NIDK/Kode Dosen" class="control-label col-lg-2">NIDN/NIDK/Kode Dosen</label>
                        <div class="col-lg-10">
                          <input type="text" name="dosen_nidn" value="<?=$data_edit->dosen_nidn;?>" class="form-control" required> 
                        </div>
                      </div><!-- /.form-group -->
<div class="form-group">
                        <label for="Nama" class="control-label col-lg-2">Nama</label>
                        <div class="col-lg-10">
                          <input type="text" name="dosen_nama" value="<?=$data_edit->dosen_nama;?>" class="form-control" required> 
                        </div>
                      </div><!-- /.form-group -->
<div class="form-group">
                        <label for="No. HP" class="control-label col-lg-2">No. HP</label>
                        <div class="col-lg-10">
                          <input type="text" name="dpsen_nohp" value="<?=$data_edit->dpsen_nohp;?>" class="form-control" > 
                        </div>
                      </div><!-- /.form-group -->
<div class="form-group">
                        <label for="Alamat" class="control-label col-lg-2">Alamat</label>
                        <div class="col-lg-10">
                          <input type="text" name="dosen_alamat" value="<?=$data_edit->dosen_alamat;?>" class="form-control" required> 
                        </div>
                      </div><!-- /.form-group -->
<div class="form-group">
                        <label for="Gelar Depan" class="control-label col-lg-2">Gelar Depan</label>
                        <div class="col-lg-10">
                          <input type="text" name="dosen_gelardepan" value="<?=$data_edit->dosen_gelardepan;?>" class="form-control" > 
                        </div>
                      </div><!-- /.form-group -->
<div class="form-group">
                        <label for="Gelar Belakang" class="control-label col-lg-2">Gelar Belakang</label>
                        <div class="col-lg-10">
                          <input type="text" name="dosen_gelarblkg" value="<?=$data_edit->dosen_gelarblkg;?>" class="form-control" > 
                        </div>
                      </div><!-- /.form-group -->
<div class="form-group">
                        <label for="Status Dosen" class="control-label col-lg-2">Status Dosen</label>
                        <div class="col-lg-10">
                          <select name="dosen_status_id" data-placeholder="Pilih Status Dosen..." class="form-control chzn-select" tabindex="2" required>
               <option value=""></option>
               <?php foreach ($db->fetch_all("m_status_dosen") as $isi) {

                  if ($data_edit->dosen_status_id==$isi->status_dosen_id) {
                    echo "<option value='$isi->status_dosen_id' selected>$isi->status_dosen_deskripsi</option>";
                  } else {
                  echo "<option value='$isi->status_dosen_id'>$isi->status_dosen_deskripsi</option>";
                    }
               } ?>
              </select>
                        </div>
                      </div><!-- /.form-group -->
<div class="form-group">
                        <label for="Jenis Kelamin" class="control-label col-lg-2">Jenis Kelamin</label>
                        <div class="col-lg-10">
                          <select name="dosen_gender_id" data-placeholder="Pilih Jenis Kelamin..." class="form-control chzn-select" tabindex="2" required>
               <option value=""></option>
               <?php foreach ($db->fetch_all("m_gender") as $isi) {

                  if ($data_edit->dosen_gender_id==$isi->gender_id) {
                    echo "<option value='$isi->gender_id' selected>$isi->gender_deskripsi</option>";
                  } else {
                  echo "<option value='$isi->gender_id'>$isi->gender_deskripsi</option>";
                    }
               } ?>
              </select>
                        </div>
                      </div><!-- /.form-group -->
<div class="form-group">
                        <label for="Jabatan Fungsional" class="control-label col-lg-2">Jabatan Fungsional</label>
                        <div class="col-lg-10">
                          <select name="dosen_jabfung_id" data-placeholder="Pilih Jabatan Fungsional..." class="form-control chzn-select" tabindex="2" >
               <option value=""></option>
               <?php foreach ($db->fetch_all("m_jab_fungsional") as $isi) {

                  if ($data_edit->dosen_jabfung_id==$isi->jab_fung_id) {
                    echo "<option value='$isi->jab_fung_id' selected>$isi->jab_fung_deskripsi</option>";
                  } else {
                  echo "<option value='$isi->jab_fung_id'>$isi->jab_fung_deskripsi</option>";
                    }
               } ?>
              </select>
                        </div>
                      </div><!-- /.form-group -->
<div class="form-group">
                        <label for="Pendidikan" class="control-label col-lg-2">Pendidikan</label>
                        <div class="col-lg-10">
                          <select name="dosen_pendidikan_id" data-placeholder="Pilih Pendidikan..." class="form-control chzn-select" tabindex="2" required>
               <option value=""></option>
               <?php foreach ($db->fetch_all("jenjang_pendidikan") as $isi) {

                  if ($data_edit->dosen_pendidikan_id==$isi->id_jenj_didik) {
                    echo "<option value='$isi->id_jenj_didik' selected>$isi->nm_jenj_didik</option>";
                  } else {
                  echo "<option value='$isi->id_jenj_didik'>$isi->nm_jenj_didik</option>";
                    }
               } ?>
              </select>
                        </div>
                      </div><!-- /.form-group -->

                      <input type="hidden" name="id" value="<?=$data_edit->dosen_id;?>">
                      <div class="form-group">
                        <label for="tags" class="control-label col-lg-2">&nbsp;</label>
                        <div class="col-lg-10">
                          <input type="submit" class="btn btn-primary btn-flat" value="submit">
                        </div>
                      </div><!-- /.form-group -->
                    </form>
                    <a href="<?=base_index();?>dosen" class="btn btn-success btn-flat"><i class="fa fa-step-backward"></i> Kembali</a>
          
                  </div>
                  </div>
              </div>
</div>
                  
                </section><!-- /.content -->
        
 