
           
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                     Kelas Kuliah
                    </h1>
                           <ol class="breadcrumb">
                        <li><a href="<?=base_index();?>"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li><a href="<?=base_index();?>kelas-kuliah">Kelas Kuliah</a></li>
                        <li class="active">Tambah Kelas Kuliah</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
<div class="row">
    <div class="col-lg-12"> 
        <div class="box box-solid box-primary">
                                 <div class="box-header">
                                    <h3 class="box-title">Tambah Kelas Kuliah</h3>
                                    <div class="box-tools pull-right">
                                        <button class="btn btn-info btn-sm" data-widget="collapse"><i class="fa fa-minus"></i></button>
                                        <button class="btn btn-info btn-sm" data-widget="remove"><i class="fa fa-times"></i></button>
                                    </div>
                                </div>

                  <div class="box-body">
                     <form id="input" method="post" class="form-horizontal foto_banyak" action="<?=base_admin();?>modul/kelas_kuliah/kelas_kuliah_action.php?act=in">
<div class="form-group">
                        <label for="Semester" class="control-label col-lg-2">Semester</label>
                        <div class="col-lg-10">
                          <select name="semester" data-placeholder="Pilih Semester..." class="form-control chzn-select" tabindex="2" >
               <option value=""></option>
               <?php foreach ($db->fetch_all("semester") as $isi) {
                    echo "<option value='$isi->semester'>$isi->semester</option>";
               } ?>
              </select>
                        </div>
                      </div><!-- /.form-group -->
<div class="form-group">
                        <label for="Kode Matakuliah" class="control-label col-lg-2">Kode Matakuliah</label>
                        <div class="col-lg-10">
                          <select id="kodemk" onchange="myFunction()" name="kode_mk" data-placeholder="Pilih Mata Kuliah..." class="form-control chzn-select" tabindex="2" >
               <option value=""></option>
               <?php foreach ($db->fetch_all("mat_kurikulum") as $isi) {
                    echo "<option kode_mk='$isi->nama_mk' nama_mk='$isi->nama_mk'>$isi->kode_mk.$isi->nama_mk</option>";
               } ?>
              </select>
                        </div>
                      </div><!-- /.form-group -->
<div class="form-group">
                        <label for="Nama Matakuliah" class="control-label col-lg-2">Nama Matakuliah</label>
                        <div class="col-lg-10">
                          <input id="namamk" type="text" name="nama_mk" placeholder="Nama Matakuliah" class="form-control" required readonly> 
                        </div>
                      </div><!-- /.form-group -->
<div class="form-group">
                        <label for="Kelas" class="control-label col-lg-2">Kelas</label>
                        <div class="col-lg-10">
                          <select name="nama_kelas" data-placeholder="Pilih Kelas..." class="form-control chzn-select" tabindex="2" >
               <option value=""></option>
               <?php foreach ($db->fetch_all("m_ruang") as $isi) {
                    echo "<option value='$isi->ruang_deskripsi'>$isi->ruang_deskripsi</option>";
               } ?>
              </select>
                        </div>
                      </div><!-- /.form-group -->

                      <input type="hidden" name="jurusan" value="<?=$id_jur;?>">
                      <div class="form-group">
                        <label for="tags" class="control-label col-lg-2">&nbsp;</label>
                        <div class="col-lg-10">
                          <input type="submit" class="btn btn-primary btn-flat" value="submit">
                        </div>
                      </div><!-- /.form-group -->
                    </form>
 <a onclick="window.history.back(-1)" class="btn btn-success btn-flat"><i class="fa fa-step-backward"></i> Kembali</a>
          
                  </div>
                  </div>
              </div>
</div>
                  
                </section><!-- /.content -->
        

<script type="text/javascript">
    $(document).ready(function(){
      $("#kodemk").change(function(){        
        $("#namamk").val($("#kodemk option:selected").attr("nama_mk"));
      });
    });
</script>  