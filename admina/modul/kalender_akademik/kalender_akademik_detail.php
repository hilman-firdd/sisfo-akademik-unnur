

                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                     Kalender Akademik
                    </h1>
                   <ol class="breadcrumb">
                        <li><a href="<?=base_index();?>"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li><a href="<?=base_index();?>kalender-akademik">Kalender Akademik</a></li>
                        <li class="active">Detail Kalender Akademik</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
<div class="row">
    <div class="col-lg-12">
        <div class="box box-solid box-primary">
                                   <div class="box-header">
                                    <h3 class="box-title">Detail Kalender Akademik</h3>
                                    <div class="box-tools pull-right">
                                        <button class="btn btn-info btn-sm" data-widget="collapse"><i class="fa fa-minus"></i></button>
                                        <button class="btn btn-info btn-sm" data-widget="remove"><i class="fa fa-times"></i></button>
                                    </div>
                                </div>

                  <div class="box-body">
                   <form class="form-horizontal">
                      <div class="form-group">
                        <label for="Kategori Kegiatan" class="control-label col-lg-2">Kategori Kegiatan</label>
                        <div class="col-lg-10">
                          <?php foreach ($db->fetch_all("m_kategori_kegiatan") as $isi) {
                  if ($data_edit->m_kat_kegiatan_id==$isi->m_kat_kegiatan_id) {

                    echo "<input disabled class='form-control' type='text' value='$isi->m_kat_kegiatan_deskripsi'>";
                  }
               } ?>
              
                        </div>
                      </div><!-- /.form-group -->
<div class="form-group">
                        <label for="Nama Kegiatan" class="control-label col-lg-2">Nama Kegiatan</label>
                        <div class="col-lg-10">
                          <?php foreach ($db->fetch_all("m_kegiatan") as $isi) {
                  if ($data_edit->m_kegiatan_id==$isi->m_kegiatan_id) {

                    echo "<input disabled class='form-control' type='text' value='$isi->m_kegiatan_nama'>";
                  }
               } ?>
              
                        </div>
                      </div><!-- /.form-group -->
<div class="form-group">
                        <label for="Tahun Kegiatan" class="control-label col-lg-2">Tahun Kegiatan</label>
                        <div class="col-lg-10">
                          <input type="text" disabled="" value="<?=$data_edit->t_kegiatan_tahun;?>" class="form-control">
                        </div>
                      </div><!-- /.form-group -->
<div class="form-group">
                        <label for="Deskripsi Kegiatan" class="control-label col-lg-2">Deskripsi Kegiatan</label>
                        <div class="col-lg-10">
                          <textarea id="editbox" name="t_kegiatan_deskripsi" disabled="" class="editbox"><?=$data_edit->t_kegiatan_deskripsi;?> </textarea>
                        </div>
                      </div><!-- /.form-group -->
<div class="form-group">
                        <label for="Tanggal Awal Kegiatan" class="control-label col-lg-2">Tanggal Awal Kegiatan</label>
                        <div class="col-lg-10">
                          <input type="text" disabled="" value="<?=tgl_indo($data_edit->t_kegiatan_tgl_awal);?>" class="form-control">
                        </div>
                      </div><!-- /.form-group -->
<div class="form-group">
                        <label for="Tanggal Akhir Kegiatan" class="control-label col-lg-2">Tanggal Akhir Kegiatan</label>
                        <div class="col-lg-10">
                          <input type="text" disabled="" value="<?=tgl_indo($data_edit->t_kegiatan_tgl_akhir);?>" class="form-control">
                        </div>
                      </div><!-- /.form-group -->
<div class="form-group">
                        <label for="Status Kegiatan" class="control-label col-lg-2">Status Kegiatan</label>
                        <div class="col-lg-10">
                          <?php if ($data_edit->t_kegiatan_status=="Aktif") {
      ?>
      <input name="t_kegiatan_status" class="make-switch" disabled type="checkbox" checked>
      <?php
    } else {
      ?>
      <input name="t_kegiatan_status" class="make-switch" disabled type="checkbox">
      <?php
    }?>
                        </div>
                      </div><!-- /.form-group -->
<div class="form-group">
                        <label for="Terhubung dengan Menu" class="control-label col-lg-2">Terhubung dengan Menu</label>
                        <div class="col-lg-10">
                          <?php foreach ($db->fetch_all("sys_menu") as $isi) {
                  if ($data_edit->t_m_kegiatan_menu==$isi->url) {

                    echo "<input disabled class='form-control' type='text' value='$isi->url'>";
                  }
               } ?>
              
                        </div>
                      </div><!-- /.form-group -->

                   
                    </form>
                    <a href="<?=base_index();?>kalender-akademik" class="btn btn-success btn-flat"><i class="fa fa-step-backward"></i> Kembali</a>
          
                  </div>
                  </div>
              </div>
</div>
                  
                </section><!-- /.content -->
        
