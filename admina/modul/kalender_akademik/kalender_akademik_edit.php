

                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                      Kalender Akademik
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="<?=base_index();?>"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li><a href="<?=base_index();?>kalender-akademik">Kalender Akademik</a></li>
                        <li class="active">Edit Kalender Akademik</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
<div class="row">
    <div class="col-lg-12">
        <div class="box box-solid box-primary">
                                   <div class="box-header">
                                    <h3 class="box-title">Edit Kalender Akademik</h3>
                                    <div class="box-tools pull-right">
                                        <button class="btn btn-info btn-sm" data-widget="collapse"><i class="fa fa-minus"></i></button>
                                        <button class="btn btn-info btn-sm" data-widget="remove"><i class="fa fa-times"></i></button>
                                    </div>
                                </div>

                  <div class="box-body">
                     <form id="update" method="post" class="form-horizontal" action="<?=base_admin();?>modul/kalender_akademik/kalender_akademik_action.php?act=up">
                      <div class="form-group">
                        <label for="Kategori Kegiatan" class="control-label col-lg-2">Kategori Kegiatan</label>
                        <div class="col-lg-10">
                          <select name="m_kat_kegiatan_id" data-placeholder="Pilih Kategori Kegiatan..." class="form-control chzn-select" tabindex="2" required>
               <option value=""></option>
               <?php foreach ($db->fetch_all("m_kategori_kegiatan") as $isi) {

                  if ($data_edit->m_kat_kegiatan_id==$isi->m_kat_kegiatan_id) {
                    echo "<option value='$isi->m_kat_kegiatan_id' selected>$isi->m_kat_kegiatan_deskripsi</option>";
                  } else {
                  echo "<option value='$isi->m_kat_kegiatan_id'>$isi->m_kat_kegiatan_deskripsi</option>";
                    }
               } ?>
              </select>
                        </div>
                      </div><!-- /.form-group -->
<div class="form-group">
                        <label for="Nama Kegiatan" class="control-label col-lg-2">Nama Kegiatan</label>
                        <div class="col-lg-10">
                          <select name="m_kegiatan_id" data-placeholder="Pilih Nama Kegiatan..." class="form-control chzn-select" tabindex="2" required>
               <option value=""></option>
               <?php foreach ($db->fetch_all("m_kegiatan") as $isi) {

                  if ($data_edit->m_kegiatan_id==$isi->m_kegiatan_id) {
                    echo "<option value='$isi->m_kegiatan_id' selected>$isi->m_kegiatan_nama</option>";
                  } else {
                  echo "<option value='$isi->m_kegiatan_id'>$isi->m_kegiatan_nama</option>";
                    }
               } ?>
              </select>
                        </div>
                      </div><!-- /.form-group -->
<div class="form-group">
                        <label for="Tahun Kegiatan" class="control-label col-lg-2">Tahun Kegiatan</label>
                        <div class="col-lg-10">
                          <input type="text" data-rule-number="true" name="t_kegiatan_tahun" value="<?=$data_edit->t_kegiatan_tahun;?>" class="form-control" required> 
                        </div>
                      </div><!-- /.form-group -->
<div class="form-group">
                        <label for="Deskripsi Kegiatan" class="control-label col-lg-2">Deskripsi Kegiatan</label>
                        <div class="col-lg-10">
                          <textarea id="editbox" name="t_kegiatan_deskripsi" class="editbox"><?=$data_edit->t_kegiatan_deskripsi;?> </textarea>
                        </div>
                      </div><!-- /.form-group -->
<div class="form-group">
                        <label for="Tanggal Awal Kegiatan" class="control-label col-lg-2">Tanggal Awal Kegiatan</label>
                        <div class="col-lg-10">
                          <input type="text" id="tgl1" data-rule-date="true" name="t_kegiatan_tgl_awal" value="<?=$data_edit->t_kegiatan_tgl_awal;?>" class="form-control" required> 
                        </div>
                      </div><!-- /.form-group -->
<div class="form-group">
                        <label for="Tanggal Akhir Kegiatan" class="control-label col-lg-2">Tanggal Akhir Kegiatan</label>
                        <div class="col-lg-10">
                          <input type="text" id="tgl2" data-rule-date="true" name="t_kegiatan_tgl_akhir" value="<?=$data_edit->t_kegiatan_tgl_akhir;?>" class="form-control" required> 
                        </div>
                      </div><!-- /.form-group -->
<div class="form-group">
                        <label for="Status Kegiatan" class="control-label col-lg-2">Status Kegiatan</label>
                        <div class="col-lg-10">
                          <?php if ($data_edit->t_kegiatan_status=="Aktif") {
      ?>
      <input name="t_kegiatan_status" class="make-switch" type="checkbox" checked>
      <?php
    } else {
      ?>
      <input name="t_kegiatan_status" class="make-switch" type="checkbox">
      <?php
    }?>
                        </div>
                      </div><!-- /.form-group -->
<div class="form-group">
                        <label for="Terhubung dengan Menu" class="control-label col-lg-2">Terhubung dengan Menu</label>
                        <div class="col-lg-10">
                          <select name="t_m_kegiatan_menu" data-placeholder="Pilih Terhubung dengan Menu..." class="form-control chzn-select" tabindex="2" required>
               <option value=""></option>
               <?php foreach ($db->fetch_all("sys_menu") as $isi) {

                  if ($data_edit->t_m_kegiatan_menu==$isi->url) {
                    echo "<option value='$isi->url' selected>$isi->url</option>";
                  } else {
                  echo "<option value='$isi->url'>$isi->url</option>";
                    }
               } ?>
              </select>
                        </div>
                      </div><!-- /.form-group -->

                      <input type="hidden" name="id" value="<?=$data_edit->t_kegiatan_id;?>">
                      <div class="form-group">
                        <label for="tags" class="control-label col-lg-2">&nbsp;</label>
                        <div class="col-lg-10">
                          <input type="submit" class="btn btn-primary btn-flat" value="submit">
                        </div>
                      </div><!-- /.form-group -->
                    </form>
                    <a href="<?=base_index();?>kalender-akademik" class="btn btn-success btn-flat"><i class="fa fa-step-backward"></i> Kembali</a>
          
                  </div>
                  </div>
              </div>
</div>
                  
                </section><!-- /.content -->
        
 