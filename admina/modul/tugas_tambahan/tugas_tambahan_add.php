
           
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                     Tugas Tambahan
                    </h1>
                           <ol class="breadcrumb">
                        <li><a href="<?=base_index();?>"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li><a href="<?=base_index();?>tugas-tambahan">Tugas Tambahan</a></li>
                        <li class="active">Tambah Tugas Tambahan</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
<div class="row">
    <div class="col-lg-12"> 
        <div class="box box-solid box-primary">
                                 <div class="box-header">
                                    <h3 class="box-title">Tambah Tugas Tambahan</h3>
                                    <div class="box-tools pull-right">
                                        <button class="btn btn-info btn-sm" data-widget="collapse"><i class="fa fa-minus"></i></button>
                                        <button class="btn btn-info btn-sm" data-widget="remove"><i class="fa fa-times"></i></button>
                                    </div>
                                </div>

                  <div class="box-body">
                     <form id="input" method="post" class="form-horizontal foto_banyak" action="<?=base_admin();?>modul/tugas_tambahan/tugas_tambahan_action.php?act=in">
                      <div class="form-group">
                        <label for="Deskripsi Tugas Tambahan" class="control-label col-lg-2">Deskripsi Tugas Tambahan</label>
                        <div class="col-lg-10">
                          <input type="text" name="tgs_tambahan_deskripsi" placeholder="Deskripsi Tugas Tambahan" class="form-control" required> 
                        </div>
                      </div><!-- /.form-group -->
<div class="form-group">
                        <label for="Wewenang Tugas Tambahan" class="control-label col-lg-2">Wewenang Tugas Tambahan</label>
                        <div class="col-lg-10">
                          <textarea id="editbox" name="tgs_tambahan_wewenang" class="editbox"></textarea>
                        </div>
                      </div><!-- /.form-group -->

                      
                      <div class="form-group">
                        <label for="tags" class="control-label col-lg-2">&nbsp;</label>
                        <div class="col-lg-10">
                          <input type="submit" class="btn btn-primary btn-flat" value="submit">
                        </div>
                      </div><!-- /.form-group -->
                    </form>
 <a href="<?=base_index();?>tugas-tambahan" class="btn btn-success btn-flat"><i class="fa fa-step-backward"></i> Kembali</a>
          
                  </div>
                  </div>
              </div>
</div>
                  
                </section><!-- /.content -->
        
            