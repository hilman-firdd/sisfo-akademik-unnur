

                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                     Tugas Tambahan
                    </h1>
                   <ol class="breadcrumb">
                        <li><a href="<?=base_index();?>"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li><a href="<?=base_index();?>tugas-tambahan">Tugas Tambahan</a></li>
                        <li class="active">Detail Tugas Tambahan</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
<div class="row">
    <div class="col-lg-12">
        <div class="box box-solid box-primary">
                                   <div class="box-header">
                                    <h3 class="box-title">Detail Tugas Tambahan</h3>
                                    <div class="box-tools pull-right">
                                        <button class="btn btn-info btn-sm" data-widget="collapse"><i class="fa fa-minus"></i></button>
                                        <button class="btn btn-info btn-sm" data-widget="remove"><i class="fa fa-times"></i></button>
                                    </div>
                                </div>

                  <div class="box-body">
                   <form class="form-horizontal">
                      <div class="form-group">
                        <label for="Deskripsi Tugas Tambahan" class="control-label col-lg-2">Deskripsi Tugas Tambahan</label>
                        <div class="col-lg-10">
                          <input type="text" disabled="" value="<?=$data_edit->tgs_tambahan_deskripsi;?>" class="form-control">
                        </div>
                      </div><!-- /.form-group -->
<div class="form-group">
                        <label for="Wewenang Tugas Tambahan" class="control-label col-lg-2">Wewenang Tugas Tambahan</label>
                        <div class="col-lg-10">
                          <textarea id="editbox" name="tgs_tambahan_wewenang" disabled="" class="editbox"required><?=$data_edit->tgs_tambahan_wewenang;?> </textarea>
                        </div>
                      </div><!-- /.form-group -->

                   
                    </form>
                    <a href="<?=base_index();?>tugas-tambahan" class="btn btn-success btn-flat"><i class="fa fa-step-backward"></i> Kembali</a>
          
                  </div>
                  </div>
              </div>
</div>
                  
                </section><!-- /.content -->
        
