

                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                     Cek SPP
                    </h1>
                   <ol class="breadcrumb">
                        <li><a href="<?=base_index();?>"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li><a href="<?=base_index();?>cek-spp">Cek SPP</a></li>
                        <li class="active">Detail Cek SPP</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
<div class="row">
    <div class="col-lg-12">
        <div class="box box-solid box-primary">
                                   <div class="box-header">
                                    <h3 class="box-title">Detail Cek SPP</h3>
                                    <div class="box-tools pull-right">
                                        <button class="btn btn-info btn-sm" data-widget="collapse"><i class="fa fa-minus"></i></button>
                                        <button class="btn btn-info btn-sm" data-widget="remove"><i class="fa fa-times"></i></button>
                                    </div>
                                </div>

                  <div class="box-body">
                   <form class="form-horizontal">
                      <div class="form-group">
                        <label for="Semester" class="control-label col-lg-2">Semester</label>
                        <div class="col-lg-10">
                          <?php foreach ($db->fetch_all("semester") as $isi) {
                  if ($data_edit->cek_spp_kode_semester==$isi->semester) {

                    echo "<input disabled class='form-control' type='text' value='$isi->semester'>";
                  }
               } ?>
              
                        </div>
                      </div><!-- /.form-group -->
<div class="form-group">
                        <label for="Mahasiswa" class="control-label col-lg-2">Mahasiswa</label>
                        <div class="col-lg-10">
                          <?php foreach ($db->fetch_all("mhs") as $isi) {
                  if ($data_edit->cek_spp_nim==$isi->nipd) {

                    echo "<input disabled class='form-control' type='text' value='$isi->nm_pd'>";
                  }
               } ?>
              
                        </div>
                      </div><!-- /.form-group -->
<div class="form-group">
                        <label for="Tanggal" class="control-label col-lg-2">Tanggal</label>
                        <div class="col-lg-10">
                          <input type="text" disabled="" value="<?=tgl_indo($data_edit->cek_spp_tgl);?>" class="form-control">
                        </div>
                      </div><!-- /.form-group -->
<div class="form-group">
                        <label for="Operator SPP" class="control-label col-lg-2">Operator SPP</label>
                        <div class="col-lg-10">
                          <input type="text" disabled="" value="<?=$data_edit->cek_spp_operator;?>" class="form-control">
                        </div>
                      </div><!-- /.form-group -->
<div class="form-group">
                        <label for="Status SPP" class="control-label col-lg-2">Status SPP</label>
                        <div class="col-lg-10">
                          <?php foreach ($db->fetch_all("m_status_spp") as $isi) {
                  if ($data_edit->cek_spp_status==$isi->status_spp_id) {

                    echo "<input disabled class='form-control' type='text' value='$isi->status_spp_nama'>";
                  }
               } ?>
              
                        </div>
                      </div><!-- /.form-group -->

                   
                    </form>
                    <a href="<?=base_index();?>cek-spp" class="btn btn-success btn-flat"><i class="fa fa-step-backward"></i> Kembali</a>
          
                  </div>
                  </div>
              </div>
</div>
                  
                </section><!-- /.content -->
        
