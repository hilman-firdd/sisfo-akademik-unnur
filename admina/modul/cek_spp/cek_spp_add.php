
           
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                     Cek SPP
                    </h1>
                           <ol class="breadcrumb">
                        <li><a href="<?=base_index();?>"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li><a href="<?=base_index();?>cek-spp">Cek SPP</a></li>
                        <li class="active">Tambah Cek SPP</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
<div class="row">
    <div class="col-lg-12"> 
        <div class="box box-solid box-primary">
                                 <div class="box-header">
                                    <h3 class="box-title">Tambah Cek SPP</h3>
                                    <div class="box-tools pull-right">
                                        <button class="btn btn-info btn-sm" data-widget="collapse"><i class="fa fa-minus"></i></button>
                                        <button class="btn btn-info btn-sm" data-widget="remove"><i class="fa fa-times"></i></button>
                                    </div>
                                </div>

                  <div class="box-body">
                     <form id="input_user" method="post" class="form-horizontal foto_banyak" action="<?=base_admin();?>modul/cek_spp/cek_spp_action.php?act=in">
                      <div class="form-group">
                        <label for="Semester" class="control-label col-lg-2">Semester</label>
                        <div class="col-lg-10">
                          <select name="cek_spp_kode_semester" data-placeholder="Pilih Semester ..." class="form-control chzn-select" tabindex="2" required>
               <option value=""></option>
               <?php foreach ($db->fetch_all("semester") as $isi) {
                  echo "<option value='$isi->semester'>$isi->semester    $isi->status</option>";
               } ?>
              </select>
                        </div>
                      </div><!-- /.form-group -->

<?php
    $kode_jurusan=ucwords($db->fetch_single_row('sys_users','id',$_SESSION['id_user'])->kode_jurusan);  
    $id_operator=ucwords($db->fetch_single_row('sys_users','id',$_SESSION['id_user'])->id);  

    if ($id_operator<> 1){$query_mhs="select * from mhs where kode_jurusan=$kode_jurusan";}
    else {$query_mhs="select * from mhs ";}
    
?>                  
<div class="form-group">
                        <label for="Mahasiswa" class="control-label col-lg-2">Mahasiswa</label>
                        <div class="col-lg-10">
                          <select name="cek_spp_nim" data-placeholder="Pilih Mahasiswa ..." class="form-control chzn-select" tabindex="2" required>
               <option value=""></option>

               <?php 
               $query="select * from kelas_kuliah where kode_jurusan=$id_jur";
               foreach ($db->fetch_custom($query_mhs) as $isi) {
                  echo "<option value='$isi->nipd'>$isi->nm_pd</option>";
               } ?>
              </select>
                        </div>
                      </div><!-- /.form-group -->
<div class="form-group">
                        <label for="Tanggal" class="control-label col-lg-2">Tanggal</label>
                        <div class="col-lg-10">
                          <input type="text" id="tgl1" data-rule-date="true"  value="<?=date("Y-m-d H:i:s")?> "name="cek_spp_tgl" placeholder="Tanggal" class="form-control" required readonly> 
                        </div>
                      </div><!-- /.form-group -->

<?php
    $nama_operator=ucwords($db->fetch_single_row('sys_users','id',$_SESSION['id_user'])->username);  
?>                      
<div class="form-group">
                        <label for="Operator SPP" class="control-label col-lg-2">Operator SPP</label>
                        <div class="col-lg-10">
                          <input type="text" name="cek_spp_operator" value="<?=$nama_operator ?>" placeholder="Operator SPP" class="form-control" required readonly>                           
                          <input type="hidden" name="cek_spp_operator_hidden" value="<?=$id_operator ?>" >
                        </div>
                      </div><!-- /.form-group -->
<div class="form-group">
                        <label for="Status SPP" class="control-label col-lg-2">Status SPP</label>
                        <div class="col-lg-10">
                          <select name="cek_spp_status" data-placeholder="Pilih Status SPP ..." class="form-control chzn-select" tabindex="2" required>
               <option value=""></option>
               <?php foreach ($db->fetch_all("m_status_spp") as $isi) {
                  echo "<option value='$isi->status_spp_id'>$isi->status_spp_nama</option>";
               } ?>
              </select>
                        </div>
                      </div><!-- /.form-group -->

<div class="alert alert-danger user_exist" style="display:none">   
    <button type="button" class="close" data-dismiss="alert">&times;</button>
    <strong>Perhatian:, Mahasiswa ini sudah membayar SPP....
    </strong> 
</div>                      

  <div class="form-group">
                        <label for="tags" class="control-label col-lg-2">&nbsp;</label>
                        <div class="col-lg-10">
                          <input type="submit" class="btn btn-primary btn-flat" value="submit">
                        </div>  
                      </div><!-- /.form-group -->
                    
                    </form>
 <a href="<?=base_index();?>cek-spp" class="btn btn-success btn-flat"><i class="fa fa-step-backward"></i> Kembali</a>
          
                  </div>
                  </div>
              </div>
</div>
                  
                </section><!-- /.content -->
        
            