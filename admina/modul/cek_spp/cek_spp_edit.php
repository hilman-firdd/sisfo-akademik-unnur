

                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                      Cek SPP
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="<?=base_index();?>"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li><a href="<?=base_index();?>cek-spp">Cek SPP</a></li>
                        <li class="active">Edit Cek SPP</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
<div class="row">
    <div class="col-lg-12">
        <div class="box box-solid box-primary">
                                   <div class="box-header">
                                    <h3 class="box-title">Edit Cek SPP</h3>
                                    <div class="box-tools pull-right">
                                        <button class="btn btn-info btn-sm" data-widget="collapse"><i class="fa fa-minus"></i></button>
                                        <button class="btn btn-info btn-sm" data-widget="remove"><i class="fa fa-times"></i></button>
                                    </div>
                                </div>

                  <div class="box-body">
                     <form id="update" method="post" class="form-horizontal" action="<?=base_admin();?>modul/cek_spp/cek_spp_action.php?act=up">
                      <div class="form-group">
                        <label for="Semester" class="control-label col-lg-2">Semester</label>
                        <div class="col-lg-10">
                          <select name="cek_spp_kode_semester" data-placeholder="Pilih Semester..." class="form-control chzn-select" tabindex="2" required>
               <option value=""></option>
               <?php foreach ($db->fetch_all("semester") as $isi) {

                  if ($data_edit->cek_spp_kode_semester==$isi->semester) {
                    echo "<option value='$isi->semester' selected>$isi->semester</option>";
                  } else {
                  echo "<option value='$isi->semester'>$isi->semester</option>";
                    }
               } ?>
              </select>
                        </div>
                      </div><!-- /.form-group -->
<div class="form-group">
                        <label for="Mahasiswa" class="control-label col-lg-2">Mahasiswa</label>
                        <div class="col-lg-10">
                          <select name="cek_spp_nim" data-placeholder="Pilih Mahasiswa..." class="form-control chzn-select" tabindex="2" required>
               <option value=""></option>
               <?php foreach ($db->fetch_all("mhs") as $isi) {

                  if ($data_edit->cek_spp_nim==$isi->nipd) {
                    echo "<option value='$isi->nipd' selected>$isi->nm_pd</option>";
                  } else {
                  echo "<option value='$isi->nipd'>$isi->nm_pd</option>";
                    }
               } ?>
              </select>
                        </div>
                      </div><!-- /.form-group -->
<div class="form-group">
                        <label for="Tanggal" class="control-label col-lg-2">Tanggal</label>
                        <div class="col-lg-10">
                          <input type="text" id="tgl1" data-rule-date="true" name="cek_spp_tgl" value="<?=$data_edit->cek_spp_tgl;?>" class="form-control" required> 
                        </div>
                      </div><!-- /.form-group -->
<?php
    $nama_operator=ucwords($db->fetch_single_row('sys_users','id',$data_edit->cek_spp_operator)->username);  
?>   
<div class="form-group">
                        <label for="Operator SPP" class="control-label col-lg-2">Operator SPP</label>
                        <div class="col-lg-10">
                          <input type="text" name="cek_spp_operator" value="<?=$nama_operator?>" class="form-control" required readonly> 
                        </div>
                      </div><!-- /.form-group -->
<div class="form-group">
                        <label for="Status SPP" class="control-label col-lg-2">Status SPP</label>
                        <div class="col-lg-10">
                          <select name="cek_spp_status" data-placeholder="Pilih Status SPP..." class="form-control chzn-select" tabindex="2" required>
               <option value=""></option>
               <?php foreach ($db->fetch_all("m_status_spp") as $isi) {

                  if ($data_edit->cek_spp_status==$isi->status_spp_id) {
                    echo "<option value='$isi->status_spp_id' selected>$isi->status_spp_nama</option>";
                  } else {
                  echo "<option value='$isi->status_spp_id'>$isi->status_spp_nama</option>";
                    }
               } ?>
              </select>
                        </div>


</div><!-- /.form-group -->

<div class="alert alert-danger update" style="display:none">   
     <button type="button" class="close" data-dismiss="alert">&times;</button>
          <strong>Perhatian:, Mahasiswa ini sudah membayar SPP....
    </strong> 
</div>

                      <input type="hidden" name="id" value="<?=$data_edit->cek_spp_id;?>">
                      <div class="form-group">
                        <label for="tags" class="control-label col-lg-2">&nbsp;</label>
                        <div class="col-lg-10">
                          <input type="submit" class="btn btn-primary btn-flat" value="submit">
                        </div>
                      </div><!-- /.form-group -->
                    </form>
                    <a href="<?=base_index();?>cek-spp" class="btn btn-success btn-flat"><i class="fa fa-step-backward"></i> Kembali</a>
          
                  </div>
                  </div>
              </div>
</div>
                  
                </section><!-- /.content -->
        
 