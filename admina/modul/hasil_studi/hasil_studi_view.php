
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        Manage Hasil Studi <?php echo $db->fetch_single_row('jurusan','kode_jurusan',$id_jur)->nama_jurusan;?>
                    </h1>
                        <ol class="breadcrumb">
                        <li><a href="<?=base_index();?>"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li><a href="<?=base_index();?>hasil-studi">Hasil Studi</a></li>
                        <li class="active">Hasil Studi List</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="box">
                                <div class="box-header">
                                  <h3 class="box-title">List Hasil Studi</h3>
                                </div><!-- /.box-header -->

<form method="get" class="form-horizontal" action="">
                      
                      <div class="form-group">
                        <label for="Menu" class="control-label col-lg-2">Mahasiswa</label>
                        <div class="col-lg-4">
                        
                        
                          <link href="<?=base_admin();?>assets/plugins/chosen/chosen.min.css" rel="stylesheet" type="text/css" />
                          <script src="<?=base_admin();?>assets/plugins/chosen/chosen.jquery.min.js" type="text/javascript"></script>

                          <!-- Script -->
                          <script type="text/javascript">
                          $(document).ready(function(){
                           $('select').chosen({ height:'50' });
                          });
                          </script>

                        <select name="user" id="id_po_select" data-placeholder="Pilih User" class="form-control chzn-select" tabindex="2" >
                        <option value=""></option>
                          <?php 
                              $semester=ucwords($db->fetch_single_row('semester','status','Aktif')->semester);
                              $username=ucwords($db->fetch_single_row('sys_users','id',$_SESSION['id_user'])->username);
                              $name=ucwords($db->fetch_single_row('mhs','nipd',$username)->nm_pd);
                              if($username<>"Admin"){
                              foreach ($db->fetch_custom("select nipd, nm_pd from mhs                                                                                                                        
                                                          where mhs.nipd=? order by mhs.nm_pd asc", array('mhs.nipd'=>$username)) as $isi) {
                                       if ($username==$isi->nipd) {
                                                   echo "<option value='$isi->nipd' selected>$isi->nipd  -  $isi->nm_pd</option>";
                                       } else {
                                                   echo "<option value='$isi->nipd'>$isi->nipd  -  $isi->nm_pd</option>";
                                       }
                               }
                               }
                               else
                               {
                                foreach ($db->fetch_custom("select nipd, nm_pd from mhs") as $isi) {
                                  
                                       if ($_GET['user']==$isi->nipd) {
                                                   echo "<option value='$isi->nipd' selected>$isi->nipd  -  $isi->nm_pd</option>";
                                                   $username=$_GET['user'];
                                                   $name=ucwords($db->fetch_single_row('mhs','nipd',$username)->nm_pd);
                                       } else {
                                                   echo "<option value='$isi->nipd'>$isi->nipd  -  $isi->nm_pd</option>";
                                       }
                               }
                               } 
                          ?>

                  
                  </select>
                        </div>
                      </div><!-- /.form-group -->

 <label for="Menu" class="control-label col-lg-2">&nbsp;</label>
                        <div class="col-lg-10">
<button style="margin-top:10px;margin-bottom:10px" class="btn btn-primary">Show Hasil Studi</button>
</div>
</form>

                                <div class="box-body table-responsive">
          
<?php if (isset($_GET['user'])) {
$syarat = $_GET['user'];
$kode_semester = $db->fetch_custom_single("select * from m_interval_nilai_grade where int_grade_deskripsi=?",array("cari" =>'C'));   
  
?>       
<h3>Hasil studi :</h3>


<table id="dtb" class="table table-bordered table-condensed table-hover table-striped">
                      <thead align="center">
                        <tr>
                          <th>No.</th>
                          <!--<th>Nama Kurikulum</th>-->
                          <th>Semester</th>
                          <th>Kode Mata Kuliah</th>
                          <th>Nama Mata Kuliah</th>
                          <th>Nilai Huruf</th>
                          <th>Bobot SKS</th>
                          <th>Nilai Indek</th>
                          <th>Nilai Angka</th>
                          <th>Nilai Kehadiran</th>
                          <th>Nilai Tugas</th>
                          <th>Nilai UTS</th>
                          <th>Nilai UAS</th>
                        </tr>
                      </thead>
                      <tbody>
                              <!--<div class="form-group"> <label>Semester : </label><input type="text" id="min" name="min"> </div> -->
                              <?php 
                                  $dtb=$db->fetch_custom("select *, func_cari_grade(kode_mk,?) as grade,
                                                              func_cari_nilai_indek(kode_mk,?) as nilai_indek,
                                                              func_cari_nilai_angka(kode_mk,?) as nilai_angka ,
                                                          func_cari_nilai_kehadiran(kode_mk,?) as nilai_kehadiran ,
                                                              func_cari_nilai_tugas(kode_mk,?) as nilai_tugas,
                                                                func_cari_nilai_uts(kode_mk,?) as nilai_uts,
                                                                func_cari_nilai_uas(kode_mk,?) as nilai_uas
                                                            
                                                            from view_0030_list_all_matkul_per_prodi
                                                            

                                                             ", array('nim1'=>$_GET['user'],
                                                                      'nim2'=>$_GET['user'],
                                                                      'nim3'=>$_GET['user'],
                                                                     ' nim4'=>$_GET['user'],
                                                                      'nim5'=>$_GET['user'],
                                                                      'nim6'=>$_GET['user'],
                                                                      'nim7'=>$_GET['user'],));
                              $mahasiswa = $_GET['user'];
                              $kode_jurusan = substr($mahasiswa, 0, 5);
                              $angkatan = substr($mahasiswa, 6, 2);
                              $key_kurikulum = 0;
                              
                              foreach ($db->fetch_custom("select mulai_berlaku from kurikulum") as $isi) {
                                $mulai_berlaku = substr($isi->mulai_berlaku, 2, 2);
                                if($angkatan >= $mulai_berlaku){
                                  $key_kurikulum = $mulai_berlaku;
                                }
                              }
                              $search_kurikulum = $db->fetch_single_row('kurikulum','mulai_berlaku','%'.$key_kurikulum.'%');
                              
                              $dtb=$db->fetch_custom("SELECT kode_mk, nama_mk, kode_jurusan, semester, sks_tm,
                                                      func_cari_grade(kode_mk,?) as grade,
                                                      func_cari_nilai_indek(kode_mk,?) as nilai_indek,
                                                      func_cari_nilai_angka(kode_mk,?) as nilai_angka ,
                                                      func_cari_nilai_kehadiran(kode_mk,?) as nilai_kehadiran ,
                                                      func_cari_nilai_tugas(kode_mk,?) as nilai_tugas,
                                                      func_cari_nilai_uts(kode_mk,?) as nilai_uts,
                                                      func_cari_nilai_uas(kode_mk,?) as nilai_uas
                                                      FROM mat_kurikulum WHERE kode_jurusan=? and id_kurikulum=? order by semester asc, kode_mk asc", 
                                                          array('nim1'=>$_GET['user'],
                                                                'nim2'=>$_GET['user'],
                                                                'nim3'=>$_GET['user'],
                                                                'nim4'=>$_GET['user'],
                                                                'nim5'=>$_GET['user'],
                                                                'nim6'=>$_GET['user'],
                                                                'nim7'=>$_GET['user'],
                                                                'kode_jurusan'=>$kode_jurusan,
                                                                'id_kurikulum'=>$search_kurikulum->id)) ;
                                  $i=1;
                                  foreach ($dtb as $isi) {
                                    ?>

                                    <tr id="line_<?=$isi->id;?>">
                                   
                                    

                                    <td><?=$i.'.';?></td>
                                    <!--<td align="center"><?=$isi->nama_kur;?></td>-->
                                    <td align="center"><?=$isi->semester;?></td>
                                    <td align="center"><?=$isi->kode_mk;?></td>
                                    <td><?=$isi->nama_mk;?></td>                                    
                                    <td align="center"><b><?=$isi->grade;?></b></td>
                                    <td align="center"><?=$isi->sks_tm;?></td>
                                    <td align="center"><?=$isi->nilai_indek;?></td>
                                    <td align="center"><?=$isi->nilai_angka;?></td>
                                    <td align="center"><?=$isi->nilai_kehadiran;?></td>
                                    <td align="center"><?=$isi->nilai_tugas;?></td>
                                    <td align="center"><?=$isi->nilai_uts;?></td>
                                    <td align="center"><?=$isi->nilai_uas;?></td>
                                    
                                    <?php
                                    if($i)
                                    {
                                        ?>
                                        
                            <?php
                                    } else {

                                    

                                    ?>


                                    
                                                      <?php 
                                                      }
                                                      ?>
                                    </tr>
                                    <?php
                                    $i++;
                                  }
                                  ?>

  <div align="right">    
    <a href="<?=base_admin();?>modul/hasil_studi/khs_report.php?customvar=<?php echo $syarat; ?>" target="_blank" class="btn btn-primary btn-flat" align="right"  <?=($i>1)? '':'disabled';  ?> ><i class="fa fa-file"></i>  Cetak KHS</a>       
 </div>
 <br></br>
                       
                       </tbody>
</table>
<?php 

}  

?>


                                </div><!-- /.box-body -->

                            </div><!-- /.box -->
                        </div>
                    </div>
                    
                    
                    
                </section><!-- /.content -->
  



<script type="text/javascript">



$(document).ready(function() {
      $('#dtb').DataTable( {

        lengthMenu: [100, 200, 500],
        //dom: 'Bfrtip',
        buttons: [
        'colvis'
        ],
        "oLanguage": {

           "sSearch": "Pencarian berdasarkan Semester atau Nama Mata Kuliah :  "

         },
    
         "columnDefs": [ {
      "targets": [0,2,5,6,7,8,9,10,11],
      "searchable": false
    } ],


    } );

   

  } );

 
  
function read_act(id,cb) {
  check_act = '';
  id_cb = id;
  if (cb.checked) {
     check_act = 'Y';

   setTimeout(function() {
   location.reload();
   }, 100);
  
  } else {
    check_act = 'N';

  setTimeout(function() {
   location.reload();
   }, 100);

  }
  
  
  
         

  $.ajax({

        type: "post",
        url: "<?=base_admin();?>modul/ajuan_krs/ajuan_krs_action.php?act=change_read",
        data: "nim="+'<?=$username;?>'+"&nama="+'<?=$name;?>'+"&id="+id_cb+"&cek="+check_act,
        
     //  enctype:  'multipart/form-data'
      success: function(data){
               
        console.log(data);
        //$("#dtb").dataTable().fnReloadAjax();

        //$('#dtb').DataTable().ajax.reload();

    }

  });
}

/*
function insert_act(id,cb) {
  check_act = '';
  if (cb.checked) {
     check_act = 'Y';
  } else {
    check_act = 'N';

  }
  $.ajax({

        type: "post",
        url: "<?=base_admin();?>modul/ajuan_krs/ajuan_krs_action.php?act=change_insert",
        data: "role_id="+id+"&data_act="+check_act,
     //  enctype:  'multipart/form-data'
      success: function(data){

        console.log(data);
    }

  });
}

function update_act(id,cb) {
  check_act = '';
  if (cb.checked) {
     check_act = 'Y';
  } else {
    check_act = 'N';

  }
  $.ajax({

        type: "post",
        url: "<?=base_admin();?>modul/ajuan_krs/ajuan_krs_action.php?act=change_update",
        data: "role_id="+id+"&data_act="+check_act,
     //  enctype:  'multipart/form-data'
      success: function(data){

        console.log(data);
    }

  });
}

function delete_act(id,cb) {
  check_act = '';
  if (cb.checked) {
     check_act = 'Y';
  } else {
    check_act = 'N';

  }
  $.ajax({

        type: "post",
        url: "<?=base_admin();?>modul/ajuan_krs/ajuan_krs_action.php?act=change_delete",
        data: "role_id="+id+"&data_act="+check_act,
     //  enctype:  'multipart/form-data'
      success: function(data){

        console.log(data);
    }

  });
}
*/

  </script>




