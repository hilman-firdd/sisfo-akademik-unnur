<?php
include "../../inc/fpdf.php";

$db = new PDO('mysql:host=localhost;dbname=ekomyanu_akademik','root','');

$syarat =  $_GET['customvar'];
$username = $syarat;   
$query=$db->query('SELECT DISTINCT * from view_0025_kartu_hasil_studi where nim ='.$syarat.' or nim is null');
$data = $query->fetch(PDO::FETCH_OBJ);

$nama_mhs = $data->nama;
$npm = $data->nim;
$jurusan = $data->nama_jurusan;
$nama_gelar = $data->nama_gelar;
$nidn = $data->dosen_nidn;


class myPDF extends FPDF{
    function header(){

        $this->Image('../../assets/logo_bw.png',10,6,-400);
        $this->ln(2);
        $this->SetFont('Arial','B',14);
        $this->cell(197,5,'UNIVERSITAS NURTANIO BANDUNG',0,1,'C');
        $this->SetFont('Arial','',14);
        $this->cell(197,5,'FAKULTAS ILMU KOMPUTER DAN INFORMATIKA',0,1,'C');
        $this->SetFont('Arial','',10);
        $this->cell(197,5,'Jl.Pajajaran No.219 Lanud Husein S.Bandung 40174 Telp.022 86061700/Fax 86061701',0,1,'C');
        $this->Line(10, 32, 210-10, 32);
        $this->ln(15);

        $this->SetFont('Arial','B',12);
        $this->cell(190,7,'DAFTAR HASIL STUDI SEMENTARA',0,1,'C');

        $this->ln(5);

        $this->SetFont('Times','',10);
        $this->cell(10,7,'',0,0); // dummy
        $this->cell(15,7,'Nama',0,0,'L');
        $this->Cell(5,7,' : ' ,0,0,'L'); 
        $this->Cell(100,7,$GLOBALS['nama_mhs'] ,0,1,'L');

        $this->cell(10,7,'',0,0); // dummy
        $this->cell(15,7,'NPM',0,0,'L');
        $this->Cell(5,7,' : ' ,0,0,'L'); 
        $this->Cell(100,7,$GLOBALS['npm'] ,0,1,'L');

        $this->cell(10,7,'',0,0); // dummy
        $this->cell(15,7,'Jurusan',0,0,'L');
        $this->Cell(5,7,' : ' ,0,0,'L'); 
        $this->Cell(100,7,$GLOBALS['jurusan'] ,0,1,'L');

        $this->Ln(5);



        
    }
    function footer(){
        $this->SetY(-15);
        $this->SetFont('Arial','',8);
        $this->Cell(0,5,'Hal. '.$this->PageNo().' dari {nb}',0,1,'C');
        $this->Cell(0,5,'Tanggal Cetak : [' . date("d/m/yy h:m:s").']',0,0,'C');
    }
    function headerTable(){
       
        
    }

    function viewTable($db){

        /*
        $ip1=$db->query('select ip from view_0028_jml_matkul_per_semester where where nim ='.$GLOBALS['syarat'].' and semester=1'); 
            if(isset($ip1->ip)){echo($ip1->ip);} else {echo "0.00";}  

        $ip2=$db->query('select ip from view_0028_jml_matkul_per_semester where where nim ='.$GLOBALS['syarat'].' and semester=2'); 
            if(isset($ip2->ip)){echo($ip2->ip);} else {echo "0.00";}  

        $ip3=$db->query('select ip from view_0028_jml_matkul_per_semester where where nim ='.$GLOBALS['syarat'].' and semester=3'); 
            if(isset($ip3->ip)){echo($ip3->ip);} else {echo "0.00";}  
        
        $ip4=$db->query('select ip from view_0028_jml_matkul_per_semester where where nim ='.$GLOBALS['syarat'].' and semester=4'); 
            if(isset($ip4->ip)){echo($ip4->ip);} else {echo "0.00";}  

        $ip5=$db->query('select ip from view_0028_jml_matkul_per_semester where where nim ='.$GLOBALS['syarat'].' and semester=5'); 
            if(isset($ip5->ip)){echo($ip5->ip);} else {echo "0.00";}  

        $ip6=$db->query('select ip from view_0028_jml_matkul_per_semester where where nim ='.$GLOBALS['syarat'].' and semester=6'); 
            if(isset($ip6->ip)){echo($ip6->ip);} else {echo "0.00";}  

        $ip7=$db->query('select ip from view_0028_jml_matkul_per_semester where where nim ='.$GLOBALS['syarat'].' and semester=7'); 
            if(isset($ip7->ip)){echo($ip7->ip);} else {echo "0.00";}      

        $ip8=$db->query('select ip from view_0028_jml_matkul_per_semester where where nim ='.$GLOBALS['syarat'].' and semester=8'); 
            if(isset($ip8->ip)){echo($ip8->ip);} else {echo "0.00";}      
                
       */
        
        //SEMESTER 1 

        $this->SetFont('Times','',9);

        $this->cell(10,10,'SMT',1,0,'C');
        $this->cell(7,10,'NO',1,0,'C');
        $this->cell(18,10,'KODE MK',1,0,'C');
        $this->cell(75,10,'Nama Mata Kuliah',1,0,'C');
        $this->cell(10,10,'SKS',1,0,'C');
        $this->cell(15,5,'NILAI','T',0,'C');
        $this->cell(15,10,'AM * SKS',1,0,'C');
        $this->cell(10,10,'',1,0,'C');
        $this->cell(10,10,'',1,0,'C');
        $this->cell(10,10,'',1,0,'C');
        $this->cell(10,10,'',1,0,'C');
        $this->cell(10,5,'',0,1,'C'); //dummy

        $this->cell(120,10,'',0,0); //dummy
        $this->cell(15,5,'(AM)','B',1,'C');

        /*$query=$db->query('SELECT DISTINCT * from view_0025_kartu_hasil_studi where (nim ='.$GLOBALS['syarat'].' or nim is null)  and semester =1');*/
        
        $query=$db->query('select *, func_cari_grade(kode_mk,'.$GLOBALS['syarat'].') as nilai_huruf,
                                                              func_cari_nilai_indek(kode_mk,'.$GLOBALS['syarat'].') as nilai_indek,
                                                              func_cari_nilai_angka(kode_mk,'.$GLOBALS['syarat'].') as nilai_angka ,
                                                          func_cari_nilai_kehadiran(kode_mk,'.$GLOBALS['syarat'].') as nilai_kehadiran ,
                                                              func_cari_nilai_tugas(kode_mk,'.$GLOBALS['syarat'].') as nilai_tugas,
                                                                func_cari_nilai_uts(kode_mk,'.$GLOBALS['syarat'].') as nilai_uts,
                                                                func_cari_nilai_uas(kode_mk,'.$GLOBALS['syarat'].') as nilai_uas
                                                            
                                                            from view_0030_list_all_matkul_per_prodi  where semester =1');
        
        $sks_1=0; $am_sks_1=0;
        $i=1;
        while($data = $query->fetch(PDO::FETCH_OBJ)){

        $this->cell(10,6,$data->semester,1,0,'C');
        $this->cell(7,6,$i.'.',1,0,'C');
        $this->cell(18,6,$data->kode_mk,1,0,'C');
        $this->cell(75,6,$data->nama_mk,1,0,'L');
        $this->cell(10,6,$data->sks_tm,1,0,'C');
        $this->cell(15,6,$data->nilai_huruf,1,0,'C');
        $this->cell(15,6,$data->sks_tm*$data->nilai_indek,1,0,'C');
        $this->cell(10,6,'',1,0,'C');
        $this->cell(10,6,'',1,0,'C');
        $this->cell(10,6,'',1,0,'C');
        $this->cell(10,6,'',1,1,'C');
        if($data->nilai_huruf =='A' or $data->nilai_huruf=='B' or $data->nilai_huruf=='C' or $data->nilai_huruf=='D' or $data->nilai_huruf=='E')
        {   $sks_1=$sks_1+$data->sks_tm;
            $am_sks_1 = $am_sks_1 + $data->sks_tm*$data->nilai_indek;
        }

        $i=$i+1;
        }

        $this->cell(110,5,'Jumlah Sks Yang Diambil : ',1,0,'C');
        $this->cell(10,5,$sks_1,1,0,'C');
        $this->cell(15,5,'',1,0,'C');
        $this->cell(15,5,$am_sks_1,1,0,'C');
        $this->cell(10,5,'IPS :',1,0,'C');
        
       
        if($sks_1<>0){$this->cell(10,5,substr($am_sks_1/$sks_1,0,4),1,0,'C');} else {$this->cell(10,5,'',1,0,'C');}
        $this->cell(10,5,'IPK :',1,0,'C');
        if($sks_1<>0){$this->cell(10,5,substr($am_sks_1/$sks_1,0,4),1,1,'C');} else {$this->cell(10,5,'',1,1,'C');}
        $this->ln(10);


        
        //SEMESTER 2

        $this->cell(10,10,'SMT',1,0,'C');
        $this->cell(7,10,'NO',1,0,'C');
        $this->cell(18,10,'KODE MK',1,0,'C');
        $this->cell(75,10,'Nama Mata Kuliah',1,0,'C');
        $this->cell(10,10,'SKS',1,0,'C');
        $this->cell(15,5,'NILAI','T',0,'C');
        $this->cell(15,10,'AM * SKS',1,0,'C');
        $this->cell(10,10,'',1,0,'C');
        $this->cell(10,10,'',1,0,'C');
        $this->cell(10,10,'',1,0,'C');
        $this->cell(10,10,'',1,0,'C');
        $this->cell(10,5,'',0,1,'C'); //dummy

        $this->cell(120,10,'',0,0); //dummy
        $this->cell(15,5,'(AM)','B',1,'C');
        
        $query=$db->query('select *, func_cari_grade(kode_mk,'.$GLOBALS['syarat'].') as nilai_huruf,
                                                              func_cari_nilai_indek(kode_mk,'.$GLOBALS['syarat'].') as nilai_indek,
                                                              func_cari_nilai_angka(kode_mk,'.$GLOBALS['syarat'].') as nilai_angka ,
                                                          func_cari_nilai_kehadiran(kode_mk,'.$GLOBALS['syarat'].') as nilai_kehadiran ,
                                                              func_cari_nilai_tugas(kode_mk,'.$GLOBALS['syarat'].') as nilai_tugas,
                                                                func_cari_nilai_uts(kode_mk,'.$GLOBALS['syarat'].') as nilai_uts,
                                                                func_cari_nilai_uas(kode_mk,'.$GLOBALS['syarat'].') as nilai_uas
                                                            
                                                            from view_0030_list_all_matkul_per_prodi  where semester =2');
        $sks_2=0; $am_sks_2=0;
        $i=1;
        while($data = $query->fetch(PDO::FETCH_OBJ)){

        $this->cell(10,6,$data->semester,1,0,'C');
        $this->cell(7,6,$i.'.',1,0,'C');
        $this->cell(18,6,$data->kode_mk,1,0,'C');
        $this->cell(75,6,$data->nama_mk,1,0,'L');
        $this->cell(10,6,$data->sks_tm,1,0,'C');
        $this->cell(15,6,$data->nilai_huruf,1,0,'C');
        $this->cell(15,6,$data->sks_tm*$data->nilai_indek,1,0,'C');
        $this->cell(10,6,'',1,0,'C');
        $this->cell(10,6,'',1,0,'C');
        $this->cell(10,6,'',1,0,'C');
        $this->cell(10,6,'',1,1,'C');
        if($data->nilai_huruf =='A' or $data->nilai_huruf=='B' or $data->nilai_huruf=='C' or $data->nilai_huruf=='D' or $data->nilai_huruf=='E')
        {   $sks_2=$sks_2+$data->sks_tm;
            $am_sks_2 = $am_sks_2 + $data->sks_tm*$data->nilai_indek;
        }

        $i=$i+1;
        }

        $this->cell(110,5,'Jumlah Sks Yang Diambil : ',1,0,'C');
        $this->cell(10,5,$sks_2,1,0,'C');
        $this->cell(15,5,'',1,0,'C');
        $this->cell(15,5,$am_sks_2,1,0,'C');
        $this->cell(10,5,'IPS :',1,0,'C');
        if($sks_2<>0){$this->cell(10,5,substr($am_sks_2/$sks_2,0,4),1,0,'C');} else {$this->cell(10,5,'',1,0,'C');}
        $this->cell(10,5,'IPK :',1,0,'C');
        if($sks_2<>0){$this->cell(10,5,substr(($am_sks_1+$am_sks_2)/($sks_1+$sks_2),0,4),1,1,'C');} else {$this->cell(10,5,'',1,1,'C');}
        $this->ln(10);

        //SEMESTER 3

        $this->cell(10,10,'SMT',1,0,'C');
        $this->cell(7,10,'NO',1,0,'C');
        $this->cell(18,10,'KODE MK',1,0,'C');
        $this->cell(75,10,'Nama Mata Kuliah',1,0,'C');
        $this->cell(10,10,'SKS',1,0,'C');
        $this->cell(15,5,'NILAI','T',0,'C');
        $this->cell(15,10,'AM * SKS',1,0,'C');
        $this->cell(10,10,'',1,0,'C');
        $this->cell(10,10,'',1,0,'C');
        $this->cell(10,10,'',1,0,'C');
        $this->cell(10,10,'',1,0,'C');
        $this->cell(10,5,'',0,1,'C'); //dummy

        $this->cell(120,10,'',0,0); //dummy
        $this->cell(15,5,'(AM)','B',1,'C');

        $query=$db->query('select *, func_cari_grade(kode_mk,'.$GLOBALS['syarat'].') as nilai_huruf,
                                                              func_cari_nilai_indek(kode_mk,'.$GLOBALS['syarat'].') as nilai_indek,
                                                              func_cari_nilai_angka(kode_mk,'.$GLOBALS['syarat'].') as nilai_angka ,
                                                          func_cari_nilai_kehadiran(kode_mk,'.$GLOBALS['syarat'].') as nilai_kehadiran ,
                                                              func_cari_nilai_tugas(kode_mk,'.$GLOBALS['syarat'].') as nilai_tugas,
                                                                func_cari_nilai_uts(kode_mk,'.$GLOBALS['syarat'].') as nilai_uts,
                                                                func_cari_nilai_uas(kode_mk,'.$GLOBALS['syarat'].') as nilai_uas
                                                            
                                                            from view_0030_list_all_matkul_per_prodi  where semester =3');
        $sks_3=0; $am_sks_3=0;
        $i=1;
        while($data = $query->fetch(PDO::FETCH_OBJ)){

        $this->cell(10,6,$data->semester,1,0,'C');
        $this->cell(7,6,$i.'.',1,0,'C');
        $this->cell(18,6,$data->kode_mk,1,0,'C');
        $this->cell(75,6,$data->nama_mk,1,0,'L');
        $this->cell(10,6,$data->sks_tm,1,0,'C');
        $this->cell(15,6,$data->nilai_huruf,1,0,'C');
        $this->cell(15,6,$data->sks_tm*$data->nilai_indek,1,0,'C');
        $this->cell(10,6,'',1,0,'C');
        $this->cell(10,6,'',1,0,'C');
        $this->cell(10,6,'',1,0,'C');
        $this->cell(10,6,'',1,1,'C');
        if($data->nilai_huruf =='A' or $data->nilai_huruf=='B' or $data->nilai_huruf=='C' or $data->nilai_huruf=='D' or $data->nilai_huruf=='E')
        {   $sks_3=$sks_3+$data->sks_tm;
            $am_sks_3 = $am_sks_3 + $data->sks_tm*$data->nilai_indek;
        }

        $i=$i+1;
        }

        $this->cell(110,5,'Jumlah Sks Yang Diambil : ',1,0,'C');
        $this->cell(10,5,$sks_3,1,0,'C');
        $this->cell(15,5,'',1,0,'C');
        $this->cell(15,5,$am_sks_3,1,0,'C');
        $this->cell(10,5,'IPS :',1,0,'C');
        if($sks_3<>0){$this->cell(10,5,substr($am_sks_3/$sks_3,0,4),1,0,'C');} else {$this->cell(10,5,'',1,0,'C');}
        $this->cell(10,5,'IPK :',1,0,'C');
        if ($sks_3<>0) {$this->cell(10,5,substr(($am_sks_1+$am_sks_2+$am_sks_3)/($sks_1+$sks_2+$sks_3),0,4),1,1,'C');} else {$this->cell(10,5,'',1,1,'C');}
        $this->ln(10);

        //SEMESTER 4

        $this->cell(10,10,'SMT',1,0,'C');
        $this->cell(7,10,'NO',1,0,'C');
        $this->cell(18,10,'KODE MK',1,0,'C');
        $this->cell(75,10,'Nama Mata Kuliah',1,0,'C');
        $this->cell(10,10,'SKS',1,0,'C');
        $this->cell(15,5,'NILAI','T',0,'C');
        $this->cell(15,10,'AM * SKS',1,0,'C');
        $this->cell(10,10,'',1,0,'C');
        $this->cell(10,10,'',1,0,'C');
        $this->cell(10,10,'',1,0,'C');
        $this->cell(10,10,'',1,0,'C');
        $this->cell(10,5,'',0,1,'C'); //dummy

        $this->cell(120,10,'',0,0); //dummy
        $this->cell(15,5,'(AM)','B',1,'C');

        $query=$db->query('select *, func_cari_grade(kode_mk,'.$GLOBALS['syarat'].') as nilai_huruf,
                                                              func_cari_nilai_indek(kode_mk,'.$GLOBALS['syarat'].') as nilai_indek,
                                                              func_cari_nilai_angka(kode_mk,'.$GLOBALS['syarat'].') as nilai_angka ,
                                                          func_cari_nilai_kehadiran(kode_mk,'.$GLOBALS['syarat'].') as nilai_kehadiran ,
                                                              func_cari_nilai_tugas(kode_mk,'.$GLOBALS['syarat'].') as nilai_tugas,
                                                                func_cari_nilai_uts(kode_mk,'.$GLOBALS['syarat'].') as nilai_uts,
                                                                func_cari_nilai_uas(kode_mk,'.$GLOBALS['syarat'].') as nilai_uas
                                                            
                                                            from view_0030_list_all_matkul_per_prodi  where semester =4');
        $sks_4=0; $am_sks_4=0;
        $i=1;
        while($data = $query->fetch(PDO::FETCH_OBJ)){

        $this->cell(10,6,$data->semester,1,0,'C');
        $this->cell(7,6,$i.'.',1,0,'C');
        $this->cell(18,6,$data->kode_mk,1,0,'C');
        $this->cell(75,6,$data->nama_mk,1,0,'L');
        $this->cell(10,6,$data->sks_tm,1,0,'C');
        $this->cell(15,6,$data->nilai_huruf,1,0,'C');
        $this->cell(15,6,$data->sks_tm*$data->nilai_indek,1,0,'C');
        $this->cell(10,6,'',1,0,'C');
        $this->cell(10,6,'',1,0,'C');
        $this->cell(10,6,'',1,0,'C');
        $this->cell(10,6,'',1,1,'C');
        if($data->nilai_huruf =='A' or $data->nilai_huruf=='B' or $data->nilai_huruf=='C' or $data->nilai_huruf=='D' or $data->nilai_huruf=='E')
        {   $sks_4=$sks_4+$data->sks_tm;
            $am_sks_4 = $am_sks_4 + $data->sks_tm*$data->nilai_indek;
        }

        $i=$i+1;
        }

        $this->cell(110,5,'Jumlah Sks Yang Diambil : ',1,0,'C');
        $this->cell(10,5,$sks_4,1,0,'C');
        $this->cell(15,5,'',1,0,'C');
        $this->cell(15,5,$am_sks_4,1,0,'C');
        $this->cell(10,5,'IPS :',1,0,'C');
        if($sks_4<>0){$this->cell(10,5,substr($am_sks_4/$sks_4,0,4),1,0,'C');} else {$this->cell(10,5,'',1,0,'C');}
        $this->cell(10,5,'IPK :',1,0,'C');
        if($sks_4<>0){$this->cell(10,5,substr(($am_sks_1+$am_sks_2+$am_sks_3+$am_sks_4)/($sks_1+$sks_2+$sks_3+$sks_4),0,4),1,1,'C');} else {$this->cell(10,5,'',1,1,'C');}
        $this->ln(10);


        //SEMESTER 5

        $this->cell(10,10,'SMT',1,0,'C');
        $this->cell(7,10,'NO',1,0,'C');
        $this->cell(18,10,'KODE MK',1,0,'C');
        $this->cell(75,10,'Nama Mata Kuliah',1,0,'C');
        $this->cell(10,10,'SKS',1,0,'C');
        $this->cell(15,5,'NILAI','T',0,'C');
        $this->cell(15,10,'AM * SKS',1,0,'C');
        $this->cell(10,10,'',1,0,'C');
        $this->cell(10,10,'',1,0,'C');
        $this->cell(10,10,'',1,0,'C');
        $this->cell(10,10,'',1,0,'C');
        $this->cell(10,5,'',0,1,'C'); //dummy

        $this->cell(120,10,'',0,0); //dummy
        $this->cell(15,5,'(AM)','B',1,'C');

        $query=$db->query('select *, func_cari_grade(kode_mk,'.$GLOBALS['syarat'].') as nilai_huruf,
                                                              func_cari_nilai_indek(kode_mk,'.$GLOBALS['syarat'].') as nilai_indek,
                                                              func_cari_nilai_angka(kode_mk,'.$GLOBALS['syarat'].') as nilai_angka ,
                                                          func_cari_nilai_kehadiran(kode_mk,'.$GLOBALS['syarat'].') as nilai_kehadiran ,
                                                              func_cari_nilai_tugas(kode_mk,'.$GLOBALS['syarat'].') as nilai_tugas,
                                                                func_cari_nilai_uts(kode_mk,'.$GLOBALS['syarat'].') as nilai_uts,
                                                                func_cari_nilai_uas(kode_mk,'.$GLOBALS['syarat'].') as nilai_uas
                                                            
                                                            from view_0030_list_all_matkul_per_prodi  where semester =5');
        $sks_5=0; $am_sks_5=0;
        $i=1;
        while($data = $query->fetch(PDO::FETCH_OBJ)){

        $this->cell(10,6,$data->semester,1,0,'C');
        $this->cell(7,6,$i.'.',1,0,'C');
        $this->cell(18,6,$data->kode_mk,1,0,'C');
        $this->cell(75,6,$data->nama_mk,1,0,'L');
        $this->cell(10,6,$data->sks_tm,1,0,'C');
        $this->cell(15,6,$data->nilai_huruf,1,0,'C');
        $this->cell(15,6,$data->sks_tm*$data->nilai_indek,1,0,'C');
        $this->cell(10,6,'',1,0,'C');
        $this->cell(10,6,'',1,0,'C');
        $this->cell(10,6,'',1,0,'C');
        $this->cell(10,6,'',1,1,'C');
        if($data->nilai_huruf =='A' or $data->nilai_huruf=='B' or $data->nilai_huruf=='C' or $data->nilai_huruf=='D' or $data->nilai_huruf=='E')
        {   $sks_5=$sks_5+$data->sks_tm;
            $am_sks_5 = $am_sks_5 + $data->sks_tm*$data->nilai_indek;
        }

        $i=$i+1;
        }

        $this->cell(110,5,'Jumlah Sks Yang Diambil : ',1,0,'C');
        $this->cell(10,5,$sks_5,1,0,'C');
        $this->cell(15,5,'',1,0,'C');
        $this->cell(15,5,$am_sks_5,1,0,'C');
        $this->cell(10,5,'IPS :',1,0,'C');
        if($sks_5<>0){$this->cell(10,5,substr($am_sks_5/$sks_5,0,4),1,0,'C');} else {$this->cell(10,5,'',1,0,'C');}
        $this->cell(10,5,'IPK :',1,0,'C');
        if($sks_5<>0){$this->cell(10,5,substr(($am_sks_1+$am_sks_2+$am_sks_3+$am_sks_4+$am_sks_5)/($sks_1+$sks_2+$sks_3+$sks_4+$sks_5),0,4),1,1,'C');} else {$this->cell(10,5,'',1,1,'C');}
        $this->ln(10);
       
       //SEMESTER 6

        $this->cell(10,10,'SMT',1,0,'C');
        $this->cell(7,10,'NO',1,0,'C');
        $this->cell(18,10,'KODE MK',1,0,'C');
        $this->cell(75,10,'Nama Mata Kuliah',1,0,'C');
        $this->cell(10,10,'SKS',1,0,'C');
        $this->cell(15,5,'NILAI','T',0,'C');
        $this->cell(15,10,'AM * SKS',1,0,'C');
        $this->cell(10,10,'',1,0,'C');
        $this->cell(10,10,'',1,0,'C');
        $this->cell(10,10,'',1,0,'C');
        $this->cell(10,10,'',1,0,'C');
        $this->cell(10,5,'',0,1,'C'); //dummy

        $this->cell(120,10,'',0,0); //dummy
        $this->cell(15,5,'(AM)','B',1,'C');

        $query=$db->query('select *, func_cari_grade(kode_mk,'.$GLOBALS['syarat'].') as nilai_huruf,
                                                              func_cari_nilai_indek(kode_mk,'.$GLOBALS['syarat'].') as nilai_indek,
                                                              func_cari_nilai_angka(kode_mk,'.$GLOBALS['syarat'].') as nilai_angka ,
                                                          func_cari_nilai_kehadiran(kode_mk,'.$GLOBALS['syarat'].') as nilai_kehadiran ,
                                                              func_cari_nilai_tugas(kode_mk,'.$GLOBALS['syarat'].') as nilai_tugas,
                                                                func_cari_nilai_uts(kode_mk,'.$GLOBALS['syarat'].') as nilai_uts,
                                                                func_cari_nilai_uas(kode_mk,'.$GLOBALS['syarat'].') as nilai_uas
                                                            
                                                            from view_0030_list_all_matkul_per_prodi  where semester =6');
        $sks_6=0; $am_sks_6=0;
        $i=1;
        while($data = $query->fetch(PDO::FETCH_OBJ)){

        $this->cell(10,6,$data->semester,1,0,'C');
        $this->cell(7,6,$i.'.',1,0,'C');
        $this->cell(18,6,$data->kode_mk,1,0,'C');
        $this->cell(75,6,$data->nama_mk,1,0,'L');
        $this->cell(10,6,$data->sks_tm,1,0,'C');
        $this->cell(15,6,$data->nilai_huruf,1,0,'C');
        $this->cell(15,6,$data->sks_tm * $data->nilai_indek,1,0,'C');
        $this->cell(10,6,'',1,0,'C');
        $this->cell(10,6,'',1,0,'C');
        $this->cell(10,6,'',1,0,'C');
        $this->cell(10,6,'',1,1,'C');
        if($data->nilai_huruf =='A' or $data->nilai_huruf=='B' or $data->nilai_huruf=='C' or $data->nilai_huruf=='D' or $data->nilai_huruf=='E')
        {   $sks_6=$sks_6+$data->sks_tm;
            $am_sks_6 = $am_sks_6 + $data->sks_tm*$data->nilai_indek;
        }

        $i=$i+1;
        }

        $this->cell(110,5,'Jumlah Sks Yang Diambil : ',1,0,'C');
        $this->cell(10,5,$sks_6,1,0,'C');
        $this->cell(15,5,'',1,0,'C');
        $this->cell(15,5,$am_sks_6,1,0,'C');
        $this->cell(10,5,'IPS :',1,0,'C');
        if($sks_6<>0){$this->cell(10,5,substr($am_sks_6/$sks_6,0,4),1,0,'C');} else {$this->cell(10,5,'',1,0,'C');}
        $this->cell(10,5,'IPK :',1,0,'C');
        if($sks_6<>0){$this->cell(10,5,substr(($am_sks_1+$am_sks_2+$am_sks_3+$am_sks_4+$am_sks_5+$am_sks_6)/($sks_1+$sks_2+$sks_3+$sks_4+$sks_5+$sks_6),0,4),1,1,'C');} else {$this->cell(10,5,'',1,1,'C');}
        $this->ln(10);

        //SEMESTER 7

        $this->cell(10,10,'SMT',1,0,'C');
        $this->cell(7,10,'NO',1,0,'C');
        $this->cell(18,10,'KODE MK',1,0,'C');
        $this->cell(75,10,'Nama Mata Kuliah',1,0,'C');
        $this->cell(10,10,'SKS',1,0,'C');
        $this->cell(15,5,'NILAI','T',0,'C');
        $this->cell(15,10,'AM * SKS',1,0,'C');
        $this->cell(10,10,'',1,0,'C');
        $this->cell(10,10,'',1,0,'C');
        $this->cell(10,10,'',1,0,'C');
        $this->cell(10,10,'',1,0,'C');
        $this->cell(10,5,'',0,1,'C'); //dummy

        $this->cell(120,10,'',0,0); //dummy
        $this->cell(15,5,'(AM)','B',1,'C');

        $query=$db->query('select *, func_cari_grade(kode_mk,'.$GLOBALS['syarat'].') as nilai_huruf,
                                                              func_cari_nilai_indek(kode_mk,'.$GLOBALS['syarat'].') as nilai_indek,
                                                              func_cari_nilai_angka(kode_mk,'.$GLOBALS['syarat'].') as nilai_angka ,
                                                          func_cari_nilai_kehadiran(kode_mk,'.$GLOBALS['syarat'].') as nilai_kehadiran ,
                                                              func_cari_nilai_tugas(kode_mk,'.$GLOBALS['syarat'].') as nilai_tugas,
                                                                func_cari_nilai_uts(kode_mk,'.$GLOBALS['syarat'].') as nilai_uts,
                                                                func_cari_nilai_uas(kode_mk,'.$GLOBALS['syarat'].') as nilai_uas
                                                            
                                                            from view_0030_list_all_matkul_per_prodi  where semester =7');
        $sks_7=0; $am_sks_7=0;
        $i=1;
        while($data = $query->fetch(PDO::FETCH_OBJ)){

        $this->cell(10,6,$data->semester,1,0,'C');
        $this->cell(7,6,$i.'.',1,0,'C');
        $this->cell(18,6,$data->kode_mk,1,0,'C');
        $this->cell(75,6,$data->nama_mk,1,0,'L');
        $this->cell(10,6,$data->sks_tm,1,0,'C');
        $this->cell(15,6,$data->nilai_huruf,1,0,'C');
        $this->cell(15,6,$data->sks_tm*$data->nilai_indek,1,0,'C');
        $this->cell(10,6,'',1,0,'C');
        $this->cell(10,6,'',1,0,'C');
        $this->cell(10,6,'',1,0,'C');
        $this->cell(10,6,'',1,1,'C');
        if($data->nilai_huruf =='A' or $data->nilai_huruf=='B' or $data->nilai_huruf=='C' or $data->nilai_huruf=='D' or $data->nilai_huruf=='E')
        {   $sks_7=$sks_7+$data->sks_tm;
            $am_sks_7 = $am_sks_7 + $data->sks_tm*$data->nilai_indek;
        }

        $i=$i+1;
        }

        $this->cell(110,5,'Jumlah Sks Yang Diambil : ',1,0,'C');
        $this->cell(10,5,$sks_7,1,0,'C');
        $this->cell(15,5,'',1,0,'C');
        $this->cell(15,5,$am_sks_7,1,0,'C');
        $this->cell(10,5,'IPS :',1,0,'C');
        if($sks_7<>0){$this->cell(10,5,substr($am_sks_7/$sks_7,0,4),1,0,'C');} else {$this->cell(10,5,'',1,0,'C');}
        $this->cell(10,5,'IPK :',1,0,'C');
        if($sks_7<>0){$this->cell(10,5,substr(($am_sks_1+$am_sks_2+$am_sks_3+$am_sks_4+$am_sks_5+$am_sks_6+$am_sks_7)/($sks_1+$sks_2+$sks_3+$sks_4+$sks_5+$sks_6+$sks_7),0,4),1,1,'C');} else {$this->cell(10,5,'',1,1,'C');}
        $this->ln(10);

        //SEMESTER 8

        $this->cell(10,10,'SMT',1,0,'C');
        $this->cell(7,10,'NO',1,0,'C');
        $this->cell(18,10,'KODE MK',1,0,'C');
        $this->cell(75,10,'Nama Mata Kuliah',1,0,'C');
        $this->cell(10,10,'SKS',1,0,'C');
        $this->cell(15,5,'NILAI','T',0,'C');
        $this->cell(15,10,'AM * SKS',1,0,'C');
        $this->cell(10,10,'',1,0,'C');
        $this->cell(10,10,'',1,0,'C');
        $this->cell(10,10,'',1,0,'C');
        $this->cell(10,10,'',1,0,'C');
        $this->cell(10,5,'',0,1,'C'); //dummy

        $this->cell(120,10,'',0,0); //dummy
        $this->cell(15,5,'(AM)','B',1,'C');

        $query=$db->query('select *, func_cari_grade(kode_mk,'.$GLOBALS['syarat'].') as nilai_huruf,
                                                              func_cari_nilai_indek(kode_mk,'.$GLOBALS['syarat'].') as nilai_indek,
                                                              func_cari_nilai_angka(kode_mk,'.$GLOBALS['syarat'].') as nilai_angka ,
                                                          func_cari_nilai_kehadiran(kode_mk,'.$GLOBALS['syarat'].') as nilai_kehadiran ,
                                                              func_cari_nilai_tugas(kode_mk,'.$GLOBALS['syarat'].') as nilai_tugas,
                                                                func_cari_nilai_uts(kode_mk,'.$GLOBALS['syarat'].') as nilai_uts,
                                                                func_cari_nilai_uas(kode_mk,'.$GLOBALS['syarat'].') as nilai_uas
                                                            
                                                            from view_0030_list_all_matkul_per_prodi  where semester =8');
        $sks_8=0; $am_sks_8=0;
        $i=1;
        while($data = $query->fetch(PDO::FETCH_OBJ)){

        $this->cell(10,6,$data->semester,1,0,'C');
        $this->cell(7,6,$i.'.',1,0,'C');
        $this->cell(18,6,$data->kode_mk,1,0,'C');
        $this->cell(75,6,$data->nama_mk,1,0,'L');
        $this->cell(10,6,$data->sks_tm,1,0,'C');
        $this->cell(15,6,$data->nilai_huruf,1,0,'C');
        $this->cell(15,6,$data->sks_tm*$data->nilai_indek,1,0,'C');
        $this->cell(10,6,'',1,0,'C');
        $this->cell(10,6,'',1,0,'C');
        $this->cell(10,6,'',1,0,'C');
        $this->cell(10,6,'',1,1,'C');
        if($data->nilai_huruf =='A' or $data->nilai_huruf=='B' or $data->nilai_huruf=='C' or $data->nilai_huruf=='D' or $data->nilai_huruf=='E')
        {   $sks_8=$sks_8+$data->sks_tm;
            $am_sks_8 = $am_sks_8 + $data->sks_tm*$data->nilai_indek;
        }

        $i=$i+1;
        }

        $this->cell(110,5,'Jumlah Sks Yang Diambil : ',1,0,'C');
        $this->cell(10,5,$sks_8,1,0,'C');
        $this->cell(15,5,'',1,0,'C');
        $this->cell(15,5,$am_sks_8,1,0,'C');
        $this->cell(10,5,'IPS :',1,0,'C');
        if($sks_8<>0){$this->cell(10,5,substr($am_sks_8/$sks_8,0,4),1,0,'C');} else {$this->cell(10,5,'',1,0,'C');}
        $this->cell(10,5,'IPK :',1,0,'C');
        if($sks_8<>0){$this->cell(10,5,substr(($am_sks_1+$am_sks_2+$am_sks_3+$am_sks_4+$am_sks_5+$am_sks_6+$am_sks_7+$am_sks_8)/($sks_1+$sks_2+$sks_3+$sks_4+$sks_5+$sks_6+$sks_7+$sks_8),0,4),1,1,'C');} else {$this->cell(10,5,'',1,1,'C');}
        $this->ln(10);

    }
    function footerTable($db){
      
        $this->cell(110,5,'Jumlah Sks Yang Diambil : ',1,0,'C');
        $this->cell(10,5,'[f_t]',1,0,'C');
        $this->cell(15,5,'',1,0,'C');
        $this->cell(15,5,'',1,0,'C');
        $this->cell(10,5,'IPS :',1,0,'C');
        $this->cell(10,5,'[f_t]',1,0,'C');
        $this->cell(10,5,'IPK :',1,0,'C');
        $this->cell(10,5,'[f_t]',1,1,'C');

        $this->Ln(5);
    }
    function signTable(){
        
        $this->SetFont('Times','',10);

        $this->cell(320,10,'Kaprodi Teknik Informatika',0,1,'C');
        $this->cell(320,15,'',0,1,'C'); //dummy
        $this->cell(320,10,$GLOBALS['nama_gelar'],0,1,'C');
        $this->cell(320,2,'NIDN.'.$GLOBALS['nidn'],0,1,'C');
    }
}
$pdf = new myPDF('P','mm','A4');
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->headerTable();
$pdf->viewTable($db);
//$pdf->footerTable($db);
$pdf->signTable();
$pdf->Output();

?>