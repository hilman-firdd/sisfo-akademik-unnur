
           
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                     Data Buku
                    </h1>
                           <ol class="breadcrumb">
                        <li><a href="<?=base_index();?>"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li><a href="<?=base_index();?>data-buku">Data Buku</a></li>
                        <li class="active">Tambah Data Buku</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
<div class="row">
    <div class="col-lg-12"> 
        <div class="box box-solid box-primary">
                                 <div class="box-header">
                                    <h3 class="box-title">Tambah Data Buku</h3>
                                    <div class="box-tools pull-right">
                                        <button class="btn btn-info btn-sm" data-widget="collapse"><i class="fa fa-minus"></i></button>
                                        <button class="btn btn-info btn-sm" data-widget="remove"><i class="fa fa-times"></i></button>
                                    </div>
                                </div>

                  <div class="box-body">
                     <form id="input" method="post" class="form-horizontal foto_banyak" action="<?=base_admin();?>modul/data_buku/data_buku_action.php?act=in">
                      <div class="form-group">
                        <label for="ID Buku" class="control-label col-lg-2">ID Buku</label>
                        <div class="col-lg-10">
                          <input type="text" name="buku_kode" placeholder="ID Buku" class="form-control" required> 
                        </div>
                      </div><!-- /.form-group -->
<div class="form-group">
                        <label for="Judul" class="control-label col-lg-2">Judul</label>
                        <div class="col-lg-10">
                          <input type="text" name="buku_judul" placeholder="Judul" class="form-control" required> 
                        </div>
                      </div><!-- /.form-group -->
<div class="form-group">
                        <label for="Pengarang" class="control-label col-lg-2">Pengarang</label>
                        <div class="col-lg-10">
                          <input type="text" name="buku_pengarang" placeholder="Pengarang" class="form-control" required> 
                        </div>
                      </div><!-- /.form-group -->
<div class="form-group">
                        <label for="Penerbit" class="control-label col-lg-2">Penerbit</label>
                        <div class="col-lg-10">
                          <input type="text" name="buku_penerbit" placeholder="Penerbit" class="form-control" required> 
                        </div>
                      </div><!-- /.form-group -->
<div class="form-group">
                        <label for="Tahun" class="control-label col-lg-2">Tahun</label>
                        <div class="col-lg-10">
                          <input type="text" data-rule-number="true" name="buku_tahun" placeholder="Tahun" class="form-control" > 
                        </div>
                      </div><!-- /.form-group -->
<div class="form-group">
                        <label for="Hal. Romawi" class="control-label col-lg-2">Hal. Romawi</label>
                        <div class="col-lg-10">
                          <input type="text" name="buku_hal_romawi" placeholder="Hal. Romawi" class="form-control" > 
                        </div>
                      </div><!-- /.form-group -->
<div class="form-group">
                        <label for="Hal." class="control-label col-lg-2">Hal.</label>
                        <div class="col-lg-10">
                          <input type="text" data-rule-number="true" name="buku_hal" placeholder="Hal." class="form-control" > 
                        </div>
                      </div><!-- /.form-group -->
<div class="form-group">
                        <label for="Tempat" class="control-label col-lg-2">Tempat</label>
                        <div class="col-lg-10">
                          <input type="text" name="buku_tempat" placeholder="Tempat" class="form-control" > 
                        </div>
                      </div><!-- /.form-group -->
<div class="form-group">
                        <label for="ISBN" class="control-label col-lg-2">ISBN</label>
                        <div class="col-lg-10">
                          <input type="text" name="buku_isbn" placeholder="ISBN" class="form-control" > 
                        </div>
                      </div><!-- /.form-group -->
<div class="form-group">
                        <label for="Kategori Buku" class="control-label col-lg-2">Kategori Buku</label>
                        <div class="col-lg-10">
                          <select name="kategori_buku_id" data-placeholder="Pilih Kategori Buku ..." class="form-control chzn-select" tabindex="2" required>
               <option value=""></option>
               <?php foreach ($db->fetch_all("kategori_buku") as $isi) {
                  echo "<option value='$isi->kategori_buku_id'>$isi->kategori_buku_nama</option>";
               } ?>
              </select>
                        </div>
                      </div><!-- /.form-group -->
<div class="form-group">
                        <label for="Rak" class="control-label col-lg-2">Rak</label>
                        <div class="col-lg-10">
                          <input type="text" name="buku_rak" placeholder="Rak" class="form-control" > 
                        </div>
                      </div><!-- /.form-group -->
<div class="form-group">
                        <label for="Jumlah" class="control-label col-lg-2">Jumlah</label>
                        <div class="col-lg-10">
                          <input type="text" data-rule-number="true" name="buku_jumlah" placeholder="Jumlah" class="form-control" > 
                        </div>
                      </div><!-- /.form-group -->
<div class="form-group">
                        <label for="Keterangan" class="control-label col-lg-2">Keterangan</label>
                        <div class="col-lg-10">
                          <input type="text" name="buku_ket" placeholder="Keterangan" class="form-control" > 
                        </div>
                      </div><!-- /.form-group -->

                      
                      <div class="form-group">
                        <label for="tags" class="control-label col-lg-2">&nbsp;</label>
                        <div class="col-lg-10">
                          <input type="submit" class="btn btn-primary btn-flat" value="submit">
                        </div>
                      </div><!-- /.form-group -->
                    </form>
 <a href="<?=base_index();?>data-buku" class="btn btn-success btn-flat"><i class="fa fa-step-backward"></i> Kembali</a>
          
                  </div>
                  </div>
              </div>
</div>
                  
                </section><!-- /.content -->
        
            