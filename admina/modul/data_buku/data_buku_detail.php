

                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                     Data Buku
                    </h1>
                   <ol class="breadcrumb">
                        <li><a href="<?=base_index();?>"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li><a href="<?=base_index();?>data-buku">Data Buku</a></li>
                        <li class="active">Detail Data Buku</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
<div class="row">
    <div class="col-lg-12">
        <div class="box box-solid box-primary">
                                   <div class="box-header">
                                    <h3 class="box-title">Detail Data Buku</h3>
                                    <div class="box-tools pull-right">
                                        <button class="btn btn-info btn-sm" data-widget="collapse"><i class="fa fa-minus"></i></button>
                                        <button class="btn btn-info btn-sm" data-widget="remove"><i class="fa fa-times"></i></button>
                                    </div>
                                </div>

                  <div class="box-body">
                   <form class="form-horizontal">
                      <div class="form-group">
                        <label for="ID Buku" class="control-label col-lg-2">ID Buku</label>
                        <div class="col-lg-10">
                          <input type="text" disabled="" value="<?=$data_edit->buku_kode;?>" class="form-control">
                        </div>
                      </div><!-- /.form-group -->
<div class="form-group">
                        <label for="Judul" class="control-label col-lg-2">Judul</label>
                        <div class="col-lg-10">
                          <input type="text" disabled="" value="<?=$data_edit->buku_judul;?>" class="form-control">
                        </div>
                      </div><!-- /.form-group -->
<div class="form-group">
                        <label for="Pengarang" class="control-label col-lg-2">Pengarang</label>
                        <div class="col-lg-10">
                          <input type="text" disabled="" value="<?=$data_edit->buku_pengarang;?>" class="form-control">
                        </div>
                      </div><!-- /.form-group -->
<div class="form-group">
                        <label for="Penerbit" class="control-label col-lg-2">Penerbit</label>
                        <div class="col-lg-10">
                          <input type="text" disabled="" value="<?=$data_edit->buku_penerbit;?>" class="form-control">
                        </div>
                      </div><!-- /.form-group -->
<div class="form-group">
                        <label for="Tahun" class="control-label col-lg-2">Tahun</label>
                        <div class="col-lg-10">
                          <input type="text" disabled="" value="<?=$data_edit->buku_tahun;?>" class="form-control">
                        </div>
                      </div><!-- /.form-group -->
<div class="form-group">
                        <label for="Hal. Romawi" class="control-label col-lg-2">Hal. Romawi</label>
                        <div class="col-lg-10">
                          <input type="text" disabled="" value="<?=$data_edit->buku_hal_romawi;?>" class="form-control">
                        </div>
                      </div><!-- /.form-group -->
<div class="form-group">
                        <label for="Hal." class="control-label col-lg-2">Hal.</label>
                        <div class="col-lg-10">
                          <input type="text" disabled="" value="<?=$data_edit->buku_hal;?>" class="form-control">
                        </div>
                      </div><!-- /.form-group -->
<div class="form-group">
                        <label for="Tempat" class="control-label col-lg-2">Tempat</label>
                        <div class="col-lg-10">
                          <input type="text" disabled="" value="<?=$data_edit->buku_tempat;?>" class="form-control">
                        </div>
                      </div><!-- /.form-group -->
<div class="form-group">
                        <label for="ISBN" class="control-label col-lg-2">ISBN</label>
                        <div class="col-lg-10">
                          <input type="text" disabled="" value="<?=$data_edit->buku_isbn;?>" class="form-control">
                        </div>
                      </div><!-- /.form-group -->
<div class="form-group">
                        <label for="Kategori Buku" class="control-label col-lg-2">Kategori Buku</label>
                        <div class="col-lg-10">
                          <?php foreach ($db->fetch_all("kategori_buku") as $isi) {
                  if ($data_edit->kategori_buku_id==$isi->kategori_buku_id) {

                    echo "<input disabled class='form-control' type='text' value='$isi->kategori_buku_nama'>";
                  }
               } ?>
              
                        </div>
                      </div><!-- /.form-group -->
<div class="form-group">
                        <label for="Rak" class="control-label col-lg-2">Rak</label>
                        <div class="col-lg-10">
                          <input type="text" disabled="" value="<?=$data_edit->buku_rak;?>" class="form-control">
                        </div>
                      </div><!-- /.form-group -->
<div class="form-group">
                        <label for="Jumlah" class="control-label col-lg-2">Jumlah</label>
                        <div class="col-lg-10">
                          <input type="text" disabled="" value="<?=$data_edit->buku_jumlah;?>" class="form-control">
                        </div>
                      </div><!-- /.form-group -->
<div class="form-group">
                        <label for="Keterangan" class="control-label col-lg-2">Keterangan</label>
                        <div class="col-lg-10">
                          <input type="text" disabled="" value="<?=$data_edit->buku_ket;?>" class="form-control">
                        </div>
                      </div><!-- /.form-group -->

                   
                    </form>
                    <a href="<?=base_index();?>data-buku" class="btn btn-success btn-flat"><i class="fa fa-step-backward"></i> Kembali</a>
          
                  </div>
                  </div>
              </div>
</div>
                  
                </section><!-- /.content -->
        
