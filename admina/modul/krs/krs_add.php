

                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                     Krs
                    </h1>
                           <ol class="breadcrumb">
                        <li><a href="<?=base_index();?>"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li><a href="<?=base_index();?>krs">Krs</a></li>
                        <li class="active">Tambah Krs</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
<div class="row">
    <div class="col-lg-12">
        <div class="box box-solid box-primary">
                                 <div class="box-header">
                                    <h3 class="box-title">Tambah Krs</h3>
                                    <div class="box-tools pull-right">
                                        <button class="btn btn-info btn-sm" data-widget="collapse"><i class="fa fa-minus"></i></button>
                                        <button class="btn btn-info btn-sm" data-widget="remove"><i class="fa fa-times"></i></button>
                                    </div>
                                </div>

                  <div class="box-body">
                     <form id="input" method="post" class="form-horizontal foto_banyak" action="<?=base_admin();?>modul/krs/krs_action.php?act=in">
<div class="form-group">
                        <label for="NIM" class="control-label col-lg-2">NIM</label>
                                                
                        <div class="col-lg-10">
                          <select id="nim_mhs" onchange="myFunction()" name="nim_mahasiswa" data-placeholder="Pilih Mahasiswa..." class="form-control chzn-select" tabindex="2" >
               <option value=""></option>

               <?php 
               $id_user=$db->fetch_single_row('sys_users','id',$_SESSION['id_user'])->id;
               if ($id_user <> 1) {
                     $nim_mhs=ucwords($db->fetch_single_row('sys_users','id',$_SESSION['id_user'])->username);
                     $query_mhs = "select * from mhs where nipd = ". $nim_mhs;
                     foreach ($db->fetch_custom($query_mhs) as $isi) {
                          echo "<option nama='$isi->nm_pd' value='$isi->nipd.$isi->nm_pd' selected> $isi->nipd.$isi->nm_pd</option>";
                     } 
                }
                else
                {                     
                     $query_mhs = "select * from mhs where kode_jurusan = ". $path_id;
                     foreach ($db->fetch_custom($query_mhs) as $isi) {
                          echo "<option nama='$isi->nm_pd' value='$isi->nipd.$isi->nm_pd' selected> $isi->nipd.$isi->nm_pd</option>";
                     } 
                }

                 

                     ?>


              </select>
                        </div>

                      </div><!-- /.form-group -->
<!--
<div class="form-group">
                        <label for="Nama" class="control-label col-lg-2">Nama</label>
                        <div class="col-lg-10">
                          <input id="nama" type="text" name="nama" placeholder="Nama" class="form-control" required readonly>
                        </div>
                      </div><!-- /.form-group -->

<div class="form-group">
                        <label for="Kode Matakuliah" class="control-label col-lg-2">Kode Matakuliah</label>
                        <div class="col-lg-10">
                          <select id="kodemk" onchange="myFunction()" name="kode_mk" data-placeholder="Pilih Mata Kuliah..." class="form-control chzn-select" tabindex="2" >
               <option value=""></option>
               <?php 
                
                
                $query_matakuliah="select * from kelas_kuliah where kode_jurusan =". $id_jur;


                foreach ($db->fetch_custom($query_matakuliah) as $isi) {
                echo "<option kode_mk='$isi->kode_mk' 
                              nama_mk= '$isi->nama_mk' 
                           nama_kelas='$isi->nama_kelas' 
                             semester='$isi->semester'
                                value='$isi->kode_mk'>$isi->kode_mk.$isi->nama_mk</option>";
               } ?>
              </select>
                        </div>
                      </div><!-- /.form-group -->

<div class="form-group">
                        <label for="Nama Matakuliah" class="control-label col-lg-2">Nama Matakuliah</label>
                        <div class="col-lg-10">
                          <input id="namamk" type="text" name="nama_mk" placeholder="Nama Matakuliah" class="form-control" required readonly>
                        </div>
                      </div><!-- /.form-group -->
<div class="form-group">
                        <label for="Nama Kelas" class="control-label col-lg-2">Nama Kelas</label>
                        <div class="col-lg-10">
                          <input id="nama_kelas" type="text" name="nama_kelas" placeholder="01" class="form-control" required readonly>
                        </div>
                      </div><!-- /.form-group -->
<div class="form-group">
                        <label for="Semester" class="control-label col-lg-2">Semester</label>
                        <div class="col-lg-10">
                          <input id="semester" type="text" name="semester" placeholder="20141" class="form-control" required readonly>
                        </div>
                      </div><!-- /.form-group -->
 <input type="hidden" name="kode_jurusan" value="<?=$id_jur;?>">

                      <div class="form-group">
                        <label for="tags" class="control-label col-lg-2">&nbsp;</label>
                        <div class="col-lg-10">
                          <input type="submit" class="btn btn-primary btn-flat" value="submit">
                        </div>
                      </div><!-- /.form-group -->
                    </form>
 <a onclick="window.history.back(-1)" class="btn btn-success btn-flat"><i class="fa fa-step-backward"></i> Kembali</a>

                  </div>
                  </div>
              </div>
</div>

                </section><!-- /.content -->


<script type="text/javascript">
    $(document).ready(function(){
      $("#nim_mhs").change(function(){        
        $("#nama").val($("#nim_mhs").val());
      });
    });

  

    $(document).ready(function(){
      $("#kodemk").change(function(){  
          

        var a=$("#kodemk option:selected").attr("nama_mk");
        var b=$("#kodemk option:selected").attr("nama_kelas");
        var c=$("#kodemk option:selected").attr("semester");

        $("#namamk").val(a);
        $("#nama_kelas").val(b);
        $("#semester").val(c);
      });
    });

</script>  