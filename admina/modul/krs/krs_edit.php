

                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                      Krs
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="<?=base_index();?>"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li><a href="<?=base_index();?>krs">Krs</a></li>
                        <li class="active">Edit Krs</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
<div class="row">
    <div class="col-lg-12">
        <div class="box box-solid box-primary">
                                   <div class="box-header">
                                    <h3 class="box-title">Edit Krs</h3>
                                    <div class="box-tools pull-right">
                                        <button class="btn btn-info btn-sm" data-widget="collapse"><i class="fa fa-minus"></i></button>
                                        <button class="btn btn-info btn-sm" data-widget="remove"><i class="fa fa-times"></i></button>
                                    </div>
                                </div>

<div class="box-body">
                     <form id="update" method="post" class="form-horizontal" action="<?=base_admin();?>modul/krs/krs_action.php?act=up">
<div class="form-group">
                        <?php //die($data_edit->nim.".".$data_edit->nama); ?>
                        <label for="l_nim_mhs" class="control-label col-lg-2">NIM</label>
                        <div class="col-lg-10">
                          <select id="nim_mhs" onchange="myFunction()" name="nim_mahasiswa"  data-placeholder="Pilih Mahasiswa..." class="form-control chzn-select" tabindex="2" >
             
               <?php foreach ($db->fetch_all("mhs") as $isi) {

                  if (trim($data_edit->nim.".".$data_edit->nama)==($isi->nipd.".".$isi->nm_pd)) {
                        echo "<option nama='$isi->nm_pd' value='$isi->nipd.$isi->nm_pd' selected> $isi->nipd.$isi->nm_pd</option>";
                  } else {
                        echo "<option nama='$isi->nm_pd' value='$isi->nipd.$isi->nm_pd' > $isi->nipd.$isi->nm_pd</option>";
                  }
                    
               } ?>


              </select>
                        </div>
                      </div><!-- /.form-group -->
<div class="form-group">
                        <label for="Nama" class="control-label col-lg-2">Nama</label>
                        <div class="col-lg-10">
                          <input id="nama" type="text" name="nama" value="<?=$data_edit->nama;?>" class="form-control" readonly> 
                        </div>
                      </div><!-- /.form-group -->

<div class="form-group">
                        <?php //die($data_edit->nim.".".$data_edit->nama); ?>
                        <label for="Kode Matakuliah" class="control-label col-lg-2">Kode Matakuliah</label>
                        
                        <div class="col-lg-10">
                          
                          <select id="kodemk2"  onchange="MyFunction" name="kode_mk" data-placeholder="Pilih Mata Kuliah..." class="form-control chzn-select" tabindex="2" >
               <?php 
                 


                $query_matakuliah="select * from kelas_kuliah where kode_jurusan =". $data_edit->kode_jurusan;


                foreach ($db->fetch_custom($query_matakuliah) as $isi) {

                  if (trim($data_edit->kode_mk.".".$data_edit->nama_mk)==($isi->kode_mk.".".$isi->nama_mk)) {
                        echo "<option 
                              kode_mk='$isi->kode_mk' 
                              nama_mk='$isi->nama_mk' 
                           nama_kelas='$isi->nama_kelas' 
                             semester='$isi->semester'
                                value='$isi->kode_mk' selected > $isi->kode_mk.$isi->nama_mk</option>";
                  } else {
                        echo "<option 
                              kode_mk='$isi->kode_mk' 
                              nama_mk='$isi->nama_mk' 
                           nama_kelas='$isi->nama_kelas' 
                             semester='$isi->semester'
                                value='$isi->kode_mk' > $isi->kode_mk.$isi->nama_mk</option>";
                  }
                    
               } ?>
              </select>
                        </div>
                      </div><!-- /.form-group -->
<div class="form-group">
                        <label for="Kode Matakuliah" class="control-label col-lg-2">Nama Matakuliah</label>
                        <div class="col-lg-10">
                          <input id="namamk" type="text" name="nama_mk" value="<?=$data_edit->nama_mk;?>" class="form-control" readonly> 
                        </div>
                      </div><!-- /.form-group -->
<div class="form-group">
                        <label for="Nama Kelas" class="control-label col-lg-2">Nama Kelas</label>
                        <div class="col-lg-10">
                          <input id="nama_kelas" type="text" name="nama_kelas" value="<?=$data_edit->nama_kelas;?>" class="form-control" readonly > 
                        </div>
                      </div><!-- /.form-group -->
<div class="form-group">
                        <label for="Semester" class="control-label col-lg-2">Semester</label>
                        <div class="col-lg-10">
                          <input id="semester" type="text" name="semester" value="<?=$data_edit->semester;?>" class="form-control" readonly > 
                        </div>
                      </div><!-- /.form-group -->

<!--
<div class="form-group">
                        <label for="Jurusan" class="control-label col-lg-2">Jurusan</label>
                        <div class="col-lg-10">
                          <select name="kode_jurusan" data-placeholder="Pilih Jurusan..." class="form-control chzn-select" tabindex="2" >
               <option value=""></option> -->
               <?php
/*               
if ($_SESSION['level']==1) {
  $jur = $db->fetch_custom("select * from jurusan");
} else {
  $jur = $db->fetch_custom("select * from jurusan where id_fakultas='".$_SESSION['fakultas']."'");
}
foreach ($jur as $isi) {

                  if ($data_edit->kode_jurusan==$isi->kode_jurusan) {
                    echo "<option value='$isi->kode_jurusan' selected>$isi->nama_jurusan</option>";
                  }
               } 

*/               ?>
            <!--  </select>
                        </div>
                      </div><!-- /.form-group -->

                      <input type="hidden" name="kode_jurusan" value="<?=$data_edit->kode_jurusan;?>">
                      <input type="hidden" name="id" value="<?=$data_edit->id;?>">
                      <div class="form-group">
                        <label for="tags" class="control-label col-lg-2">&nbsp;</label>
                        <div class="col-lg-10">
                          <input type="submit" class="btn btn-primary btn-flat" value="submit">
                        </div>
                      </div><!-- /.form-group -->
                    </form>
                    <a onclick="window.history.back(-1)" class="btn btn-success btn-flat"><i class="fa fa-step-backward"></i> Kembali</a>
          
                  </div>
                  </div>
              </div>
</div>
                  
                </section><!-- /.content -->
        
 <script type="text/javascript">
    $(document).ready(function(){
      $("#nim_mhs").change(function(){        
        $("#nama").val($("#nim_mhs").val());
      });
    });

  
 
    $(document).ready(function(){
      $("#kodemk2").change(function (){  
          

        var a=$("#kodemk2 option:selected").attr("nama_mk");
        var b=$("#kodemk2 option:selected").attr("nama_kelas");
        var c=$("#kodemk2 option:selected").attr("semester");

        $("#namamk").val(a);
        $("#nama_kelas").val(b);
        $("#semester").val(c);
      });
    });

</script>  