
           
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                     Semester
                    </h1>
                           <ol class="breadcrumb">
                        <li><a href="<?=base_index();?>"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li><a href="<?=base_index();?>semester">Semester</a></li>
                        <li class="active">Tambah Semester</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
<div class="row">
    <div class="col-lg-12"> 
        <div class="box box-solid box-primary">
                                 <div class="box-header">
                                    <h3 class="box-title">Tambah Semester</h3>
                                    <div class="box-tools pull-right">
                                        <button class="btn btn-info btn-sm" data-widget="collapse"><i class="fa fa-minus"></i></button>
                                        <button class="btn btn-info btn-sm" data-widget="remove"><i class="fa fa-times"></i></button>
                                    </div>
                                </div>

                  <div class="box-body">
                     <form id="input" method="post" class="form-horizontal foto_banyak" action="<?=base_admin();?>modul/semester/semester_action.php?act=in">

<div class="form-group">
                        <label for="Kode Semester" class="control-label col-lg-2">Kode Semester</label>
                        <div class="col-lg-10">
                          <input type="text" name="kode_semester" placeholder="Kode Semester, Contoh: 20201 (Tahun 2020 Semester 1 atau Ganjil)" class="form-control" required > 
                        </div>
                      </div><!-- /.form-group -->

                      <div class="form-group">
                        <label for="Nama Semester" class="control-label col-lg-2">Nama Semester</label>
                        <div class="col-lg-10">
                          <select name="nama_semester" data-placeholder="Pilih Nama Semester ..." class="form-control chzn-select" tabindex="2" required>
               <option value=""></option>
               <?php foreach ($db->fetch_all("m_jenis_semester") as $isi) {
                  echo "<option value='$isi->jns_semester_deskripsi'>$isi->jns_semester_deskripsi</option>";
               } ?>
              </select>
                        </div>
                      </div><!-- /.form-group -->
<div class="form-group">
                        <label for="Deskripsi Semester" class="control-label col-lg-2">Deskripsi Semester</label>
                        <div class="col-lg-10">
                          <input type="text" name="smt_name" placeholder="Contoh Deskripsi Semester : 2020/2021 Ganjil" class="form-control" required > 
                        </div>
                      </div><!-- /.form-group -->
<div class="form-group">
                        <label for="Status" class="control-label col-lg-2">Status</label>
                        <div class="col-lg-10">
                          <input name="status" class="make-switch" type="checkbox" checked>
                        </div>
                      </div><!-- /.form-group -->

                      
                      <div class="form-group">
                        <label for="tags" class="control-label col-lg-2">&nbsp;</label>
                        <div class="col-lg-10">
                          <input type="submit" class="btn btn-primary btn-flat" value="submit">
                        </div>
                      </div><!-- /.form-group -->
                    </form>
 <a href="<?=base_index();?>semester" class="btn btn-success btn-flat"><i class="fa fa-step-backward"></i> Kembali</a>
          
                  </div>
                  </div>
              </div>
</div>
                  
                </section><!-- /.content -->
        
            