
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        Manage Verifikasi SPP <?php echo $db->fetch_single_row('jurusan','kode_jurusan',$id_jur)->nama_jurusan;?>
                    </h1>
                        <ol class="breadcrumb">
                        <li><a href="<?=base_index();?>"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li><a href="<?=base_index();?>setujui-krs">Verifikasi SPP</a></li>
                        <li class="active">Verifikasi SPP</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="box">
                                <div class="box-header">
                                  <h3 class="box-title">List Verifikasi SPP</h3>                                  
                                </div><!-- /.box-header -->

<form method="get" class="form-horizontal foto_banyak" action="">

                      <div class="form-group">
                        <label for="Menu" class="control-label col-lg-2">Pencarian</label>
                        <div class="col-lg-8" >

                          <link href="<?=base_admin();?>assets/plugins/chosen/chosen.min.css" rel="stylesheet" type="text/css" />
                          <script src="<?=base_admin();?>assets/plugins/chosen/chosen.jquery.min.js" type="text/javascript"></script>

                          <!-- Script -->
                          
                          <script type="text/javascript">
                          $(document).ready(function(){
                           $('select').chosen({ height:'50' });
                          });
                          </script>
                          

                            <select id="mhs" name="user" id="id_po_select" data-placeholder="Pilih Semester-Matakuliah...." class="form-control chzn-select" tabindex="2" >
                        <option value=""></option>
                          <?php 
                              $username=ucwords($db->fetch_single_row('sys_users','id',$_SESSION['id_user'])->username);
                              $name=ucwords($db->fetch_single_row('m_dosen','dosen_nidn',$username)->dosen_nama);
                              if($username<>"Admin"){
                              foreach ($db->fetch_custom("select DISTINCT tahun_angkatan from view_0023_list_thn_angkatan_mhs where kode_jurusan=$id_jur and nipd=$username                                                                                                                      
                                                          ") as $isi) {

                                       if ($_GET['user']==$isi->tahun_angkatan) {
                                                   echo "<option value='$isi->tahun_angkatan' selected > $isi->tahun_angkatan</option>";
                                       } else {
                                                   echo "<option value='$isi->tahun_angkatan'  > $isi->tahun_angkatan</option>";

                                       }
                               }
                               }
                               else
                               {
                                foreach ($db->fetch_custom("select DISTINCT tahun_angkatan from view_0023_list_thn_angkatan_mhs where kode_jurusan=$id_jur 
                                                          ") as $isi) {

                                       if ($_GET['user']==$isi->tahun_angkatan) {
                                                   echo "<option value='$isi->tahun_angkatan' selected > $isi->tahun_angkatan</option>";
                                       } else {
                                                   echo "<option value='$isi->tahun_angkatan'  > $isi->tahun_angkatan</option>";

                                       }
                               }
                               }
                          ?>
                          <option value=""> </option>
                  
                  </select>
                        </div>
                      </div><!-- /.form-group -->

 <label for="Menu" class="control-label col-lg-2">&nbsp;</label>
                        <div class="col-lg-10">
<button style="margin-top:10px;margin-bottom:10px" class="btn btn-primary">Show Daftar Mahasiswa</button>
</div>
</form>

                                <div class="box-body table-responsive">
          
<?php if (isset($_GET['user'])) {
  
?>       
<h3>Checklist Untuk memberikan verifikasi Mahasiswa telah membayar SPP</h3>
<table id="dtb" class="table table-bordered table-condensed table-hover table-striped">
                      <thead align="center">
                        <tr>

                          <th style="width:10px">No.</th>
                          <th style="width:20px">NIM</th>
                          <th style="width:10px ">Nama Mahasiswa</th>
                          <th style="width:20px">SPP untuk KRS  </th>
                          <th style="width:40px">Keterangan</th>
                          
                          <th style="width:40px">SPP untuk UTS</th>
                          <th style="width:40px">Keterangan</th>

                          <th style="width:10px">SPP untuk UAS</th>
                          <th style="width:40px">Keterangan</th>
                          
                         
                        </tr>
                      </thead>
                      <tbody>
                              <!--<div class="form-group"> <label>Semester : </label><input type="text" id="min" name="min"> </div> -->
                              <?php 
                                  $dtb=$db->fetch_custom("SELECT * from view_0023_list_thn_angkatan_mhs where tahun_angkatan=?", array('tahun_angkatan'=>$_GET['user'])) ;
                                  $i=1;
                                  foreach ($dtb as $isi) {
                                    ?>

                                    <tr id="line_<?=$isi->cek_spp_id;?>">
                                   
                                    <td align="center"><?=$i.'.';?></td>
                                    <td align="center"><?=$isi->nipd;?></td>
                                    <td ><?=$isi->nm_pd;?></td>

                                    <td align="center" >
                                      <div class="checkbox">
                                              <label>
                                                <input class="uniform" name="option1" type="checkbox" value="<?=$isi->nipd;?>" onclick="spp_krs(<?=$isi->nipd;?>,this)" <?=($isi->cek_spp_status_krs==1)?'checked':'';?>>                                                
                                              </label>                                              
                                       </div>
                                    </td>

                                    <td align="center" >
                                      <div class="text">
                                              <label>                                                
                                                <input size="20" data-rule-number="true" class="uniform" id="text_ket_krs_<?=$isi->nipd;?>" name="text_ket_krs_<?=$isi->nipd;?>" type="text" value="<?=(isset($isi->cek_spp_id))? $isi->cek_spp_ket_krs : '';?>" onchange="spp_krs_ket(<?=$isi->nipd;?>,this);"   >  
                                              </label>                                              
                                       </div>
                                    </td>
                                   
                                  <td align="center" >
                                      <div class="checkbox">
                                              <label>
                                                <input class="uniform" name="option2" type="checkbox" value="<?=$isi->nipd;?>" onclick="spp_uts(<?=$isi->nipd;?>,this)" <?=($isi->cek_spp_status_uts==1)?'checked':'';?>>                                                
                                              </label>                                              
                                       </div>
                                    </td>

                                    <td align="center" >
                                      <div class="text">
                                              <label>                                                
                                                <input size="20" data-rule-number="true" class="uniform" id="text_ket_uts_<?=$isi->nipd;?>" name="text_ket_uts_<?=$isi->nipd;?>" type="text" value="<?=(isset($isi->cek_spp_id))? $isi->cek_spp_ket_uts : '';?>" onchange="spp_uts_ket(<?=$isi->nipd;?>,this);"   >  
                                              </label>                                              
                                       </div>
                                    </td>
                                    
                                    <td align="center" >
                                      <div class="checkbox">
                                              <label>
                                                <input class="uniform" name="option3" type="checkbox" value="<?=$isi->nipd;?>" onclick="spp_uas(<?=$isi->nipd;?>,this)" <?=($isi->cek_spp_status_uas==1)?'checked':'';?>>                                                
                                              </label>                                              
                                       </div>
                                    </td>

                                    <td align="center" >
                                      <div class="text">
                                              <label>                                                
                                                <input size="20" data-rule-number="true" class="uniform" id="text_ket_uas_<?=$isi->nipd;?>" name="text_ket_uas_<?=$isi->nipd;?>" type="text" value="<?=(isset($isi->cek_spp_id))? $isi->cek_spp_ket_uas : '';?>" onchange="spp_uas_ket(<?=$isi->nipd;?>,this);"   >  
                                              </label>                                              
                                       </div>
                                    </td>
                                    
                                    
                                    <?php
                                    if($i)
                                    {
                                        ?>                                        
                            <?php
                                    } else {
                                    ?>                                    
                                                      <?php 
                                                      }
                                                      ?>
                                    </tr>
                                    <?php
                                    $i++;
                                  }
                                  ?>
                       </tbody>
</table>
<?php 

}  

?>


                                </div><!-- /.box-body -->
                            </div><!-- /.box -->
                        </div>
                    </div>
                </section><!-- /.content -->
  



<script type="text/javascript">



$(document).ready(function() {
      $('#dtb').DataTable( {


        //dom: 'Bfrtip',
        buttons: [
        'colvis'
        ],
        "oLanguage": {

           "sSearch": "Pencarian berdasarkan Semester atau Nama Mata Kuliah :  "

         },
    
         "columnDefs": [ {
      "targets": [0,1,3,4,5],
      "searchable": false
    } ],


    } );

   

  } );

 
  
function read_act(id,cb) {
  check_act = '';
  id_cb = id;
  if (cb.checked) {
     check_act = 'Y';

   setTimeout(function() {
   location.reload();
   }, 100);
  
  } else {
    check_act = 'N';

  setTimeout(function() {
   location.reload();
   }, 100);

  }
  
  
  
         

  $.ajax({

        type: "post",
        url: "<?=base_admin();?>modul/ajuan_krs/ajuan_krs_action.php?act=change_read",
        data: "nim="+'<?=$username;?>'+"&nama="+'<?=$name;?>'+"&id="+id_cb+"&cek="+check_act,
        
     //  enctype:  'multipart/form-data'
      success: function(data){
               
        console.log(data);
        //$("#dtb").dataTable().fnReloadAjax();

        //$('#dtb').DataTable().ajax.reload();

    }

  });
}


function spp_krs(id,cb) {  
  $.ajax({

        type: "post",
        url: "<?=base_admin();?>modul/verifikasi_spp/verifikasi_spp_action.php?act=change_spp_krs",
        data: "id="+id,        
     
      success: function(data){               
        console.log(data);        

    }

  });
}

function spp_krs_ket(id,cb) {  
  $.ajax({

        type: "post",
        url: "<?=base_admin();?>modul/verifikasi_spp/verifikasi_spp_action.php?act=change_spp_krs_ket",
        data: "id="+id+"&ket="+cb.value,        
     
      success: function(data){               
        console.log(data);        

    }

  });
}

function spp_uts(id,cb) {  
  $.ajax({

        type: "post",
        url: "<?=base_admin();?>modul/verifikasi_spp/verifikasi_spp_action.php?act=change_spp_uts",
        data: "id="+id,        
     
      success: function(data){               
        console.log(data);        

    }

  });
}

function spp_uts_ket(id,cb) {  
  $.ajax({

        type: "post",
        url: "<?=base_admin();?>modul/verifikasi_spp/verifikasi_spp_action.php?act=change_spp_uts_ket",
        data: "id="+id+"&ket_uts="+cb.value,        
     
      success: function(data){               
        console.log(data);        

    }

  });
}

function spp_uas(id,cb) {  
  $.ajax({

        type: "post",
        url: "<?=base_admin();?>modul/verifikasi_spp/verifikasi_spp_action.php?act=change_spp_uas",
        data: "id="+id,        
     
      success: function(data){               
        console.log(data);        

    }

  });
}

function spp_uas_ket(id,cb) {  
  $.ajax({

        type: "post",
        url: "<?=base_admin();?>modul/verifikasi_spp/verifikasi_spp_action.php?act=change_spp_uas_ket",
        data: "id="+id+"&ket_uas="+cb.value,        
     
      success: function(data){               
        console.log(data);        

    }

  });
}

/*
function insert_act(id,cb) {
  check_act = '';
  if (cb.checked) {
     check_act = 'Y';
  } else {
    check_act = 'N';

  }
  $.ajax({

        type: "post",
        url: "<?=base_admin();?>modul/ajuan_krs/ajuan_krs_action.php?act=change_insert",
        data: "role_id="+id+"&data_act="+check_act,
     //  enctype:  'multipart/form-data'
      success: function(data){

        console.log(data);
    }

  });
}

function update_act(id,cb) {
  check_act = '';
  if (cb.checked) {
     check_act = 'Y';
  } else {
    check_act = 'N';

  }
  $.ajax({

        type: "post",
        url: "<?=base_admin();?>modul/ajuan_krs/ajuan_krs_action.php?act=change_update",
        data: "role_id="+id+"&data_act="+check_act,
     //  enctype:  'multipart/form-data'
      success: function(data){

        console.log(data);
    }

  });
}

function delete_act(id,cb) {
  check_act = '';
  if (cb.checked) {
     check_act = 'Y';
  } else {
    check_act = 'N';

  }
  $.ajax({

        type: "post",
        url: "<?=base_admin();?>modul/ajuan_krs/ajuan_krs_action.php?act=change_delete",
        data: "role_id="+id+"&data_act="+check_act,
     //  enctype:  'multipart/form-data'
      success: function(data){

        console.log(data);
    }

  });
}
*/

  </script>




