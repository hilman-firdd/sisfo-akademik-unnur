<?php
session_start();
include "../../inc/config.php";
session_check();
switch ($_GET["act"]) {
  case "in":
  
  case 'change_spp_krs':
 
    $cek_spp_kode_semester=ucwords($db->fetch_single_row('semester','status','Aktif')->semester);
    $cek_spp_operator_krs=ucwords($db->fetch_single_row('sys_users','id',$_SESSION['id_user'])->id);

    $data_spp = $db->fetch_custom_single("select * from view_0023_list_thn_angkatan_mhs where cek_spp_nim=? and cek_spp_kode_semester=? ",array("cari" =>$_POST['id'], "semester"=>$cek_spp_kode_semester ));

    $data_cek = array("cek_spp_nim"=>$_POST["id"] , "cek_spp_kode_semester"=>$cek_spp_kode_semester);
    $check = $db->check_exist('t_cek_spp',$data_cek);

    if ($check==false) {         
          
          $data = array(
          "cek_spp_nim"=>$_POST['id'],
          "cek_spp_kode_semester"=>$cek_spp_kode_semester,
          "cek_spp_tgl_krs"=>date("Y-m-d H:i:s"),
          "cek_spp_operator_krs"=>$cek_spp_operator_krs,
          "cek_spp_status_krs"=>1,
          
          );
          
          $in = $db->insert("t_cek_spp",$data);   
          
    }
    else {
          
          if($data_spp->cek_spp_status_krs==1) {
              $data = array(  "cek_spp_status_krs" =>2, "cek_spp_kode_semester"=>$cek_spp_kode_semester, "cek_spp_operator_krs"=>$cek_spp_operator_krs, "cek_spp_tgl_krs"=>date("Y-m-d H:i:s"),); }
          else{
              $data = array(  "cek_spp_status_krs" =>1, "cek_spp_kode_semester"=>$cek_spp_kode_semester, "cek_spp_operator_krs"=>$cek_spp_operator_krs, "cek_spp_tgl_krs"=>date("Y-m-d H:i:s"),);
          }
          
          $in = $db->update("t_cek_spp",$data, 'cek_spp_id', $data_spp->cek_spp_id);   
          
    }
    
    break;

    case 'change_spp_krs_ket':
  
    $cek_spp_kode_semester=ucwords($db->fetch_single_row('semester','status','Aktif')->semester);
    $cek_spp_operator_krs=ucwords($db->fetch_single_row('sys_users','id',$_SESSION['id_user'])->id);

    $data_spp = $db->fetch_custom_single("select * from view_0023_list_thn_angkatan_mhs where cek_spp_nim=? and cek_spp_kode_semester=? ",array("cari" =>$_POST['id'], "semester"=>$cek_spp_kode_semester ));
    

    $data_cek = array("cek_spp_nim"=>$_POST["id"] , "cek_spp_kode_semester"=>$cek_spp_kode_semester, "cek_spp_status_krs"=>$data_spp->cek_spp_status_krs);
    $check = $db->check_exist('t_cek_spp',$data_cek);

    if ($check==false) {         
          
          $data = array(
          "cek_spp_nim"=>$_POST['id'],
          "cek_spp_kode_semester"=>$cek_spp_kode_semester,
          "cek_spp_tgl_krs"=>date("Y-m-d H:i:s"),
          "cek_spp_operator_krs"=>$cek_spp_operator_krs,
          "cek_spp_status_krs"=>1,
          "cek_spp_ket_krs" =>$_POST['ket'],
          );
          
          $in = $db->insert("t_cek_spp",$data);  
          
    }
    else {
          
           $data = array(
          "cek_spp_ket_krs" =>$_POST['ket'], 
          "cek_spp_operator_krs"=>$cek_spp_operator_krs,   
          "cek_spp_tgl_krs"=>date("Y-m-d H:i:s"),
          );
          
          $in = $db->update("t_cek_spp",$data, 'cek_spp_id', $data_spp->cek_spp_id);   
          
    }
    
    break;


  case 'change_spp_uts':
 
    $cek_spp_kode_semester=ucwords($db->fetch_single_row('semester','status','Aktif')->semester);
    $cek_spp_operator_uts=ucwords($db->fetch_single_row('sys_users','id',$_SESSION['id_user'])->id);

    $data_spp = $db->fetch_custom_single("select * from view_0023_list_thn_angkatan_mhs where cek_spp_nim=? and cek_spp_kode_semester=? ",array("cari" =>$_POST['id'], "semester"=>$cek_spp_kode_semester ));

    $data_cek = array("cek_spp_nim"=>$_POST["id"] , "cek_spp_kode_semester"=>$cek_spp_kode_semester);
    $check = $db->check_exist('t_cek_spp',$data_cek);

    if ($check==false) {         
          
          $data = array(
          "cek_spp_nim"=>$_POST['id'],
          "cek_spp_kode_semester"=>$cek_spp_kode_semester,
          "cek_spp_tgl_uts"=>date("Y-m-d H:i:s"),
          "cek_spp_operator_uts"=>$cek_spp_operator_uts,
          "cek_spp_status_uts"=>1,
          
          );
          
          $in = $db->insert("t_cek_spp",$data);   
          
    }
    else {
          
          if($data_spp->cek_spp_status_uts==1) {
              $data = array(  "cek_spp_status_uts" =>2,"cek_spp_operator_uts"=>$cek_spp_operator_uts, "cek_spp_tgl_uts"=>date("Y-m-d H:i:s"),); }
          else{
              $data = array(  "cek_spp_status_uts" =>1,"cek_spp_operator_uts"=>$cek_spp_operator_uts, "cek_spp_tgl_uts"=>date("Y-m-d H:i:s"),);
          }
          
          $in = $db->update("t_cek_spp",$data, 'cek_spp_id', $data_spp->cek_spp_id);   
          
    }
    
    break;

    case 'change_spp_uts_ket':
  
    $cek_spp_kode_semester=ucwords($db->fetch_single_row('semester','status','Aktif')->semester);
    $cek_spp_operator_uts=ucwords($db->fetch_single_row('sys_users','id',$_SESSION['id_user'])->id);

    $data_spp = $db->fetch_custom_single("select * from view_0023_list_thn_angkatan_mhs where cek_spp_nim=? and cek_spp_kode_semester=? ",array("cari" =>$_POST['id'], "semester"=>$cek_spp_kode_semester ));
    

    $data_cek = array("cek_spp_nim"=>$_POST["id"] , "cek_spp_kode_semester"=>$cek_spp_kode_semester, "cek_spp_status_uts"=>$data_spp->cek_spp_status_uts);
    $check = $db->check_exist('t_cek_spp',$data_cek);

    if ($check==false) {         
          
          $data = array(
          "cek_spp_nim"=>$_POST['id'],
          "cek_spp_kode_semester"=>$cek_spp_kode_semester,
          "cek_spp_tgl_uts"=>date("Y-m-d H:i:s"),
          "cek_spp_operator_uts"=>$cek_spp_operator_uts,
          "cek_spp_status_uts"=>1,
          "cek_spp_ket_uts" =>$_POST['ket_uts'],
          );
          
          $in = $db->insert("t_cek_spp",$data);  
          
    }
    else {
          
           $data = array(
          "cek_spp_ket_uts" =>$_POST['ket_uts'], "cek_spp_operator_uts"=>$cek_spp_operator_uts, "cek_spp_tgl_uts"=>date("Y-m-d H:i:s"),
             
          
          );
          
          $in = $db->update("t_cek_spp",$data, 'cek_spp_id', $data_spp->cek_spp_id);   
          
    }
    
    break;

case 'change_spp_uas':
 
    $cek_spp_kode_semester=ucwords($db->fetch_single_row('semester','status','Aktif')->semester);
    $cek_spp_operator_uas=ucwords($db->fetch_single_row('sys_users','id',$_SESSION['id_user'])->id);

    $data_spp = $db->fetch_custom_single("select * from view_0023_list_thn_angkatan_mhs where cek_spp_nim=? and cek_spp_kode_semester=? ",array("cari" =>$_POST['id'], "semester"=>$cek_spp_kode_semester ));

    $data_cek = array("cek_spp_nim"=>$_POST["id"] , "cek_spp_kode_semester"=>$cek_spp_kode_semester);
    $check = $db->check_exist('t_cek_spp',$data_cek);

    if ($check==false) {         
          
          $data = array(
          "cek_spp_nim"=>$_POST['id'],
          "cek_spp_kode_semester"=>$cek_spp_kode_semester,
          "cek_spp_tgl_uas"=>date("Y-m-d H:i:s"),
          "cek_spp_operator_uas"=>$cek_spp_operator_uas,
          "cek_spp_status_uas"=>1,
          
          );
          
          $in = $db->insert("t_cek_spp",$data);   
          
    }
    else {
          
          if($data_spp->cek_spp_status_uas==1) {
              $data = array(  "cek_spp_status_uas" =>2,"cek_spp_operator_uas"=>$cek_spp_operator_uas, "cek_spp_tgl_uas"=>date("Y-m-d H:i:s"),); }
          else{
              $data = array(  "cek_spp_status_uas" =>1,"cek_spp_operator_uas"=>$cek_spp_operator_uas, "cek_spp_tgl_uas"=>date("Y-m-d H:i:s"),);
          }
          
          $in = $db->update("t_cek_spp",$data, 'cek_spp_id', $data_spp->cek_spp_id);   
          
    }
    
    break;

    case 'change_spp_uas_ket':
  
    $cek_spp_kode_semester=ucwords($db->fetch_single_row('semester','status','Aktif')->semester);
    $cek_spp_operator_uas=ucwords($db->fetch_single_row('sys_users','id',$_SESSION['id_user'])->id);

    $data_spp = $db->fetch_custom_single("select * from view_0023_list_thn_angkatan_mhs where cek_spp_nim=? and cek_spp_kode_semester=? ",array("cari" =>$_POST['id'], "semester"=>$cek_spp_kode_semester ));
    

    $data_cek = array("cek_spp_nim"=>$_POST["id"] , "cek_spp_kode_semester"=>$cek_spp_kode_semester, "cek_spp_status_uas"=>$data_spp->cek_spp_status_uas);
    $check = $db->check_exist('t_cek_spp',$data_cek);

    if ($check==false) {         
          
          $data = array(
          "cek_spp_nim"=>$_POST['id'],
          "cek_spp_kode_semester"=>$cek_spp_kode_semester,
          "cek_spp_tgl_uas"=>date("Y-m-d H:i:s"),
          "cek_spp_operator_uas"=>$cek_spp_operator_uas,
          "cek_spp_status_uas"=>1,
          "cek_spp_ket_uas" =>$_POST['ket_uas'],
          );
          
          $in = $db->insert("t_cek_spp",$data);  
          
    }
    else {
          
           $data = array(
          "cek_spp_ket_uas" =>$_POST['ket_uas'], "cek_spp_operator_uas"=>$cek_spp_operator_uas, "cek_spp_tgl_uas"=>date("Y-m-d H:i:s"),
             
          
          );
          
          $in = $db->update("t_cek_spp",$data, 'cek_spp_id', $data_spp->cek_spp_id);   
          
    }
    
    break;


  case "delete":    
    
    $db->delete("t_cek_spp_semester","cek_spp_id",$_GET["id"]);
    break;
  case "up":
   $data = array("cek_spp_kode_semester"=>$_POST["cek_spp_kode_semester"],"cek_spp_nim"=>$_POST["cek_spp_nim"],"cek_spp_tgl"=>$_POST["cek_spp_tgl"],"cek_spp_operator"=>$_POST["cek_spp_operator"],"cek_spp_syarat_kegiatan"=>$_POST["cek_spp_syarat_kegiatan"],);
   

    if(isset($_POST["cek_spp_status"])=="on")
    {
      $cek_spp_status = array("cek_spp_status"=>1);
      $data=array_merge($data,$cek_spp_status);
    } else { 
      $cek_spp_status = array("cek_spp_status"=>2);
      $data=array_merge($data,$cek_spp_status);
    }
    $up = $db->update("t_cek_spp_semester",$data,"cek_spp_id",$_POST["id"]);
    if ($up=true) {
      echo "good";
    } else {
      return false; 
    }
    break;
  default:
    # code...
    break;
}

?>